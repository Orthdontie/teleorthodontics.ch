﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/General.Master" AutoEventWireup="true" CodeFile="privacy.aspx.cs" Inherits="Teleorthodontics.Web.privacy" Culture="Auto" UICulture="Auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="bodyattributes" runat="server">
    id="contact"
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
    <title><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:title%>" /></title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="<asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:metadescription%>" />" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <section id="contact-section" class="contact bg-primary">
        <div class="container">
            <h2><asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:privacy1%>" /></h2>
            <div class="innerText">
                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:privacy2%>" />
                <br>
            </div>
            <br />
            <div class="grayText">
                <strong>
                    <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:privacy3%>" />
                </strong>
            </div>
            <br />
            <div class="innerText">
                <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:privacy4%>" />
            </div>
            <br />
            <div class="grayText">
                <strong>
                    <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:privacy5%>" />
                </strong>
            </div>
            <br />
            <div class="innerText">
                <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:privacy6%>" />
            </div>
            <br />
            <div class="grayText">
                <strong>
                    <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:privacy7%>" />
                </strong>
            </div>
            <br />
            <div class="innerText">
                <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:privacy8%>" />
                <br /><br />
            </div>
            <div class="innerText">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> 
                <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:privacy9%>" />
            </div>
            <br />
            <div class="grayText">
                <strong>
                    <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:privacy10%>" />
                </strong>
            </div>
            <br />
            <div class="innerText">
                <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:privacy11%>" />
                <br /><br />
            </div>
            <div class="innerText">
                <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:privacy12%>" />
                <br /><br />
            </div>
            <div class="innerText">
                <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:privacy13%>" />
            </div>
            <br />
            <div class="innerText">
                <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:privacy14%>" />
            </div>
            <br />
            <div class="innerText">
                <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:privacy15%>" />
            </div>
            <br />
            <br />
            <div class="grayText">
                <strong>
                    <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:privacy16%>" />
                </strong>
            </div>
            <br />
            <div class="innerText">
                <asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:privacy17%>" />
            </div>
            <div class="innerText">
                <br />
                <strong>
                    <asp:Literal ID="Literal20" runat="server" Text="<%$ Resources:privacy18%>" />
                </strong>
            </div>
            <div class="innerText">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> 
                <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:privacy19%>" />
            </div>
            <div class="innerText">
                <br />
                <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:privacy20%>" />
                <br />
            </div>
            <div class="innerText">
                <br />
                <strong>
                    <asp:Literal ID="Literal23" runat="server" Text="<%$ Resources:privacy21%>" />
                </strong>
            </div>
            <br />
            <div class="innerText">
                <asp:Literal ID="Literal24" runat="server" Text="<%$ Resources:privacy22%>" />
            </div>
            <br />
            <br />
            <div class="grayText">
                <strong>
                    <asp:Literal ID="Literal25" runat="server" Text="<%$ Resources:privacy23%>" />
                </strong>
            </div>
            <br />
            <div class="innerText">
                <asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:privacy24%>" />
                <br><br>
                <asp:Literal ID="Literal26a" runat="server" Text="<%$ Resources:privacy24a%>" />
            </div>
            <br />
            <div class="grayText">
                <strong>
                    <asp:Literal ID="Literal27" runat="server" Text="<%$ Resources:privacy25%>" />
                </strong>
            </div>
            <br />
            <div class="innerText">
                <asp:Literal ID="Literal28" runat="server" Text="<%$ Resources:privacy26%>" />
            </div>
            <br />
            <div class="blueText">
                <strong>
                    <asp:Literal ID="Literal29" runat="server" Text="<%$ Resources:privacy27%>" />
                </strong>
            </div>
            <br />
            <div class="innerText">
                <asp:Literal ID="Literal30" runat="server" Text="<%$ Resources:privacy28%>" />
                <br /><br />
            </div>
            <div class="innerText">
                <asp:Literal ID="Literal31" runat="server" Text="<%$ Resources:privacy29%>" />
            </div>
            <br />
            <div class="blueText">
                <strong>
                    <asp:Literal ID="Literal32" runat="server" Text="<%$ Resources:privacy30%>" />
                </strong>
            </div>
            <br />
            <div class="innerText">
                <asp:Literal ID="Literal33" runat="server" Text="<%$ Resources:privacy31%>" />
                <br />
            </div>
            <div class="innerText">
                <br />
                <strong>
                    <asp:Literal ID="Literal34" runat="server" Text="<%$ Resources:privacy32%>" />
                </strong>
                <br />
            </div>
            <div class="innerText">
                <asp:Literal ID="Literal35" runat="server" Text="<%$ Resources:privacy33%>" />
            </div>
            <div class="innerText">
                <asp:Literal ID="Literal36" runat="server" Text="<%$ Resources:privacy34%>" />
                <br />
            </div>
            <div class="innerText">
                <asp:Literal ID="Literal37" runat="server" Text="<%$ Resources:privacy35%>" />
            </div>
            <div class="innerText">
                <br />
                <asp:Literal ID="Literal38" runat="server" Text="<%$ Resources:privacy36%>" />
            </div>
            <div class="innerText">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> 
                <asp:Literal ID="Literal39" runat="server" Text="<%$ Resources:privacy37%>" />
            </div>
            <div class="innerText">
                <asp:Literal ID="Literal40" runat="server" Text="<%$ Resources:privacy38%>" />
            </div>
            <div class="innerText">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> 
                <asp:Literal ID="Literal41" runat="server" Text="<%$ Resources:privacy39%>" />
            </div>
            <div class="innerText">
                <br />
                <strong>
                    <asp:Literal ID="Literal42" runat="server" Text="<%$ Resources:privacy40%>" />
                </strong>
                <br />
            </div>
            <div class="innerText">
                <asp:Literal ID="Literal43" runat="server" Text="<%$ Resources:privacy41%>" />
            </div>
            <div class="innerText">
                <br />
                <strong>
                    <asp:Literal ID="Literal44" runat="server" Text="<%$ Resources:privacy42%>" />
                </strong>
                <br />
            </div>
            <div class="innerText">
                <asp:Literal ID="Literal45" runat="server" Text="<%$ Resources:privacy43%>" />
            </div>
            <br />
            <div class="blueText">
                <strong>
                    <asp:Literal ID="Literal46" runat="server" Text="<%$ Resources:privacy44%>" />
                </strong>
            </div>
            <br />
            <div class="innerText">
                <asp:Literal ID="Literal47" runat="server" Text="<%$ Resources:privacy45%>" />
                <br><br>
            </div>
            <div class="innerText">
                <asp:Literal ID="Literal47a" runat="server" Text="<%$ Resources:privacy45a%>" />
            </div>
            <div class="innerText">
                <asp:Literal ID="Literal48" runat="server" Text="<%$ Resources:privacy46%>" />
            </div>
            <br />
            <div class="blueText">
                <strong>
                    <asp:Literal ID="Literal49" runat="server" Text="<%$ Resources:privacy47%>" />
                </strong>
            </div>
            <br />
            <div class="innerText">
                <asp:Literal ID="Literal50" runat="server" Text="<%$ Resources:privacy48%>" />
                <br /><br />
            </div>
            <div class="innerText">
                <strong>
                    <asp:Literal ID="Literal51" runat="server" Text="<%$ Resources:privacy49%>" />
                </strong>
            </div>
            <div class="innerText">
                <asp:Literal ID="Literal52" runat="server" Text="<%$ Resources:privacy50%>" />
            </div>
            <div class="innerText">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>&bull;</strong> 
                <asp:Literal ID="Literal53" runat="server" Text="<%$ Resources:privacy51%>" />
            </div>
            <div class="innerText">
                <br />
                <asp:Literal ID="Literal54" runat="server" Text="<%$ Resources:privacy52%>" />
            </div>
        </div>
    </section>
</asp:Content>
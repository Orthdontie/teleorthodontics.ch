﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/General.Master" AutoEventWireup="true" CodeFile="how-it-works.aspx.cs" Inherits="Teleorthodontics.Web.how_it_works" Culture="Auto" UICulture="Auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="bodyattributes" runat="server">
    id="how-it-works" class="homepage"
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
    <title><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:title%>" /></title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="<asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:metadescription%>" />" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <section id="pricing" class="pricing text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-heading">
                        <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:how_it_works1%>" />
                    </h2>
                    <p>
                        <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:how_it_works2%>" />
                    </p>
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/step-1.jpg" alt="<asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:how_it_works3%>" />">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>
                                        <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:how_it_works4%>" />
                                    </h4>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/step-2.jpg" alt="<asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:how_it_works5%>" />">
                            </div>
                            <div class="timeline-panel inverted">
                                <div class="timeline-heading">
                                    <h4>
                                        <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:how_it_works6%>" />
                                    </h4>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/step-2a.jpg" alt="<asp:Literal ID="Literal7a" runat="server" Text="<%$ Resources:how_it_works5a%>" />">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4><asp:Literal ID="Literal8a" runat="server" Text="<%$ Resources:how_it_works6a%>" /></h4>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/step-3.jpg" alt="<asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:how_it_works7%>" />">
                            </div>
                            <div class="timeline-panel inverted">
                                <div class="timeline-heading">
                                    <h4>
                                        <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:how_it_works8%>" />
                                    </h4>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/step-4.jpg" alt="<asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:how_it_works9%>" />">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>
                                        <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:how_it_works10%>" />
                                    </h4>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/step-5.jpg" alt="<asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:how_it_works11%>" />">
                            </div>
                            <div class="timeline-panel inverted">
                                <div class="timeline-heading">
                                    <h4>
                                        <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:how_it_works12%>" />
                                    </h4>
                                    <a id="learnmoreoninternet20" href="<asp:Literal ID="Literal14a" runat="server" Text="<%$ Resources:how_it_works12a%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                        <asp:Literal ID="Literal14b" runat="server" Text="<%$ Resources:how_it_works12b%>" />
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/step-6.jpg" alt="<asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:how_it_works13%>" />">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>
                                        <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:how_it_works14%>" />
                                    </h4>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/step-7.jpg" alt="<asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:how_it_works15%>" />">
                            </div>
                            <div class="timeline-panel inverted">
                                <div class="timeline-heading">
                                    <h4>
                                        <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:how_it_works16%>" />
                                        <br />
                                        <asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:how_it_works17%>" />
                                    </h4>
                                    <a id="learnmoreoninternet21" href="<asp:Literal ID="Literal19a" runat="server" Text="<%$ Resources:how_it_works17a%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                        <asp:Literal ID="Literal19b" runat="server" Text="<%$ Resources:how_it_works17b%>" />
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/step-8.jpg" alt="<asp:Literal ID="Literal20" runat="server" Text="<%$ Resources:how_it_works18%>" />">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>
                                        <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:how_it_works19%>" />
                                    </h4>
                                    <a id="learnmoreoninternet21" href="<asp:Literal ID="Literal21a" runat="server" Text="<%$ Resources:how_it_works19a%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                        <asp:Literal ID="Literal21b" runat="server" Text="<%$ Resources:how_it_works19b%>" />
                                    </a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/step-9.jpg" alt="<asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:how_it_works20%>" />">
                            </div>
                            <div class="timeline-panel inverted">
                                <div class="timeline-heading">
                                    <h4>
                                        <asp:Literal ID="Literal23" runat="server" Text="<%$ Resources:how_it_works21%>" />
                                    </h4>
                                    <a id="learnmoreoninternet21" href="<asp:Literal ID="Literal23a" runat="server" Text="<%$ Resources:how_it_works21a%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                        <asp:Literal ID="Literal23b" runat="server" Text="<%$ Resources:how_it_works21b%>" />
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <a id="learnmore" href="Registration.aspx" class="btn btn-outline btn-xl page-scroll">
                        <asp:Literal ID="Literal24" runat="server" Text="<%$ Resources:how_it_works22%>" />
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Teleorthodontics.Web.DB;
using Teleorthodontics.Web.Helper;
using System.Configuration;
using System.Text;
using System.Drawing.Imaging;

namespace Teleorthodontics.Web
{
    public partial class Registration : System.Web.UI.Page
    {
        DatabaseHelper databaseHelper = new DatabaseHelper();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                this.Session["CaptchaImageText"] = CImage.GenerateRandomCode();
                hdnCaptchaCode.Value = this.Session["CaptchaImageText"].ToString();

                PopulateDayMonthYear();
                rptQAnswer.DataSource = GetQuestion();
                rptQAnswer.DataBind();

                try
                {
                    txtCountryCode.Text = ConfigurationManager.AppSettings["CountryCode"].ToString();
                }
                catch
                {
                }

                BindDropdowns();
            }


            // hdnCapchaCode.Value = this.Session["CaptchaImageText"].ToString(); ;
        }

        private DataTable GetQuestion()
        {

            String query = @"select * from TeleQuestionTable";
            DataSet ds = new DataSet();
            ds = databaseHelper.ExecuteDataSet(query, CommandType.Text);
            DataTable dt = ds.Tables[0];
            return dt;
        }

        public void PopulateDayMonthYear()
        {
            cmbday.Items.Add("dd");
            for (int i = 1; i <= 31; i++)
            {
                cmbday.Items.Add(Convert.ToString(i));
                //cmblstday.Items.Add(Convert.ToString(i));
            }
            cmbmnth.Items.Add("mm");
            for (int i = 1; i <= 12; i++)
            {
                cmbmnth.Items.Add(Convert.ToString(i));
                //cmblstmonth.Items.Add(Convert.ToString(i));
            }
            cmbyear.Items.Add("yyyy");
            for (int i = 1930; i <= Convert.ToInt16(DateTime.Now.Year.ToString()); i++)
            {
                cmbyear.Items.Add(Convert.ToString(i));
                //cmblstyy.Items.Add(Convert.ToString(i));
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                string Captcha = txtCaptcha.Text.Trim();
                if (Captcha == "")
                {
                    dvMsg.Visible = true;
                    lblMsg.Text = "Please enter captcha code";
                    lblMsg.ForeColor = Color.Red;
                    txtCaptcha.Attributes.CssStyle.Add("border-color", "red");
                }
                else
                {
                    dvMsg.Visible = false;
                    lblMsg.Text = "";
                    txtCaptcha.Attributes.CssStyle.Add("border-color", "");


                    string firstName = txtFirstName.Text.Trim();
                    string lastName = txtLastName.Text.Trim();
                    string email = txtEmail.Text.Trim();
                    string countryCode = txtCountryCode.Text.Trim();
                    string phone = txtPhone.Text.Trim();
                    string day = cmbday.Text.Trim();
                    string month = cmbmnth.Text.Trim();
                    string year = cmbyear.Text.Trim();
                    string dob = month + "/" + day + "/" + year;
                    string gender = cmbgender.Text.Trim();

                    string comments = txtComments.Text.Trim();

                    string ip = HttpContext.Current.Request.UserHostAddress;

                    string faceImage = string.Empty;
                    if (FileUploadFace.HasFile)
                        faceImage = FileUpload(FileUploadFace);
                    else if (!string.IsNullOrEmpty(hfFileUploadFace.Value) && hfFileUploadFace.Value.Length > 50)
                        faceImage = FileUploadBase64(hfFileUploadFace.Value);
                    else if (!string.IsNullOrEmpty(hfFileUploadFace.Value) && hfFileUploadFace.Value.Length <= 50)
                        faceImage = hfFileUploadFace.Value;

                    //Right Image//
                    string rightImage = string.Empty;
                    if (FileUploadRightside.HasFile)
                        rightImage = FileUpload(FileUploadRightside);
                    else if (!string.IsNullOrEmpty(hfFileUploadRightside.Value) && hfFileUploadRightside.Value.Length > 50)
                        rightImage = FileUploadBase64(hfFileUploadRightside.Value);
                    else if (!string.IsNullOrEmpty(hfFileUploadRightside.Value) && hfFileUploadRightside.Value.Length <= 50)
                        rightImage = hfFileUploadRightside.Value;

                    //Left Image//
                    string leftImage = string.Empty;
                    if (FileUploadLeftside.HasFile)
                        leftImage = FileUpload(FileUploadLeftside);
                    else if (!string.IsNullOrEmpty(hfFileUploadLeftside.Value) && hfFileUploadLeftside.Value.Length > 50)
                        leftImage = FileUploadBase64(hfFileUploadLeftside.Value);
                    else if (!string.IsNullOrEmpty(hfFileUploadLeftside.Value) && hfFileUploadLeftside.Value.Length <= 50)
                        leftImage = hfFileUploadLeftside.Value;

                    //Slightly Top Image//
                    string frontImage = string.Empty;
                    if (FileUploadFront.HasFile)
                        frontImage = FileUpload(FileUploadFront);
                    else if (!string.IsNullOrEmpty(hfFileUploadFront.Value) && hfFileUploadFront.Value.Length > 50)
                        frontImage = FileUploadBase64(hfFileUploadFront.Value);
                    else if (!string.IsNullOrEmpty(hfFileUploadFront.Value) && hfFileUploadFront.Value.Length <= 50)
                        frontImage = hfFileUploadFront.Value;

                    //Top Image//
                    string topImage = string.Empty;
                    if (FileUploadTopteeth.HasFile)
                        topImage = FileUpload(FileUploadTopteeth);
                    else if (!string.IsNullOrEmpty(hfFileUploadTopteeth.Value) && hfFileUploadTopteeth.Value.Length > 50)
                        topImage = FileUploadBase64(hfFileUploadTopteeth.Value);
                    else if (!string.IsNullOrEmpty(hfFileUploadTopteeth.Value) && hfFileUploadTopteeth.Value.Length <= 50)
                        topImage = hfFileUploadTopteeth.Value;

                    //Bottom Image//
                    string bottomImage = string.Empty;
                    if (FileUploadLowerteeth.HasFile)
                        bottomImage = FileUpload(FileUploadLowerteeth);
                    else if (!string.IsNullOrEmpty(hfFileUploadLowerteeth.Value) && hfFileUploadLowerteeth.Value.Length > 50)
                        bottomImage = FileUploadBase64(hfFileUploadLowerteeth.Value);
                    else if (!string.IsNullOrEmpty(hfFileUploadLowerteeth.Value) && hfFileUploadLowerteeth.Value.Length <= 50)
                        bottomImage = hfFileUploadLowerteeth.Value;

                    //Big Smile Image//
                    string bigSmileImage = string.Empty;
                    if (FileUploadBigSmile.HasFile)
                        bigSmileImage = FileUpload(FileUploadBigSmile);
                    else if (!string.IsNullOrEmpty(hfFileUploadBigSmile.Value) && hfFileUploadBigSmile.Value.Length > 50)
                        bigSmileImage = FileUploadBase64(hfFileUploadBigSmile.Value);
                    else if (!string.IsNullOrEmpty(hfFileUploadBigSmile.Value) && hfFileUploadBigSmile.Value.Length <= 50)
                        bigSmileImage = hfFileUploadBigSmile.Value;

                    //Mouth Open Image//
                    string mouthOpenImage = string.Empty;
                    if (FileUploadMouthOpen.HasFile)
                        mouthOpenImage = FileUpload(FileUploadMouthOpen);
                    else if (!string.IsNullOrEmpty(hfFileUploadMouthOpen.Value) && hfFileUploadMouthOpen.Value.Length > 50)
                        mouthOpenImage = FileUploadBase64(hfFileUploadMouthOpen.Value);
                    else if (!string.IsNullOrEmpty(hfFileUploadMouthOpen.Value) && hfFileUploadMouthOpen.Value.Length <= 50)
                        mouthOpenImage = hfFileUploadMouthOpen.Value;

                    //Mouth Closed Image//
                    string mouthClosedImage = string.Empty;
                    if (FileUploadMouthClosed.HasFile)
                        mouthClosedImage = FileUpload(FileUploadMouthClosed);
                    else if (!string.IsNullOrEmpty(hfFileUploadMouthClosed.Value) && hfFileUploadMouthClosed.Value.Length > 50)
                        mouthClosedImage = FileUploadBase64(hfFileUploadMouthClosed.Value);
                    else if (!string.IsNullOrEmpty(hfFileUploadMouthClosed.Value) && hfFileUploadMouthClosed.Value.Length <= 50)
                        mouthClosedImage = hfFileUploadMouthClosed.Value;

                    //Profile Image//
                    string profileImage = string.Empty;
                    if (FileUploadProfile.HasFile)
                        profileImage = FileUpload(FileUploadProfile);
                    else if (!string.IsNullOrEmpty(hfFileUploadProfile.Value) && hfFileUploadProfile.Value.Length > 50)
                        profileImage = FileUploadBase64(hfFileUploadProfile.Value);
                    else if (!string.IsNullOrEmpty(hfFileUploadProfile.Value) && hfFileUploadProfile.Value.Length <= 50)
                        profileImage = hfFileUploadProfile.Value;

                    //FileUploadPanoramicXRay
                    string PanoramicXRay = string.Empty;
                    if (FileUploadPanoramicXRay.HasFile)
                        PanoramicXRay = FileUpload(FileUploadPanoramicXRay);
                    else if (!string.IsNullOrEmpty(hfFileUploadPanoramicXRay.Value) && hfFileUploadPanoramicXRay.Value.Length > 50)
                        PanoramicXRay = FileUploadBase64(hfFileUploadPanoramicXRay.Value);
                    else if (!string.IsNullOrEmpty(hfFileUploadPanoramicXRay.Value) && hfFileUploadPanoramicXRay.Value.Length <= 50)
                        PanoramicXRay = hfFileUploadPanoramicXRay.Value;

                    //FileUploadLateralCeph
                    string LateralCeph = string.Empty;
                    if (FileUploadLateralCeph.HasFile)
                        LateralCeph = FileUpload(FileUploadLateralCeph);
                    else if (!string.IsNullOrEmpty(hfFileUploadLateralCeph.Value) && hfFileUploadLateralCeph.Value.Length > 50)
                        LateralCeph = FileUploadBase64(hfFileUploadLateralCeph.Value);
                    else if (!string.IsNullOrEmpty(hfFileUploadLateralCeph.Value) && hfFileUploadLateralCeph.Value.Length <= 50)
                        LateralCeph = hfFileUploadLateralCeph.Value;

                    Dictionary<string, string> imageList = new Dictionary<string, string>();
                    //if (!string.IsNullOrEmpty(faceImage))
                    //    imageList.Add("Face Image", faceImage);
                    //if (!string.IsNullOrEmpty(rightImage))
                    //    imageList.Add("Right Image", rightImage);
                    //if (!string.IsNullOrEmpty(leftImage))
                    //    imageList.Add("Left Image", leftImage);
                    //if (!string.IsNullOrEmpty(frontImage))
                    //    imageList.Add("Front Image", frontImage);
                    //if (!string.IsNullOrEmpty(topImage))
                    //    imageList.Add("Top Image", topImage);
                    //if (!string.IsNullOrEmpty(bottomImage))
                    //    imageList.Add("Bottom Image", bottomImage);
                    //if (!string.IsNullOrEmpty(bigSmileImage))
                    //    imageList.Add("Big Smile Image", bigSmileImage);
                    //if (!string.IsNullOrEmpty(mouthOpenImage))
                    //    imageList.Add("Mouth Open Image", mouthOpenImage);
                    //if (!string.IsNullOrEmpty(mouthClosedImage))
                    //    imageList.Add("Mouth Closed Image", mouthClosedImage);
                    //if (!string.IsNullOrEmpty(profileImage))
                    //    imageList.Add("Profile Image", profileImage);
                    //if (!string.IsNullOrEmpty(PanoramicXRay))
                    //    imageList.Add("Panoramic x-ray", PanoramicXRay);
                    //if (!string.IsNullOrEmpty(LateralCeph))
                    //    imageList.Add("Lateral Ceph", LateralCeph);

                    if (!string.IsNullOrEmpty(faceImage))
                        imageList.Add(GetLocalResourceObject("FaceImage").ToString(), faceImage);
                    if (!string.IsNullOrEmpty(rightImage))
                        imageList.Add(GetLocalResourceObject("RightImage").ToString(), rightImage);
                    if (!string.IsNullOrEmpty(leftImage))
                        imageList.Add(GetLocalResourceObject("LeftImage").ToString(), leftImage);
                    if (!string.IsNullOrEmpty(frontImage))
                        imageList.Add(GetLocalResourceObject("FrontImage").ToString(), frontImage);
                    if (!string.IsNullOrEmpty(topImage))
                        imageList.Add(GetLocalResourceObject("TopImage").ToString(), topImage);
                    if (!string.IsNullOrEmpty(bottomImage))
                        imageList.Add(GetLocalResourceObject("BottomImage").ToString(), bottomImage);
                    if (!string.IsNullOrEmpty(bigSmileImage))
                        imageList.Add(GetLocalResourceObject("BigSmileImage").ToString(), bigSmileImage);
                    if (!string.IsNullOrEmpty(mouthOpenImage))
                        imageList.Add(GetLocalResourceObject("MouthOpenImage").ToString(), mouthOpenImage);
                    if (!string.IsNullOrEmpty(mouthClosedImage))
                        imageList.Add(GetLocalResourceObject("MouthClosedImage").ToString(), mouthClosedImage);
                    if (!string.IsNullOrEmpty(profileImage))
                        imageList.Add(GetLocalResourceObject("ProfileImage").ToString(), profileImage);
                    if (!string.IsNullOrEmpty(PanoramicXRay))
                        imageList.Add(GetLocalResourceObject("Panoramicx-ray").ToString(), PanoramicXRay);
                    if (!string.IsNullOrEmpty(LateralCeph))
                        imageList.Add(GetLocalResourceObject("LateralCeph").ToString(), LateralCeph);

                    string User_Id = CommonClass.generatepatientcode();
                    string Password = CommonClass.GenerateRandomCode();


                    Teleorthodontics.Web.DB.Registration reg = new DB.Registration();
                    string age = (DateTime.Now.Year - Convert.ToInt32(cmbyear.Text)).ToString();


                    if (Session["languagechng"] == null)
                    {
                        Session["languagechng"] = "fr-FR";
                    }

                    string selectedCountry = drpCountry.SelectedItem != null ? drpCountry.SelectedItem.Text : "";
                    string user_Id = reg.AddRegistration(firstName, lastName, email, ip, phone, dob, gender, age, User_Id, Password, drpOfficeLocation.SelectedValue, txtAddress.Text, selectedCountry, drpState.SelectedValue, txtCity.Text, txtZipCode.Text, Convert.ToString(Session["languagechng"]));
                    Session["User_Id"] = User_Id;
                    var website = ConfigurationManager.AppSettings["WebsiteName"].ToString();
                    reg.AddOnlineConsultation(firstName + " " + lastName, email, phone, "", age, frontImage, rightImage, leftImage, frontImage, topImage, bottomImage, comments, ip, " ", bigSmileImage, mouthOpenImage, mouthClosedImage, profileImage, PanoramicXRay, LateralCeph, User_Id, website);

                    Dictionary<string, string> questionList = new Dictionary<string, string>();


                    string strQuery = "";
                    foreach (RepeaterItem item in rptQAnswer.Items)
                    {

                        HiddenField hdnQuestionId = item.FindControl("hdnQuestionId") as HiddenField;
                        string str = "";
                        RadioButtonList ddlAnswer = item.FindControl("ddlAnswer") as RadioButtonList;

                        Label lblQuestion = item.FindControl("lblQuestion") as Label;
                        if (ddlAnswer.SelectedValue == "True")
                        {
                            str = "Yes";
                        }
                        else if ((ddlAnswer.SelectedValue == "False"))
                        {
                            str = "No";
                        }
                        if (str != "")
                        {

                            if (strQuery == "")
                            {
                                strQuery = " Insert Into TeleQuestionAnswer(User_Id,Q_Id,Answer) values('" + Session["User_Id"] + "'," + hdnQuestionId.Value + ",'" + str + "')";
                            }
                            else
                            {
                                strQuery = strQuery + ",('" + Session["User_Id"] + "'," + hdnQuestionId.Value + ",'" + str + "')";
                            }
                            questionList.Add(lblQuestion.Text, str);
                        }
                    }

                    if (strQuery != "")
                    {
                        databaseHelper.ExecuteNonQuery(strQuery, CommandType.Text);
                    }

                    Sendmail.SendmailNewUser(User_Id, firstName, lastName, email, Password, comments, ip, imageList, questionList);

                    //dvMsg.Visible = true;
                    //lblMsg.Text = "Patient registration successfully completed . Please check your email";
                    //lblMsg.ForeColor = Color.Green;
                    //btnSubmit.Visible = false;

                    dvMsg.Visible = true;
                    lblMsg.Text = "Patient Registration Successfully. Please check your email";
                    lblMsg.ForeColor = Color.Green;

                    // Response.Redirect("Register.aspx");
                    txtCaptcha.Text = "";
                    this.Session["CaptchaImageText"] = null;

                }
            }
            catch (Exception ex)
            {
                dvMsg.Visible = true;
                lblMsg.Text = ex.Message + Environment.NewLine + (ex.InnerException != null ? ex.InnerException.Message : "");
                lblMsg.ForeColor = Color.Red;
            }
        }

        private string FileUpload(FileUpload fileUploadControl)
        {
            string uploadPath = AppDomain.CurrentDomain.BaseDirectory + "OnlineConsultaionImages//UserImages";
            if (!Directory.Exists(uploadPath))
                System.IO.Directory.CreateDirectory(uploadPath);

            string imageName = "";
            // Check file exist or not
            if (fileUploadControl.PostedFile != null)
            {
                string fileExt = System.IO.Path.GetExtension(fileUploadControl.PostedFile.FileName).ToString().ToLower();
                // Check the extension of image
                string extension = Path.GetExtension(fileUploadControl.FileName);
                if (extension.ToLower() == ".png" || extension.ToLower() == ".jpg")
                {
                    Stream strm = fileUploadControl.PostedFile.InputStream;

                    var image = ScaleImage(System.Drawing.Image.FromStream(strm), 400, 400);
                    using (image)
                    {
                        // Print Original Size of file (Height or Width) 
                        //Label1.Text = image.Size.ToString();
                        int newWidth = 400; // New Width of Image in Pixel
                        int newHeight = 240; // New Height of Image in Pixel
                        var thumbImg = new Bitmap(newWidth, newHeight);
                        var thumbGraph = Graphics.FromImage(thumbImg);
                        thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                        thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                        thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;

                        var imgRectangle = new Rectangle(0, 0, newWidth, newHeight);
                        thumbGraph.DrawImage(image, imgRectangle);
                        // Save the file

                        string savePath = HttpContext.Current.Server.MapPath(@"~\OnlineConsultaionImages\UserImages\");
                        imageName = System.Guid.NewGuid().ToString().Replace('-', '_') + fileExt;


                        // string targetPath = Server.MapPath(@"~\Images\") + fileUploadControl.FileName;
                        thumbImg.Save(savePath + imageName, image.RawFormat);
                        // Print new Size of file (height or Width)
                        //Label2.Text = thumbImg.Size.ToString();
                        //Show Image
                        //Image1.ImageUrl = @"~\Images\" + fileUploadControl.FileName;
                    }
                }
            }

            return imageName;
        }

        private string FileUploadBase64(string imageBase64String)
        {
            if (string.IsNullOrEmpty(imageBase64String))
            {
                return "";
            }

            string uploadPath = AppDomain.CurrentDomain.BaseDirectory + "OnlineConsultaionImages//UserImages";
            if (!Directory.Exists(uploadPath))
                System.IO.Directory.CreateDirectory(uploadPath);

            string imageName = "";

            string imageString = imageBase64String.Replace("data:image/png;base64,", "");

            byte[] imgByteArray = Convert.FromBase64String(imageString);
            Stream stream = new MemoryStream(imgByteArray);
            var image = ScaleImage(System.Drawing.Image.FromStream(stream), 400, 400);
            using (image)
            {
                int newWidth = 400; // New Width of Image in Pixel
                int newHeight = 240; // New Height of Image in Pixel
                var thumbImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;

                var imgRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imgRectangle);
                // Save the file

                string savePath = HttpContext.Current.Server.MapPath(@"~\OnlineConsultaionImages\UserImages\");
                imageName = System.Guid.NewGuid().ToString().Replace('-', '_') + ".png";
                thumbImg.Save(savePath + imageName, ImageFormat.Png);
            }

            return imageName;
        }

        public System.Drawing.Image ScaleImage(System.Drawing.Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);

            using (var graphics = Graphics.FromImage(newImage))
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);

            return newImage;
        }

        private void BindDropdowns()
        {
            drpCountry.Items.Clear();
            string countrysql = string.Empty;
            string ci = SessionManager.GetLanguage(null);

            if (ci.ToString() == "fr-FR")
            {
                countrysql = "fr_country";
            }
            else if (ci.ToString().ToLower() == "en-gb" || ci.ToString().ToLower() == "en-us")
            {
                countrysql = "country";
            }
            else if (ci.ToString() == "de-DE")
            {
                countrysql = "de_country";
            }
            else if (ci.ToString() == "it-IT")
            {
                countrysql = "it_country";
            }
            else if (ci.ToString() == "ru-RU")
            {
                countrysql = "ru_country";
            }
            else if (ci.ToString() == "es-ES")
            {
                countrysql = "es_country";
            }
            else
            {
                countrysql = "fr_country";
            }

            string query = "select Country_id, " + countrysql + " AS Country from dt_country where " + countrysql + "<> ''";
            DatabaseHelper databaseHelper = new DatabaseHelper();
            DataTable dtbl = databaseHelper.ExecuteDataSet(query, CommandType.Text).Tables[0];

            drpCountry.DataSource = dtbl;
            drpCountry.DataTextField = "Country";
            drpCountry.DataValueField = "Country_id";
            drpCountry.DataBind();
            drpCountry.Items.Insert(0, new ListItem("Select Country", ""));

            var defaultLang = ConfigurationManager.AppSettings["DefaultLanguage"].ToString();

            if (defaultLang.ToLower() == "en-gb" || defaultLang.ToLower() == "en-us")
                drpCountry.SelectedValue = "225";
            else
                drpCountry.SelectedValue = "204";


            PopulateState();
            PopulateLocation();
        }

        private void PopulateState()
        {
            drpState.Items.Clear();
            string query = "select distinct state from dt_state where country_id= '" + Convert.ToString(drpCountry.SelectedValue) + "' order by state ASC";
            DatabaseHelper databaseHelper = new DatabaseHelper();
            DataTable dtbl = databaseHelper.ExecuteDataSet(query, CommandType.Text).Tables[0];

            drpState.DataSource = dtbl;
            drpState.DataTextField = "state";
            drpState.DataValueField = "state";
            drpState.DataBind();
            drpState.Items.Insert(0, new ListItem("Select Canton", ""));
        }

        private void PopulateLocation()
        {
            drpOfficeLocation.Items.Clear();

            string websiteUrl = ConfigurationManager.AppSettings["WebsiteName"].ToString();
            int serviceID = 2;
            if (websiteUrl.ToLower().Contains("teleorthodontics"))
                serviceID = 2;
            else
                serviceID = 3;

            string query = "select Office_Id,Office_Name from Office_table where ServiceID = " + serviceID;
            DatabaseHelper databaseHelper = new DatabaseHelper();
            DataTable dtbl = databaseHelper.ExecuteDataSet(query, CommandType.Text).Tables[0];

            drpOfficeLocation.DataSource = dtbl;
            drpOfficeLocation.DataTextField = "Office_Name";
            drpOfficeLocation.DataValueField = "Office_Id";
            drpOfficeLocation.DataBind();
            drpOfficeLocation.Items.Insert(0, new ListItem("Select Location", ""));
        }
    }
}
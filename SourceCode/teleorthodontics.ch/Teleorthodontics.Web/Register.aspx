﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Teleorthodontics.Web.Register" %>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Teleorthodontics - Straighter teeth from your home comfort</title>

    <!-- Bootstrap Core CSS -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="lib/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="lib/device-mockups/device-mockups.min.css">

    <!-- Theme CSS -->
    <link href="css/main.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        header {
            color: #171010;
        }
    </style>

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="/"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                   <li>
                        <a class="page-scroll" href="how-it-works.aspx">How it works</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="pricing.aspx">Pricing</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="contact.aspx">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header class="contact">
        <form id="registrationForm" runat="server">
            <div class="container">
                <div class="row">
                    <div class="">
                        <br />
                        <br />
                        <br />

                        <h1><strong>Registration</strong> </h1>

                        <div runat="server" id="dvMsg" visible="false" class="col-md-6 col-md-offset-3">
                            <div class="alert alert-danger fade in">
                                <asp:Label ID="lblMsg" runat="server"></asp:Label>
                            </div>
                        </div>

                        <div id="dvError" style="display: none" class="col-md-6 col-md-offset-3">
                            <div class="alert alert-danger fade in">
                                <strong>
                                    <center> Please enter required field</center>
                                </strong>
                                <div id="dvErrorMsg"></div>

                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div class="col-md-12">
                            <asp:Repeater ID="rptQAnswer" runat="server" OnDataBinding="rptQAnswer_DataBinding" OnItemDataBound="rptQAnswer_ItemDataBound">
                                <HeaderTemplate>
                                    <table>
                                        <tr>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        
                                        <td ><br />
                                            <asp:HiddenField ID="hdnQuestionId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Q_Id")%>' />
                                                      <asp:HiddenField ID="hdnQType" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Q_Type")%>' />

                                            <span style="font-size: 20px;"><b><%# Container.ItemIndex + 1 %>
                                       . <%# Eval("Q_Name")%></b></span>
                                            <br />
                                            <%# Eval("Q_Description")%>


                                            <table runat="server"   Visible='<%#Eval("Q_Type").ToString() == "1" %>'  >
                                                <tr>
                                                    <td>
                                                        <asp:RadioButtonList   ID="ddlAnswer" runat="server" CssClass="form-control"   RepeatDirection="Horizontal">
                                                            <asp:ListItem  Text="Yes" style='margin-right:30px;' Value="True" />
                                                            <asp:ListItem Text="No" style="margin-right:30px" Value="False" />
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>

                                                 <table runat="server"  Visible='<%#Eval("Q_Type").ToString() == "2"%>'>
                                                <tr>
                                                    <td>
                                                         <asp:RadioButtonList   ID="RadioButtonList1" runat="server"    RepeatDirection="Horizontal">
                                                            <asp:ListItem  Text="I have 1 or 3 teeth that need to be moved" style='margin-right:30px;' Value="1" />
                                                            <asp:ListItem Text="I just want to improve my style. I do not need perfection" style="margin-right:30px; " Value="2" />
                                                             <asp:ListItem Text="I want the most ideal smile possible" style="margin-right:30px;" Value="3"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>

                                             <table runat="server"  Visible='<%#Eval("Q_Type").ToString() == "3" %>'>
                                                <tr>
                                                    <td>
                                                      <asp:TextBox ID="txtMessage" CssClass="form-control" TextMode="MultiLine" Rows="4" placeholder="Type your answer here" Columns="140" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                          

                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>


                            <div class="form-group">
                                <div class="col-sm-offset-1 col-sm-10"><br />
                                    <asp:Button ID="btnSubmit" class="btn btn-primary form-group" OnClientClick="return Validate()" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <br />
                <br />
            </div>
        </form>
    </header>


    <footer>
        <div class="container">
            <p>&copy; 2017 Teleorthodontics.ch. All Rights Reserved.</p>
            <ul class="list-inline">
                <li>
                    <a href="/privacy.aspx">Privacy</a>
                </li>
                <li>
                    <a href="/terms.aspx">Terms</a>
                </li>
                <li>
                    <a href="/faq.aspx">FAQ</a>
                </li>
            </ul>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="lib/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/new-age.min.js"></script>

</body>

</html>

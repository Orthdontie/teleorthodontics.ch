﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace Teleorthodontics.Web
{
    /// <summary>
    /// Summary description for Uploader
    /// </summary>
    public class Uploader : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            HttpFileCollection files = HttpContext.Current.Request.Files;
            string fileName = "";
            for (int index = 0; index < files.Count; index++)
            {
                HttpPostedFile uploadfile = files[index];
                fileName = FileUpload(uploadfile);
            }

            HttpContext.Current.Response.Write(fileName);
        }

        private string FileUpload(HttpPostedFile fileUploadControl)
        {
            string uploadPath = AppDomain.CurrentDomain.BaseDirectory + "OnlineConsultaionImages//UserImages";
            if (!Directory.Exists(uploadPath))
                System.IO.Directory.CreateDirectory(uploadPath);

            string imageName = "";
            // Check file exist or not
            if (fileUploadControl != null)
            {
                string fileExt = System.IO.Path.GetExtension(fileUploadControl.FileName).ToString().ToLower();
                // Check the extension of image
                string extension = Path.GetExtension(fileUploadControl.FileName);
                if (extension.ToLower() == ".png" || extension.ToLower() == ".jpg")
                {
                    Stream strm = fileUploadControl.InputStream;

                    var image = ScaleImage(System.Drawing.Image.FromStream(strm), 400, 400);
                    using (image)
                    {
                        int newWidth = 400; // New Width of Image in Pixel
                        int newHeight = 240; // New Height of Image in Pixel
                        var thumbImg = new Bitmap(newWidth, newHeight);
                        var thumbGraph = Graphics.FromImage(thumbImg);
                        thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                        thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                        thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;

                        var imgRectangle = new Rectangle(0, 0, newWidth, newHeight);
                        thumbGraph.DrawImage(image, imgRectangle);
                        // Save the file

                        string savePath = HttpContext.Current.Server.MapPath(@"~\OnlineConsultaionImages\UserImages\");
                        imageName = System.Guid.NewGuid().ToString().Replace('-', '_') + fileExt;
                        
                        thumbImg.Save(savePath + imageName, image.RawFormat);
                    }
                }
            }

            return imageName;
        }
        
        public System.Drawing.Image ScaleImage(System.Drawing.Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);

            using (var graphics = Graphics.FromImage(newImage))
                graphics.DrawImage(image, 0, 0, newWidth, newHeight);

            return newImage;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
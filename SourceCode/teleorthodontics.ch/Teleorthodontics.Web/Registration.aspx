﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/General2.Master" AutoEventWireup="true" CodeFile="registration.aspx.cs" Inherits="Teleorthodontics.Web.Registration" Culture="Auto" UICulture="Auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="bodyattributes" runat="server">
    id="page-top"
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
    <title><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:title%>" /></title>
    <style type="text/css">
        /*#modelPhoto {
            width: 350px !important;
        }*/

        #progress {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 99;
            background-color: #f6f6f6;
            color: black;
            font-weight: bolder;
            margin-top: 20%;
            padding-top: 20%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="<asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:metadescription%>" />" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <div id="modelPhoto" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Take Photo</h5>
        <button type="button" class="close" onclick="CloseModel();">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <video id="video" width="310" height="180" autoplay style="border: 1px solid green;"></video><br />
        <canvas id="canvas" width="310" height="180" style="border: 1px solid green;"></canvas>
      </div>
      <div class="modal-footer text-center">
        <button type="button" id="BtnCapture" class="btn btn-primary">Capture</button>
          <button type="button" id="btnRetake" class="btn btn-primary">Re-take</button>
        <button type="button" class="btn btn-secondary" onclick="CloseModel();">Close</button>
      </div>
    </div>
  </div>
</div>
    <section id="registration-wrapper" class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>
                        <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:registration1%>" />
                    </h1>

                    <div runat="server" id="dvMsg" visible="false" class="col-md-6 col-md-offset-3">
                        <div class="alert alert-danger fade in">
                            <asp:Label ID="lblMsg" ClientIDMode="Static" runat="server"></asp:Label>
                        </div>
                    </div>


                    <div id="dvError" style="display: none" class="col-md-6 col-md-offset-3">
                        <div class="alert alert-danger fade in">
                            <strong>
                                <center> 
                                    <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:registration2%>" />
                                </center>
                            </strong>
                            <div id="dvErrorMsg"></div>

                        </div>
                    </div>
                    <br />
                    <br />

                    <asp:HiddenField ID="hdnCaptchaCode" runat="server" />
                    <div id="dvFirst">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <asp:Label ID="lblFirstName" Text="<%$ Resources:registration3%>" CssClass="col-sm-3" runat="server"></asp:Label>
                                <div class="col-sm-9 form-group">
                                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" placeholder="<%$ Resources:registration4%>"></asp:TextBox>
                                    <span id="spnFirstName" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-md-6 form-group">
                                <asp:Label ID="lblLastName" Text="<%$ Resources:registration5%>" runat="server" CssClass="col-sm-3"></asp:Label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" placeholder="<%$ Resources:registration6%>"></asp:TextBox>
                                    <span id="spnLastName" style="color: red;"></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6 form-group">
                                <asp:Label ID="lblEmail" Text="<%$ Resources:registration7%>" runat="server" CssClass="col-sm-3"></asp:Label>
                                <div class="col-sm-9">
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="<%$ Resources:registration8%>"></asp:TextBox>
                                    <span id="spnEmail" style="color: red;"></span>
                                </div>
                            </div>

                            <div class="col-md-6 form-group">
                                <asp:Label ID="lblphone" Text="<%$ Resources:registration9%>" runat="server" CssClass="col-sm-3"></asp:Label>
                                <div class="col-sm-1" style="padding-right: 0; text-align: right; font-size: 20px">+</div>
                                <div class="col-sm-1" style="padding-left: 0; padding-right: 0">

                                    <asp:TextBox ID="txtCountryCode" runat="server" MaxLength="2" CssClass="form-control" onkeypress="return numbersonly(event)" placeholder="<%$ Resources:registration10%>" Text="41"></asp:TextBox>
                                </div>
                                <div class="col-sm-7 form-group" style="padding-left: 0;">
                                    <asp:TextBox ID="txtPhone" runat="server" MaxLength="10" CssClass="form-control" placeholder="<%$ Resources:registration11%>" onkeypress="return numbersonly(event)"></asp:TextBox>
                                    <span id="spnPhone" style="color: red;"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6 form-group">
                                <asp:Label ID="lblDOB" Text="<%$ Resources:registration12%>" runat="server" CssClass="col-sm-3"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="cmbday" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="cmbmnth" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="cmbyear" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6 form-group">
                                <asp:Label ID="lblGender" Text="<%$ Resources:registration13%>" runat="server" CssClass="col-sm-3"></asp:Label>
                                <div class="col-sm-9">
                                    <asp:DropDownList ID="cmbgender" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                        <asp:ListItem Value="Male" Text="Male"></asp:ListItem>
                                        <asp:ListItem Value="Female" Text="Female"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6 form-group">
                                <asp:Label ID="Label1" Text="<%$ Resources:AddressLabel%>" runat="server" CssClass="col-sm-3"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="drpCountry" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    
                                </div>
                                <div class="col-sm-5">
                                    <asp:TextBox ID="txtAddress" TextMode="MultiLine" placeholder="Address" runat="server" CssClass="form-control">
                                    </asp:TextBox>
                                    
                                </div>
                                
                            </div>
                            <div class="col-md-6 form-group">
                                <asp:Label ID="Label3" Text="<%$ Resources:CityLabel%>" runat="server" CssClass="col-sm-2"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtCity" runat="server"  placeholder="City" CssClass="form-control">
                                    </asp:TextBox>                                    
                                </div>
                                <asp:Label ID="Label2" Text="<%$ Resources:StateLabel%>" runat="server" CssClass="col-sm-1"></asp:Label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="drpState" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtZipCode" runat="server" placeholder="Zip" CssClass="form-control">
                                    </asp:TextBox>
                                </div>                                
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6 form-group">
                                <asp:Label ID="Label4" Text="<%$ Resources:OfficeLocation%>" runat="server" CssClass="col-sm-3"></asp:Label>
                                <div class="col-sm-5">
                                     <asp:DropDownList ID="drpOfficeLocation" ClientIDMode="Static" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 form-group">
                                <h2 class="pd10">
                                    <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:registration14%>" />
                                </h2>
                                <p class="marg-top">
                                    <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:registration15%>" />
                                </p>
                            </div>

                        </div>


                        <div class="form-group">
                            <div class="col-md-6">
                                <h3 class="ullisthdr1">
                                    <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:registration16%>" />
                                </h3>
                                <label id="lblFace">
                                    <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:registration17%>" />
                                </label>
                                <span id="spnFace" style="color: red;"></span>
                                <div class="row">
                                    <div class="col-md-6 text-right">  
                                        
                                        <button type="button" onclick="StartCamera('FileUploadFace','btnFileUploadFace','hfFileUploadFace','lblMsg')" 
                                            id="btnFileUploadFace" class="btn btn-primary btn-sm">
                                          Camera
                                        </button>
                                         <asp:HiddenField ID="hfFileUploadFace" ClientIDMode="Static" runat="server" />
                                    </div>
                                    <div class="col-md-6">
                                        <asp:FileUpload ClientIDMode="Static" ID="FileUploadFace" capture="capture" onchange="ValidateFileUpload(this)" runat="server" />
                                    </div>
                                </div>                                
                                
                                <asp:Image ID="ImageSample1" AlternateText="<%$ Resources:registration18%>" class="MRbottom" runat="server" Height="142px" ImageUrl="~/OnlineConsultaionImages/front.jpg" Width="250px" />

                                <p class="Aeroundrimg">
                                    <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:registration19%>" />
                                </p>
                            </div>
                            <div class="col-md-6">
                                <h3 class="ullisthdr1">
                                    <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:registration20%>" />
                                </h3>
                                <label id="lblRightside">
                                    <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:registration21%>" />
                                </label>
                                <span id="spnRightside" style="color: red;"></span>
                               
                                <div class="row">
                                    <div class="col-md-6 text-right">
                                         <button type="button" onclick="StartCamera('FileUploadRightside','btnFileUploadRightside','hfFileUploadRightside','lblMsg')" 
                                            id="btnFileUploadRightside" class="btn btn-primary btn-sm">
                                          Camera
                                        </button>
                                         <asp:HiddenField ClientIDMode="Static" ID="hfFileUploadRightside" runat="server" />
                                    </div>
                                    <div class="col-md-6">
                                        <asp:FileUpload ClientIDMode="Static" capture="capture" ID="FileUploadRightside" onchange="ValidateFileUpload(this)" runat="server" />
                                    </div>
                                </div>

                                <asp:Image ID="Image1" AlternateText="<%$ Resources:registration22%>" class="MRbottom" runat="server" Height="142px" ImageUrl="~/OnlineConsultaionImages/Right.jpg" Width="250px" />
                                <p class="Aeroundrimg">
                                    <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:registration23%>" />
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <h3 class="ullisthdr1">
                                    <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:registration24%>" />
                                </h3>
                                <label id="lblLeftside">
                                    <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:registration25%>" />
                                </label>
                                <span id="spnLeftside" style="color: red;"></span>
                                
                                <div class="row">
                                    <div class="col-md-6 text-right">
                                        <button type="button" onclick="StartCamera('FileUploadLeftside','btnFileUploadLeftside','hfFileUploadLeftside','lblMsg')" 
                                            id="btnFileUploadLeftside" class="btn btn-primary btn-sm">
                                          Camera
                                        </button>
                                         <asp:HiddenField ClientIDMode="Static" ID="hfFileUploadLeftside" runat="server" />
                                    </div>
                                    <div class="col-md-6">
                                        <asp:FileUpload ClientIDMode="Static" capture="capture" ID="FileUploadLeftside" onchange="ValidateFileUpload(this)" runat="server" />
                                    </div>
                                </div>
                                


                                <asp:Image ID="ImageSample2" AlternateText="<%$ Resources:registration26%>" class="MRbottom" runat="server" Height="142px" ImageUrl="~/OnlineConsultaionImages/Left.jpg" Width="250px" />

                                <p class="Aeroundrimg">
                                    <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:registration26a%>" />
                                </p>
                            </div>
                            <div class="col-md-6">
                                <h3 class="ullisthdr1">
                                    <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:registration27%>" />
                                </h3>
                                <label id="lblFront">
                                    <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:registration28%>" />
                                </label>
                                <span id="spnFront" style="color: red;"></span>
                                 <div class="row">
                                    <div class="col-md-6 text-right">
                                        <button type="button" onclick="StartCamera('FileUploadFront','btnFileUploadFront','hfFileUploadFront','lblMsg')" 
                                            id="btnFileUploadFront" class="btn btn-primary btn-sm">
                                          Camera
                                        </button>
                                         <asp:HiddenField ClientIDMode="Static" ID="hfFileUploadFront" runat="server" />
                                    </div>
                                    <div class="col-md-6">
                                        <asp:FileUpload ClientIDMode="Static" capture="capture" ID="FileUploadFront" onchange="ValidateFileUpload(this)" runat="server" />
                                    </div>
                                </div>
                                

                                <asp:Image ID="ImageSample3" AlternateText="<%$ Resources:registration29%>" class="MRbottom" runat="server" Height="142px" ImageUrl="~/OnlineConsultaionImages/slightly.jpg" Width="250px" />

                                <p class="Aeroundrimg">
                                    <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:registration30%>" />
                                </p>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="col-md-6 row">
                                <h3 class="ullisthdr1">
                                    <asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:registration31%>" />
                                </h3>
                                <label id="lblTopteeth">
                                    <asp:Literal ID="Literal20" runat="server" Text="<%$ Resources:registration32%>" />
                                </label>
                                <span id="spnTopteeth" style="color: red;"></span>

                                 <div class="row">
                                    <div class="col-md-6 text-right">
                                        <button type="button" onclick="StartCamera('FileUploadTopteeth','btnFileUploadTopteeth','hfFileUploadTopteeth','lblMsg')" 
                                            id="btnFileUploadTopteeth" class="btn btn-primary btn-sm">
                                          Camera
                                        </button>
                                         <asp:HiddenField ClientIDMode="Static" ID="hfFileUploadTopteeth" runat="server" />
                                    </div>
                                    <div class="col-md-6">
                                        <asp:FileUpload ClientIDMode="Static" capture="capture" ID="FileUploadTopteeth" onchange="ValidateFileUpload(this)" runat="server" />
                                    </div>
                                </div>
                                
                                <asp:Image ID="ImageSample4" AlternateText="<%$ Resources:registration33%>" class="MRbottom" runat="server" Height="142px" ImageUrl="~/OnlineConsultaionImages/Top.jpg" Width="250px" />
                                
                                <p class="Aeroundrimg">
                                    <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:registration34%>" />
                                </p>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <h3 class="ullisthdr1">
                                        <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:registration35%>" />
                                    </h3>
                                    <label id="lblLowerteeth">
                                        <asp:Literal ID="Literal23" runat="server" Text="<%$ Resources:registration36%>" />
                                    </label>
                                    <span id="spnLowerteeth" style="color: red;"></span>
                                
                                    <div class="row">
                                        <div class="col-md-6 text-right">
                                            <button type="button" onclick="StartCamera('FileUploadLowerteeth','btnFileUploadLowerteeth','hfFileUploadLowerteeth','lblMsg')" 
                                                id="btnFileUploadLowerteeth" class="btn btn-primary btn-sm">
                                              Camera
                                            </button>
                                             <asp:HiddenField ClientIDMode="Static" ID="hfFileUploadLowerteeth" runat="server" />
                                        </div>
                                        <div class="col-md-6">
                                             <asp:FileUpload ClientIDMode="Static" capture="capture" ID="FileUploadLowerteeth" onchange="ValidateFileUpload(this)" runat="server" />
                                        </div>
                                    </div>

                                    <asp:Image ID="ImageSample5" AlternateText="<%$ Resources:registration37%>" class="MRbottom" runat="server" Height="142px" ImageUrl="~/OnlineConsultaionImages/bottom.jpg" Width="250px" />
                                    <p class="Aeroundrimg">
                                        <asp:Literal ID="Literal24" runat="server" Text="<%$ Resources:registration37a%>" />
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <h3 class="ullisthdr1">
                                    <asp:Literal ID="Literal25" runat="server" Text="<%$ Resources:registration38%>" />
                                </h3>
                                <span id="spnBigSmile" style="color: red;"></span>
                                
                                <div class="row">
                                    <div class="col-md-6 text-right">
                                        <button type="button" onclick="StartCamera('FileUploadBigSmile','btnFileUploadBigSmile','hfFileUploadBigSmile','lblMsg')" 
                                                id="btnFileUploadBigSmile" class="btn btn-primary btn-sm">
                                              Camera
                                            </button>
                                         <asp:HiddenField ClientIDMode="Static" ID="hfFileUploadBigSmile" runat="server" />
                                    </div>
                                    <div class="col-md-6">
                                         <asp:FileUpload ClientIDMode="Static" capture="capture" ID="FileUploadBigSmile" onchange="ValidateFileUpload(this)" runat="server" />
                                    </div>
                                </div>
                                                                
                                <asp:Image ID="Image2" AlternateText="<%$ Resources:registration39%>" class="MRbottom" runat="server" Height="142px" ImageUrl="~/OnlineConsultaionImages/BigSmile.jpg" />
                                <p class="Aeroundrimg">
                                    <asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:registration40%>" />
                                </p>
                            </div>
                            <div class="col-md-6">
                                <h3 class="ullisthdr1">
                                    <asp:Literal ID="Literal26a" runat="server" Text="<%$ Resources:registration40a%>" />
                                </h3>
                                <span id="spnMouthOpen" style="color: red;"></span>
                                
                                <div class="row">
                                    <div class="col-md-6 text-right">
                                        <button type="button" onclick="StartCamera('FileUploadMouthOpen','btnFileUploadMouthOpen','hfFileUploadMouthOpen','lblMsg')" 
                                                id="btnFileUploadMouthOpen" class="btn btn-primary btn-sm">
                                              Camera
                                            </button>
                                         <asp:HiddenField ClientIDMode="Static" ID="hfFileUploadMouthOpen" runat="server" />
                                    </div>
                                    <div class="col-md-6">
                                         <asp:FileUpload ClientIDMode="Static" ID="FileUploadMouthOpen" onchange="ValidateFileUpload(this)" runat="server" />
                                    </div>
                                </div>

                                <asp:Image ID="Image3" AlternateText="<%$ Resources:registration41%>" class="MRbottom" runat="server" Height="142px" ImageUrl="~/OnlineConsultaionImages/MouthOpen.jpg" />
                                <p class="Aeroundrimg">
                                    <asp:Literal ID="Literal27" runat="server" Text="<%$ Resources:registration41a%>" />
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <h3 class="ullisthdr1">
                                    <asp:Literal ID="Literal28" runat="server" Text="<%$ Resources:registration42%>" />
                                </h3>
                                <span id="spnMouthClosed" style="color: red;"></span>
                                
                                 <div class="row">
                                    <div class="col-md-6 text-right">
                                        <button type="button" onclick="StartCamera('FileUploadMouthClosed','btnFileUploadMouthClosed','hfFileUploadMouthClosed','lblMsg')" 
                                                id="btnFileUploadMouthClosed" class="btn btn-primary btn-sm">
                                              Camera
                                            </button>
                                         <asp:HiddenField ClientIDMode="Static" ID="hfFileUploadMouthClosed" runat="server" />
                                    </div>
                                    <div class="col-md-6">
                                        <asp:FileUpload ClientIDMode="Static" capture="capture" ID="FileUploadMouthClosed" onchange="ValidateFileUpload(this)" runat="server" />
                                    </div>
                                </div>

                                <asp:Image ID="Image4" AlternateText="<%$ Resources:registration43%>" class="MRbottom" runat="server" Height="142px" ImageUrl="~/OnlineConsultaionImages/MouthClosed.jpg" />
                                <p class="Aeroundrimg">
                                    <asp:Literal ID="Literal29" runat="server" Text="<%$ Resources:registration44%>" />
                                </p>
                            </div>
                            <div class="col-md-6">
                                <h3 class="ullisthdr1">
                                    <asp:Literal ID="Literal29a" runat="server" Text="<%$ Resources:registration44a%>" />
                                </h3>
                                <span id="spnProfile" style="color: red;"></span>
                                
                                 <div class="row">
                                    <div class="col-md-6 text-right">
                                         <button type="button" onclick="StartCamera('FileUploadProfile','btnFileUploadProfile','hfFileUploadProfile','lblMsg')" 
                                                id="btnFileUploadProfile" class="btn btn-primary btn-sm">
                                              Camera
                                         </button>
                                         <asp:HiddenField ClientIDMode="Static" ID="hfFileUploadProfile" runat="server" />
                                    </div>
                                    <div class="col-md-6">
                                        <asp:FileUpload ClientIDMode="Static" capture="capture" ID="FileUploadProfile" onchange="ValidateFileUpload(this)" runat="server" />
                                    </div>
                                </div>

                                <asp:Image ID="Image5" AlternateText="<%$ Resources:registration45%>" class="MRbottom" runat="server" Height="142px" ImageUrl="~/OnlineConsultaionImages/Profile3.jpg" />
                                <p class="Aeroundrimg">
                                    <asp:Literal ID="Literal30" runat="server" Text="<%$ Resources:registration46%>" />
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <h3 class="ullisthdr1">
                                    <b> 
                                        <asp:Literal ID="Literal31" runat="server" Text="<%$ Resources:registration47%>" />
                                    </b>
                                </h3>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <h3 class="ullisthdr1">
                                    <asp:Literal ID="Literal32" runat="server" Text="<%$ Resources:registration48%>" />
                                </h3>
                                <span id="spnPanoramicXRay" style="color: red;"></span>
                                
                                 <div class="row">
                                    <div class="col-md-6 text-right">
                                        <button type="button" onclick="StartCamera('FileUploadPanoramicXRay','btnFileUploadPanoramicXRay','hfFileUploadPanoramicXRay','lblMsg')" 
                                                id="btnFileUploadPanoramicXRay" class="btn btn-primary btn-sm">
                                              Camera
                                         </button>
                                         <asp:HiddenField ID="hfFileUploadPanoramicXRay" ClientIDMode="Static" runat="server" />
                                    </div>
                                    <div class="col-md-6">
                                       <asp:FileUpload ClientIDMode="Static" capture="capture" ID="FileUploadPanoramicXRay" onchange="ValidateFileUpload(this)" runat="server" />
                                    </div>
                                </div>


                                <asp:Image ID="Image6" AlternateText="<%$ Resources:registration49%>" class="MRbottom" runat="server" Height="142px" ImageUrl="~/OnlineConsultaionImages/panoramic.PNG" />
                                <p class="Aeroundrimg">
                                    <asp:Literal ID="Literal33" runat="server" Text="<%$ Resources:registration49a%>" />
                                </p>
                            </div>
                            <div class="col-md-6">
                                <h3 class="ullisthdr1">
                                    <asp:Literal ID="Literal34" runat="server" Text="<%$ Resources:registration50%>" />
                                </h3>
                                <span id="spnLateralCeph" style="color: red;"></span>
                                                                
                                 <div class="row">
                                    <div class="col-md-6 text-right">
                                        <button type="button" onclick="StartCamera('FileUploadLateralCeph','btnFileUploadLateralCeph','hfFileUploadLateralCeph','lblMsg')" 
                                                id="btnFileUploadLateralCeph" class="btn btn-primary btn-sm">
                                              Camera
                                         </button>
                                         <asp:HiddenField ClientIDMode="Static" ID="hfFileUploadLateralCeph" runat="server" />
                                    </div>
                                    <div class="col-md-6">
                                       <asp:FileUpload ClientIDMode="Static" capture="capture" ID="FileUploadLateralCeph" onchange="ValidateFileUpload(this)" runat="server" />
                                    </div>
                                </div>
                                
                                <asp:Image ID="Image7" AlternateText="<%$ Resources:registration51%>" class="MRbottom" runat="server" Height="142px" ImageUrl="~/OnlineConsultaionImages/lateralCeph.PNG" />
                                <p class="Aeroundrimg">
                                    <asp:Literal ID="Literal35" runat="server" Text="<%$ Resources:registration51a%>" />
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-1 col-sm-10">
                                <br />
                                <input id="btnNext" type="button" value="Next" onclick="return ValidateFirst()" class="btn btn-primary" />

                            </div>
                        </div>
                    </div>
                    <div id="dvSecond" style="display:none">
                        <div class="form-group">
                            <div class="col-md-12">
                                <asp:Repeater ID="rptQAnswer" runat="server">
                                    <HeaderTemplate>
                                        <table id="radioquestions">
                                            <tr>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>

                                                <asp:HiddenField ID="hdnQuestionId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Q_id")%>' />

                                                <span style="font-size: 20px;"><b><%# Container.ItemIndex + 1 %>.
                                                </b></span><b>
                                                    <asp:Label ID="lblQuestion" runat="server" Text='<%# Eval("Q_Name")%>'></asp:Label>
                                                </b>

                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButtonList ID="ddlAnswer" runat="server" CssClass="form-control" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="<%$ Resources:registration51b%>" style='margin-right: 30px;' Value="True" />
                                                                <asp:ListItem Text="<%$ Resources:registration51c%>" style="margin-right: 30px" Value="False" />
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12">
                                <p class="ppd10">
                                    <asp:Literal ID="Literal36" runat="server" Text="<%$ Resources:registration52%>" />
                                </p>
                                <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Height="200px" Width="411px" CssClass="form-control"></asp:TextBox><br />
                                <%--<asp:Image ID="imgCaptcha" AlternateText="imgCaptcha" runat="server" Style="display: none" Height="30px" />--%>
                                <asp:Image ID="imgCaptcha" AlternateText="imgCaptcha" runat="server" ImageUrl="~/CImage.aspx" /><br />
                            </div>
                        </div>
                         <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-offset-4 col-md-4">
                                    <asp:Literal ID="Literal38" runat="server" Text="<%$ Resources:registration54%>" />
                                    <asp:TextBox ID="txtCaptcha" runat="server" CssClass="form-control"></asp:TextBox>
                                    <span id="spnCaptcha" style="color: red;"></span>
                                </div>
                                </div>
                            
                             </div>
                         <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-offset-2 col-md-8">
                                    <asp:Literal ID="ltrlTermConfirmation" runat="server" Text="<%$ Resources:TermConfirmationMessage %>"></asp:Literal>
                                    <br />
                                    <p>
                                    <%--<input type="checkbox" id="chkTermandConditions" />--%>
                                    <%--<a href="javascript:void" onclick="openwindowpassword();">--%>
                                    <a href="Terms.aspx" target="_blank">
                                        <asp:Literal ID="Literal37" runat="server" Text="<%$ Resources:registration53%>" />
                                    </a><br />
                                    <span id="spnTermandConditions" style="color: red;"></span>
                                </p>
                                    </div>
                                </div>
                             </div>
                        <div class="form-group">
                            <div class="col-sm-offset-1 col-sm-10">
                                 <br />
                                <input type="button" id="btnBack" onclick="BackPreview()" value="<asp:Literal ID="Literal38a" runat="server" Text="<%$ Resources:registration55%>" />" class="btn btn-primary" />
                                <asp:Button ID="btnSubmit" class="btn btn-primary" OnClientClick="return Validate()" runat="server" Text="<%$ Resources:registration56%>" OnClick="btnSubmit_Click" />
                                 <%--OnClientClick="return Validate()"--%>
                            </div>
                        </div>
                    </div>


            </div>
        </div>
        <br />
        <br />
        <br />
        <br />

            <div id="progress" style="display:none"></div>
    </section>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="footerscripts" runat="server">
    <link href="lib/loading/HoldOn.min.css" rel="stylesheet" />
    <script src="lib/loading/HoldOn.min.js"></script>
    <script type="text/javascript">
        function FetchingData() {
            var options = {
                message: "<asp:Literal ID="Literal39" runat="server" Text="<%$ Resources:registration57%>" />"
        };
        HoldOn.open(options);
        }

        

        function CloseLoading() {
            HoldOn.close();
        }

        function SavingImage() {
            var options = { message: "Saving the image."};
            HoldOn.open(options);
        }

        function ValidateFirst() {

            $("#dvError").css("display", "none");
            var IsError = false;
            var errorMsg = "<br/>";
            var firstName = $("#ContentPlaceHolderBody_txtFirstName");
            if (firstName.val().trim() == "") {
                firstName.css("border-color", "red");
                // errorMsg += "Please enter first name.<br/>";
                //  document.getElementById("spnFirstName").text = "Please Enter";
                $("#spnFirstName").text("<asp:Literal ID="Literal40" runat="server" Text="<%$ Resources:registration58%>" />");
                IsError = true;
            }
            else {
                firstName.css("border-color", "");
                $("#spnFirstName").text("");
            }

            var lastName = $("#ContentPlaceHolderBody_txtLastName");
            if (lastName.val().trim() == "") {
                lastName.css("border-color", "red");
                //  errorMsg += "Please enter last name.<br/>";
                $("#spnLastName").text("<asp:Literal ID="Literal41" runat="server" Text="<%$ Resources:registration59%>" />");
                IsError = true;
            }
            else {
                lastName.css("border-color", "");
                $("#spnLastName").text("");
            }
            var email = $("#ContentPlaceHolderBody_txtEmail");
            if (email.val().trim() == "") {
                email.css("border-color", "red");
                //  errorMsg += "Please enter email.<br/>";
                $("#spnEmail").text("<asp:Literal ID="Literal42" runat="server" Text="<%$ Resources:registration60%>" />");
                IsError = true;
            }
            else if (!IsEmail(email.val())) {
                email.css("border-color", "red");
                // errorMsg += "Please enter valid email.<br/>";
                $("#spnEmail").text("<asp:Literal ID="Literal43" runat="server" Text="<%$ Resources:registration61%>" />");
                IsError = true;
            }
            else {
                email.css("border-color", "");
                $("#spnEmail").text("");
            }

        var countryCode = $("#ContentPlaceHolderBody_txtCountryCode");
        if (countryCode.val().trim() == "") {
            countryCode.css("border-color", "red");
            //  errorMsg += "<asp:Literal ID="Literal44" runat="server" Text="<%$ Resources:registration62%>" /><br/>";
            IsError = true;
        }
        else {
            countryCode.css("border-color", "");
        }

        var phone = $("#ContentPlaceHolderBody_txtPhone");
        if (phone.val().trim() == "") {
            phone.css("border-color", "red");
            //  errorMsg += "Please enter mobile number.<br/>";
            $("#spnPhone").text("<asp:Literal ID="Literal45" runat="server" Text="<%$ Resources:registration63%>" />");
            IsError = true;
        }
        else {
            phone.css("border-color", "");
            $("#spnPhone").text("");
        }

        var cmbday = $("#ContentPlaceHolderBody_cmbday");
        if (cmbday.val().trim() == "dd") {
            cmbday.css("border-color", "red");
            // errorMsg += "<asp:Literal ID="Literal46" runat="server" Text="<%$ Resources:registration64%>" /><br/>";
                IsError = true;
            }
            else {
                cmbday.css("border-color", "");
            }

            var cmbmnth = $("#ContentPlaceHolderBody_cmbmnth");
            if (cmbmnth.val().trim() == "mm") {
                cmbmnth.css("border-color", "red");
                //  errorMsg += "<asp:Literal ID="Literal47" runat="server" Text="<%$ Resources:registration65%>" /><br/>";
                IsError = true;
            }
            else {
                cmbmnth.css("border-color", "");
            }

            var cmbyear = $("#ContentPlaceHolderBody_cmbyear");
            if (cmbyear.val().trim() == "yyyy") {
                cmbyear.css("border-color", "red");
                // errorMsg += "<asp:Literal ID="Literal48" runat="server" Text="<%$ Resources:registration66%>" /><br/>";
                IsError = true;
            }
            else {
                cmbyear.css("border-color", "");
            }

            var cmbgender = $("#ContentPlaceHolderBody_cmbgender");
            if (cmbgender.val().trim() == "") {
                cmbgender.css("border-color", "red");
                //     errorMsg += "<asp:Literal ID="Literal49" runat="server" Text="<%$ Resources:registration67%>" /><br/>";
                IsError = true;
            }
            else {
                cmbgender.css("border-color", "");
            }

            var drplocation = $("#drpOfficeLocation");
            if (drplocation.val().trim() == "Select Location" || drplocation.val().trim() == "") {
                drplocation.css("border-color", "red");
                errorMsg += "<asp:Literal ID="Literal407" runat="server" Text="<%$ Resources:drpLocationErrorMessage%>" /><br/>";
                IsError = true;
            }
            else {
                drplocation.css("border-color", "");
            }

            if ($("#ContentPlaceHolderBody_FileUploadFace").val() == "") {
                $("#lblFace").css("color", "red");
                errorMsg += "<asp:Literal ID="Literal50" runat="server" Text="<%$ Resources:registration68%>" /><br/>";
                IsError = true;
            }
            else {
                $("#lblFace").css("color", "");
            }
            if ($("#ContentPlaceHolderBody_FileUploadRightside").val() == "") {
                $("#lblRightside").css("color", "red");
                errorMsg += "<asp:Literal ID="Literal51" runat="server" Text="<%$ Resources:registration69%>" /><br/>";
                IsError = true;
            }
            else {
                $("#lblRightside").css("border-color", "");
            }
            if ($("#ContentPlaceHolderBody_FileUploadLeftside").val() == "") {
                $("#lblLeftside").css("color", "red");
                errorMsg += "<asp:Literal ID="Literal52" runat="server" Text="<%$ Resources:registration70%>" /><br/>";
                IsError = true;
            }
            else {
                $("#lblLeftside").css("color", "");
            }
            if ($("#ContentPlaceHolderBody_FileUploadFront").val() == "") {
                $("#lblFront").css("color", "red");
                errorMsg += "<asp:Literal ID="Literal53" runat="server" Text="<%$ Resources:registration71%>" /><br/>";
                IsError = true;
            }
            else {
                $("#lblFront").css("color", "");
            }
            if ($("#ContentPlaceHolderBody_FileUploadTopteeth").val() == "") {
                $("#lblTopteeth").css("color", "red");
                errorMsg += "<asp:Literal ID="Literal54" runat="server" Text="<%$ Resources:registration72%>" /><br/>";
                IsError = true;
            }
            else {
                $("#lblTopteeth").css("border-color", "");
            }
            if ($("#ContentPlaceHolderBody_FileUploadLowerteeth").val() == "") {
                $("#lblLowerteeth").css("color", "red");
                errorMsg += "<asp:Literal ID="Literal55" runat="server" Text="<%$ Resources:registration73%>" /><br/>";
                IsError = true;
            }
            else {
                $("#lblLowerteeth").css("color", "");
            }

            if ($("#ContentPlaceHolderBody_FileUploadBigSmile").val() == "") {
                $("#spnBigSmile").text("<asp:Literal ID="Literal56" runat="server" Text="<%$ Resources:registration74%>" />");
                IsError = true;
            }
            else {
                $("#spnBigSmile").text("");
            }

            if ($("#ContentPlaceHolderBody_FileUploadMouthOpen").val() == "") {
                $("#spnMouthOpen").text("<asp:Literal ID="Literal57" runat="server" Text="<%$ Resources:registration75a%>" />");
                IsError = true;
            }
            else {
                $("#spnMouthOpen").text("");
            }

            if ($("#ContentPlaceHolderBody_FileUploadMouthClosed").val() == "") {
                $("#spnMouthClosed").text("<asp:Literal ID="Literal58" runat="server" Text="<%$ Resources:registration76%>" />");
                IsError = true;
            }
            else {
                $("#spnMouthClosed").text("");
            }

            if ($("#ContentPlaceHolderBody_FileUploadProfile").val() == "") {
                $("#spnProfile").text("<asp:Literal ID="Literal59" runat="server" Text="<%$ Resources:registration77%>" />");
                IsError = true;
            }
            else {
                $("#spnProfile").text("");
            }



            if (!IsError) {
                $("#dvFirst").hide();
                $("#dvSecond").show();
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
            else {

                $("#dvError").css("display", "block");
                $("#dvErrorMsg").empty();
                // $("#dvErrorMsg").append("<center><span>" + errorMsg + "</span></center>");
                //  $("#dvErrorMsg").append("<center><span> <asp:Literal ID="Literal74" runat="server" Text="<%$ Resources:registration77%>" />  </span></center>");
                $("html, body").animate({ scrollTop: 0 }, "slow");

            }

            return false;

        }
        function Validate() {
            $("#dvError").css("display", "none");
            var IsError = false;
            var errorMsg = "<br/>";
            var firstName = $("#ContentPlaceHolderBody_txtFirstName");
            if (firstName.val().trim() == "") {
                firstName.css("border-color", "red");
                $("#spnFirstName").text("<asp:Literal ID="Literal60" runat="server" Text="<%$ Resources:registration58%>" />");
                IsError = true;
            }
            else {
                firstName.css("border-color", "");
                $("#spnFirstName").text("");
            }

            var lastName = $("#ContentPlaceHolderBody_txtLastName");
            if (lastName.val().trim() == "") {
                lastName.css("border-color", "red");
                $("#spnLastName").text("<asp:Literal ID="Literal61" runat="server" Text="<%$ Resources:registration59%>" />");
                IsError = true;
            }
            else {
                lastName.css("border-color", "");
                $("#spnLastName").text("");
            }
            var email = $("#ContentPlaceHolderBody_txtEmail");
            if (email.val().trim() == "") {
                email.css("border-color", "red");
                $("#spnEmail").text("<asp:Literal ID="Literal62" runat="server" Text="<%$ Resources:registration60%>" />");
                IsError = true;
            }
            else if (!IsEmail(email.val())) {
                email.css("border-color", "red");
                $("#spnEmail").text("<asp:Literal ID="Literal63" runat="server" Text="<%$ Resources:registration61%>" />");
                IsError = true;
            }
            else {
                email.css("border-color", "");
                $("#spnEmail").text("");
            }

        var countryCode = $("#ContentPlaceHolderBody_txtCountryCode");
        if (countryCode.val().trim() == "") {
            countryCode.css("border-color", "red");
            IsError = true;
        }
        else {
            countryCode.css("border-color", "");
        }

        var phone = $("#ContentPlaceHolderBody_txtPhone");
        if (phone.val().trim() == "") {
            phone.css("border-color", "red");
            $("#spnPhone").text("<asp:Literal ID="Literal64" runat="server" Text="<%$ Resources:registration63%>" />");
            IsError = true;
        }
        else {
            phone.css("border-color", "");
            $("#spnPhone").text("");
        }

        var cmbday = $("#ContentPlaceHolderBody_cmbday");
        if (cmbday.val().trim() == "dd") {
            cmbday.css("border-color", "red");
            IsError = true;
        }
        else {
            cmbday.css("border-color", "");
        }

        var cmbmnth = $("#ContentPlaceHolderBody_cmbmnth");
        if (cmbmnth.val().trim() == "mm") {
            cmbmnth.css("border-color", "red");
            IsError = true;
        }
        else {
            cmbmnth.css("border-color", "");
        }

        var cmbyear = $("#ContentPlaceHolderBody_cmbyear");
        if (cmbyear.val().trim() == "yyyy") {
            cmbyear.css("border-color", "red");
            IsError = true;
        }
        else {
            cmbyear.css("border-color", "");
        }

        var cmbgender = $("#ContentPlaceHolderBody_cmbgender");
        if (cmbgender.val().trim() == "") {
            cmbgender.css("border-color", "red");
            IsError = true;
        }
        else {
            cmbgender.css("border-color", "");
        }

        if ($("#ContentPlaceHolderBody_FileUploadFace").val() == "") {
            $("#lblFace").css("color", "red");
            errorMsg += "<asp:Literal ID="Literal65" runat="server" Text="<%$ Resources:registration68%>" /><br/>";
            IsError = true;
        }
        else {
            $("#lblFace").css("color", "");
        }
        if ($("#ContentPlaceHolderBody_FileUploadRightside").val() == "") {
            $("#lblRightside").css("color", "red");
            errorMsg += "<asp:Literal ID="Literal66" runat="server" Text="<%$ Resources:registration69%>" /><br/>";
                IsError = true;
            }
            else {
                firstName.css("border-color", "");
            }
            if ($("#ContentPlaceHolderBody_FileUploadLeftside").val() == "") {
                $("#lblLeftside").css("color", "red");
                errorMsg += "<asp:Literal ID="Literal67" runat="server" Text="<%$ Resources:registration70%>" /><br/>";
                IsError = true;
            }
            else {
                $("#lblLeftside").css("color", "");
            }
            if ($("#ContentPlaceHolderBody_FileUploadFront").val() == "") {
                $("#lblFront").css("color", "red");
                errorMsg += "<asp:Literal ID="Literal68" runat="server" Text="<%$ Resources:registration71%>" /><br/>";
                IsError = true;
            }
            else {
                $("#lblFront").css("color", "");
            }
            if ($("#ContentPlaceHolderBody_FileUploadTopteeth").val() == "") {
                $("#lblTopteeth").css("color", "red");
                errorMsg += "<asp:Literal ID="Literal69" runat="server" Text="<%$ Resources:registration72%>" /><br/>";
                IsError = true;
            }
            else {
                firstName.css("border-color", "");
            }
            if ($("#ContentPlaceHolderBody_FileUploadLowerteeth").val() == "") {
                $("#lblLowerteeth").css("color", "red");
                errorMsg += "<asp:Literal ID="Literal70" runat="server" Text="<%$ Resources:registration73%>" /><br/>";
                IsError = true;
            }
            else {
                $("#lblLowerteeth").css("color", "");
            }

            <%--if (!$("#chkTermandConditions").is(':checked')) {
                $("#spnTermandConditions").text("<asp:Literal ID="Literal71" runat="server" Text="<%$ Resources:registration74a%>" />");
                IsError = true;
            }
            else {
                $("#spnTermandConditions").text("");
            }--%>


            var captcha = $("#ContentPlaceHolderBody_txtCaptcha");
            if (captcha.val().trim() == "") {
                captcha.css("border-color", "red");
                $("#spnCaptcha").text("<asp:Literal ID="Literal72" runat="server" Text="<%$ Resources:registration75%>" />");
                IsError = true;
            }
            else if (captcha.val().trim() != $("#ContentPlaceHolderBody_hdnCaptchaCode").val()) {
                captcha.css("border-color", "red");
                $("#spnCaptcha").text("<asp:Literal ID="Literal73" runat="server" Text="<%$ Resources:registration76a%>" />");
                IsError = true;
            }
            else {
                captcha.css("border-color", "");
                $("#spnCaptcha").text("");
            }

        if (IsError) {
            $("#dvError").css("display", "block");
            $("#dvErrorMsg").empty();
            // $("#dvErrorMsg").append("<center><span>" + errorMsg + "</span></center>");
            //$("#dvErrorMsg").append("<center><span> <asp:Literal ID="Literal74a" runat="server" Text="<%$ Resources:registration77a%>" />  </span></center>");
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }
        else {
            FetchingData();
        }

        return !IsError;

    }
    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }


    function numbersonly(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }


    function openwindowpassword() {

        alert("<asp:Literal ID="Literal75" runat="server" Text="<%$ Resources:registration78%>" />");
    }

    function BackPreview() {
        $("#dvError").css("display", "none");
        $("#dvFirst").show();
        $("#dvSecond").hide();
        $("html, body").animate({ scrollTop: 0 }, "slow");

    }
    function ValidateFileUpload(e) {
        var MAX_SIZE = 1000; //kb
        //var fuData = document.getElementById('fileChooser');

        var fuData = e;
        var FileUploadPath = fuData.value;
        $("#" + (fuData.id).replace("FileUpload", "spn")).html("");
        $("#" + (fuData.id).replace("FileUpload", "lbl")).css("color", "");

        if (FileUploadPath == '') {
            alert("<asp:Literal ID="Literal76" runat="server" Text="<%$ Resources:registration79%>" />");

        } else {
            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();



            if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                        || Extension == "jpeg" || Extension == "jpg") {


                if (fuData.files && fuData.files[0]) {

                    var size = fuData.files[0].size;

                    if (size / 1024 > MAX_SIZE) {
                        alert("<asp:Literal ID="Literal77" runat="server" Text="<%$ Resources:registration80%>" />");
                            $("#" + (fuData.id).replace("FileUpload", "lbl")).css("color", "red");
                            $("#" + (fuData.id).replace("FileUpload", "spn")).html("<br><asp:Literal ID="Literal78" runat="server" Text="<%$ Resources:registration81%>" />");
                            fuData.value = "";
                            return;
                        } else {
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                $('#blah').attr('src', e.target.result);
                            }

                            reader.readAsDataURL(fuData.files[0]);
                        }
                    }

                }


                else {
                    alert("<asp:Literal ID="Literal79" runat="server" Text="<%$ Resources:registration82%>" />");
                    $("#" + (fuData.id).replace("FileUpload", "lbl")).css("color", "red");
                    $("#" + (fuData.id).replace("FileUpload", "spn")).html("<br><asp:Literal ID="Literal80" runat="server" Text="<%$ Resources:registration83%>" />");
                    fuData.value = "";
                }
            }
        }
    </script>
    <script src="lib/pagejs/camera.js" type="text/javascript"></script>
</asp:Content>
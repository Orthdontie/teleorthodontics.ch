﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Teleorthodontics.Web.DB;
using System.Web.UI.HtmlControls;
using System.Drawing;

namespace Teleorthodontics.Web
{
    public partial class Register : System.Web.UI.Page
    {
        DatabaseHelper databaseHelper = new DatabaseHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["User_Id"] = "17061792";
                if (Session["User_Id"] == null)
                {
                    Response.Redirect("Registration.aspx");
                }
                rptQAnswer.DataSource = GetQuestion();
                rptQAnswer.DataBind();
            }
        }

        private DataTable GetQuestion()
        {

            String query = @"select * from TeleQuestion where active=1";
            DataSet ds = new DataSet();
            ds = databaseHelper.ExecuteDataSet(query, CommandType.Text);
            DataTable dt = ds.Tables[0];
            return dt;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            string strQuery = "";
            foreach (RepeaterItem item in rptQAnswer.Items)
            {

                HiddenField hdnQuestionId = item.FindControl("hdnQuestionId") as HiddenField;
                HiddenField hdnQType = item.FindControl("hdnQType") as HiddenField;

                string str = "";
                int QType = Convert.ToInt32(hdnQType.Value);
                if (QType == 1)
                {
                    RadioButtonList ddlAnswer = item.FindControl("ddlAnswer") as RadioButtonList;
                    if (ddlAnswer.SelectedValue == "True")
                    {
                        str = "Yes";
                    }
                    else if ((ddlAnswer.SelectedValue == "False"))
                    {
                        str = "No";
                    }
                }
                else if (QType == 2)
                {
                    RadioButtonList ddlAnswer = item.FindControl("RadioButtonList1") as RadioButtonList;
                    if (ddlAnswer.SelectedValue != "")
                    {
                        str = ddlAnswer.SelectedItem.Text;
                    }

                }
                else if (QType == 3)
                {
                    TextBox txtMessage = item.FindControl("txtMessage") as TextBox;
                    str = txtMessage.Text;
                }





                if (str != "")
                {
                    if (strQuery == "")
                    {
                        strQuery = " Insert Into TeleQuestionAnswer(User_Id,Q_Id,Answer) values('" + Session["User_Id"] + "'," + hdnQuestionId.Value + ",'" + str + "')";
                    }
                    else
                    {
                        strQuery = strQuery + ",('" + Session["User_Id"] + "'," + hdnQuestionId.Value + ",'" + str + "')";
                    }
                }
            }

            if (strQuery != "")
            {
                databaseHelper.ExecuteNonQuery(strQuery, CommandType.Text);
            }

            dvMsg.Visible = true;
            lblMsg.Text = "Patient registration successfully completed . Please check your email";
            lblMsg.ForeColor = Color.Green;
            btnSubmit.Visible = false;
        }

        protected void rptQAnswer_DataBinding(object sender, EventArgs e)
        {

        }

        protected void rptQAnswer_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //if (e.Item.ItemType == ListItemType.Item)
            //{
            //    // get your radioButtonList
            //    RadioButtonList radioList = (RadioButtonList)e.Item.FindControl("ddlAnswer");
            //    HiddenField hdnQType = (HiddenField)e.Item.FindControl("hdnQType");
            //    int QType = Convert.ToInt32(hdnQType.Value);
            //    if(QType==1)
            //    {
            //        radioList.Items.Add(new ListItem("Yes", "1"));
            //        radioList.Items.Add(new ListItem("No", "0"));
            //    }
            //    else if (QType == 2)
            //    {
            //        radioList.Items.Add(new ListItem("I have 1 or 3 teeth that need to be moved", "1"));
            //        radioList.Items.Add(new ListItem("I just want to improve my style. I do not need perfection", "2"));
            //        radioList.Items.Add(new ListItem("I want the most ideal smile possible", "3"));
            //    }

            //    // loop in options of the RadioButtonList
            //    //foreach (ListItem option in optionsList.Items)
            //    //{
            //    //    // add a custom attribute
            //    //    option.Attributes["data-flag"] = "true";
            //    //}
            //}
        }
    }

}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="Teleorthodontics.Web.Payment" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Teleorthodontics - Straighter teeth from your home comfort</title>

    <!-- Bootstrap Core CSS -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="lib/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="lib/device-mockups/device-mockups.min.css">

    <!-- Theme CSS -->
    <link href="css/main.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        header {
            color: #171010;
        }
    </style>

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <%--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="how-it-works.aspx">How it works</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="pricing.aspx">Pricing</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="contact.aspx">Contact</a>
                    </li>
                </ul>
            </div>--%>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header class="contact">
        <form id="registrationForm" runat="server">
            <br />
            <br />
            <div class="container">
                <div class="row">
                    <br />
                    <br />
                    <h2 class="text-center" id="paymentHeading" runat="server" visible="false">Please make a payment</h2>
              
                    <div runat="server" id="dvMsg" visible="false" class="col-md-6 col-md-offset-3">
                        <div runat="server" id="dvPanel" class="alert alert-danger fade in">
                            <asp:Label ID="lblMsg"  runat="server"></asp:Label>
                            <a href="/" class="pull-right"> Home</a>
                        </div>
                    </div>
                    <div  runat="server" id="dvContent"  class="col-md-6 col-md-offset-3">
                        <div class="">
                            <div class="form-group">
                                <label for="cardExpiry"> <asp:Label ID="lblChoosePlan" runat="server" Text="Choose Plan"></asp:Label> </label>
                                <asp:TextBox ID="txtTotalAmount" runat="server" ReadOnly="true" CssClass="form-control" Visible="false"></asp:TextBox>
                                <asp:DropDownList ID="ddlPlans" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="panel panel-default credit-card-box">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="couponCode">Card Holder Name</label>
                                            <asp:TextBox ID="txtFullName" runat="server" CssClass="form-control" placeholder="Full Name" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-8 col-md-8">
                                        <div class="form-group">
                                            <label for="cardExpiry">Card Number</label>
                                            <div class="input-group">
                                                <asp:TextBox ID="txtCardNumber" runat="server" CssClass="form-control" autocomplete="off" placeholder="Valid Card Number"></asp:TextBox>
                                                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-md-4 pull-right">
                                        <div class="form-group">
                                            <label for="cardCVC">Card Type</label>
                                            <asp:DropDownList ID="ddlCardType" runat="server" CssClass="form-control">
                                                
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-9 col-md-9">
                                        <div class="form-group">
                                            <label for="cardExpiry"><span class="hidden-xs">Expiration</span><span class="visible-xs-inline">Exp</span> Date</label>
                                            <div class="row">
                                                <div class="col-xs-5 col-md-5">
                                                    <asp:DropDownList ID="ddlMonth" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                <div class="col-xs-7 col-md-7 pull-right">
                                                    <asp:DropDownList ID="ddlYear" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 col-md-3 pull-right">
                                        <div class="form-group">
                                            <label for="cardCVC">CV Code</label>
                                            <asp:TextBox ID="txtCVC" runat="server" CssClass="form-control" autocomplete="off" placeholder="CVC"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: center">
                                        <asp:Button ID="btnPayment" class="subscribe btn btn-success btn-lg btn-block"  OnClientClick="return Validate()" runat="server" Text="Payment" OnClick="btnPayment_Click" />
                                    </div>
                                </div>
                                <div class="row" style="display: none;">
                                    <div class="col-xs-12">
                                        <p class="payment-errors"></p>
                                    </div>
                                </div>

                            </div>
                            <div class="panel-footer">
                                <div class="row display-tr">
                                    <div class="col-md-6">
                                        <h3 class="panel-title display-td"></h3>
                                    </div>
                                    <div class="col-md-6 pull-right">
                                        <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </header>


    <footer>
        <div class="container">
            <p>&copy; 2017 Teleorthodontics.ch. All Rights Reserved.</p>
            <ul class="list-inline">
                <li>
                    <a href="/privacy.aspx">Privacy</a>
                </li>
                <li>
                    <a href="/terms.aspx">Terms</a>
                </li>
                <li>
                    <a href="/faq.aspx">FAQ</a>
                </li>
            </ul>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="lib/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/new-age.min.js"></script>

</body>

</html>

<%--<link href="lib/loading/HoldOn.min.css" rel="stylesheet" />
<script src="lib/loading/HoldOn.min.js"></script>--%>
<script src="js/creditCard.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("body").on("contextmenu", function (e) {
            return false;
        });
        $('body').bind('cut copy paste', function (e) {
            e.preventDefault();
        });
        $(document).keydown(function (event) {
            if (event.keyCode == 123) {
                return false;
            }
            else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) {
                return false;
            }
        });

        $(document).on("contextmenu", function (e) {
            e.preventDefault();
        });
    });
    function Validate() {
        var IsError = false;

        var ddlPlans = $("#ddlPlans");
        if (ddlPlans.val() == "0") {
            ddlPlans.css("border-color", "red");
            IsError = true;
        }
        else {
            ddlPlans.css("border-color", "");
        }
        var CardType = $("#ddlCardType");
        if (CardType.val() == "0") {
            CardType.css("border-color", "red");
            IsError = true;
        }
        else {
            CardType.css("border-color", "");
        }
        var FullName = $("#txtFullName");
        if (FullName.val() == "") {
            FullName.css("border-color", "red");
            IsError = true;
        }
        else {
            FullName.css("border-color", "");
        }
        var CardNumber = $("#txtCardNumber");
        if (CardNumber.val() == "") {
            CardNumber.css("border-color", "red");
            IsError = true;
        }
        else if (!testCreditCard(CardNumber.val(), CardType.val())) {
            CardNumber.css("border-color", "red");
            IsError = true;
        }
        else {
            CardNumber.css("border-color", "");
        }
        var ddlMonth = $("#ddlMonth");
        if (ddlMonth.val() == "Month") {
            ddlMonth.css("border-color", "red");
            IsError = true;
        }
        else {
            ddlMonth.css("border-color", "");
        }
        var ddlYear = $("#ddlYear");
        if (ddlYear.val() == "Year") {
            ddlYear.css("border-color", "red");
            IsError = true;
        }
        else {
            ddlYear.css("border-color", "");
        }
        var txtCVC = $("#txtCVC");
        if (txtCVC.val() == "") {
            txtCVC.css("border-color", "red");
            IsError = true;
        }
        else {
            txtCVC.css("border-color", "");
        }

        return !IsError;
    }


    
    function testCreditCard(CardNumber, CardType) {
        if (!checkCreditCard(CardNumber, CardType)) {
            //ShowerrorMsg(ccErrors[ccErrorNo]);
            alert(ccErrors[ccErrorNo]);
            return false;
        }
        return true;
    }

    //function ShowerrorMsg(Msg) {
    //    if (Msg != "") {
    //        $("#MsgContainer").show();
    //        $("#MsgContainer").addClass("alert alert-warning fade in alert-dismissable text-center");
    //        $("#spnMsg").text(Msg);
    //        checkMsg();
    //    }
    //}
    //function checkMsg() {
    //    if ($("#spnMsg").text() != null && $("#spnMsg").text() != "") {
    //        $("#MsgContainer").show();
    //        setTimeout(function () {
    //            $("#MsgContainer").hide();
    //        }, 20000);
    //    }
    //}

    $(function () {
        $.post("Api.asmx/SubscriptionNotification", function (data) { alert(data) });
    });

</script>

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Teleorthodontics.Web.Helper
{
    public class CreditCardAttribute : ValidationAttribute
    {

        public static bool IsValid(object value, CardType _cardTypes)
        {
            var number = Convert.ToString(value);
            if (String.IsNullOrEmpty(number))
                return false;

            return IsValidType(number, _cardTypes) && IsValidNumber(number);
        }
        private static bool IsValidType(string cardNumber, CardType cardType)
        {
            // Visa
            if (Regex.IsMatch(cardNumber, "^(4)")
                && ((cardType & CardType.Visa) != 0))
                return cardNumber.Length == 13 || cardNumber.Length == 16;

            // MasterCard
            if (Regex.IsMatch(cardNumber, "^(51|52|53|54|55)")
                && ((cardType & CardType.Master) != 0))
                return cardNumber.Length == 16;

            // Amex
            if (Regex.IsMatch(cardNumber, "^(34|37)")
                && ((cardType & CardType.AmericanExpress) != 0))
                return cardNumber.Length == 15;

            // Diners
            if (Regex.IsMatch(cardNumber, "^(300|301|302|303|304|305|36|38)")
                && ((cardType & CardType.Diners) != 0))
                return cardNumber.Length == 14;

            // JCB -- 3 -- 16 length
            if (Regex.IsMatch(cardNumber, "^(3)") && (cardType & CardType.JCB) != 0)
                return cardNumber.Length == 16;

            // JCB-- 2131, 1800-- 15 length
            if (Regex.IsMatch(cardNumber, "^(2131|1800)") && (cardType & CardType.JCB) != 0)
                return cardNumber.Length == 15;
            // Discover

            if (Regex.IsMatch(cardNumber, "^(6011)") && (cardType & CardType.Discover) != 0)
                return cardNumber.Length == 16;

            return false;
        }

        private static bool IsValidNumber(string number)
        {
            int[] DELTAS = new int[] { 0, 1, 2, 3, 4, -4, -3, -2, -1, 0 };
            int checksum = 0;
            char[] chars = number.ToCharArray();
            for (int i = chars.Length - 1; i > -1; i--)
            {
                int j = ((int)chars[i]) - 48;
                checksum += j;
                if (((i - chars.Length) % 2) == 0)
                    checksum += DELTAS[j];
            }

            return ((checksum % 10) == 0);
        }

    }
}
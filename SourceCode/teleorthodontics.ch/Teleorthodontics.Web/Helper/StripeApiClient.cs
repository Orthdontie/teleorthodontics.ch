﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Teleorthodontics.Web.Helper
{
    public class StripeApiClient
    {

        public static SingleModelResponse<StripeSubscriptionResponse> CreateCustomerAndPaymentOneTime(PlanViewModel customerinfo)
        {
            var response = new SingleModelResponse<StripeSubscriptionResponse>();
            try
            {
                StripeSubscriptionResponse stripeSubscriptionResponse = new StripeSubscriptionResponse();
                var myCustomer = new StripeCustomerCreateOptions();
                myCustomer.Email = customerinfo.Email;
                myCustomer.Description = customerinfo.Name + "(" + customerinfo.Email + " - " + customerinfo.UserId + ")";
                var patientData = new Dictionary<string, string>();
                patientData.Add("Patient UserId", customerinfo.UserId);
                patientData.Add("Patient First Name", customerinfo.FirstName);
                patientData.Add("Patient Last Name", customerinfo.LastName);
                patientData.Add("Patient Email", customerinfo.Email);
                patientData.Add("Patient AccountId", (string.IsNullOrEmpty(customerinfo.AccountId)?"NA":customerinfo.AccountId));
                myCustomer.Metadata = patientData;
                var charges = new StripeChargeService();
                myCustomer.SourceCard = new SourceCard()
                {
                    Number = customerinfo.CardNumber,
                    ExpirationYear = customerinfo.ExpiryYear,
                    ExpirationMonth = customerinfo.ExpiryMonth,
                    Name = customerinfo.Name,
                    Cvc = customerinfo.Cvc
                };
                var customerService = new StripeCustomerService();
                StripeCustomer stripeCustomer = customerService.Create(myCustomer);
                stripeSubscriptionResponse.StripeCustomerID = stripeCustomer.Id;
                var charge = charges.Create(new StripeChargeCreateOptions
                {
                    Amount = Convert.ToInt32(customerinfo.TotalCost * 100),
                    Description = "Tele Orthodontie online consultation  charge - " + customerinfo.UserId,
                    Currency = "chf",
                    CustomerId = stripeCustomer.Id
                });


                stripeSubscriptionResponse.StripePlanID = customerinfo.StripePlanId;
                stripeSubscriptionResponse.StripeCardId = stripeCustomer.DefaultSourceId;
                stripeSubscriptionResponse.TransactionId = charge.BalanceTransactionId;
                response.IsSuccess = charge.Status == "succeeded" ? true : false;

                response.Model = stripeSubscriptionResponse;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.IsSuccess = false;
                response.Details = ex.StackTrace;
            }

            return response;
        }

        public static SingleModelResponse<StripeSubscriptionResponse> CreateCustomerAndSubscription(PlanViewModel customerinfo)
        {
            var response = new SingleModelResponse<StripeSubscriptionResponse>();
            try
            {
                StripeSubscriptionResponse stripeSubscriptionResponse = new StripeSubscriptionResponse();
                var myCustomer = new StripeCustomerCreateOptions();

                myCustomer.Email = customerinfo.Email;
                myCustomer.Description = customerinfo.Name + "(" + customerinfo.Email + " - " + customerinfo.UserId + ")";

                var patientData = new Dictionary<string, string>();
                patientData.Add("Patient UserId", customerinfo.UserId);
                patientData.Add("Patient First Name", customerinfo.FirstName);
                patientData.Add("Patient Last Name", customerinfo.LastName);
                patientData.Add("Patient Email", customerinfo.Email);
                patientData.Add("Patient AccountId", (string.IsNullOrEmpty(customerinfo.AccountId)?"NA":customerinfo.AccountId));
                myCustomer.Metadata = patientData;
                myCustomer.SourceCard = new SourceCard()
                {
                    Number = customerinfo.CardNumber,
                    ExpirationYear = customerinfo.ExpiryYear,
                    ExpirationMonth = customerinfo.ExpiryMonth,
                    Name = customerinfo.Name,
                    Cvc = customerinfo.Cvc
                };

                var customerService = new StripeCustomerService();
                StripeCustomer stripeCustomer = customerService.Create(myCustomer);
                stripeSubscriptionResponse.StripeCustomerID = stripeCustomer.Id;

                DateTime? trialEndDate = null;

                if (customerinfo.TrialEnd.HasValue && customerinfo.TrialEnd.Value > 0)
                    trialEndDate = DateTime.Now.AddDays(customerinfo.TrialEnd.Value);

                var subscriptionService = new StripeSubscriptionService();
                var subscriptionCreateOption = new StripeSubscriptionCreateOptions()
                {
                    PlanId = customerinfo.StripePlanId,
                    CustomerId = stripeCustomer.Id,
                    Quantity = 1,
                    TrialEnd = trialEndDate,
                    EndTrialNow = !trialEndDate.HasValue
                };

                StripeSubscription stripeSubscription = subscriptionService.Create(stripeCustomer.Id, subscriptionCreateOption);

                if (stripeSubscription != null)
                    stripeSubscriptionResponse.StripeSubscriptionID = stripeSubscription.Id;

                stripeSubscriptionResponse.StripePlanID = customerinfo.StripePlanId;
                stripeSubscriptionResponse.StripeCardId = stripeCustomer.DefaultSourceId;
                response.IsSuccess = true;
                response.Model = stripeSubscriptionResponse;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.IsSuccess = false;
                response.Details = ex.StackTrace;
            }

            return response;
        }

        public static SingleModelResponse<StripeSubscriptionResponse> UpdateSubscription(UpgradePlanViewModel planModel)
        {
            var response = new SingleModelResponse<StripeSubscriptionResponse>();
            try
            {
                StripeSubscriptionResponse stripeSubscriptionResponse = new StripeSubscriptionResponse();
                DateTime? trialEndDate = null;

                if ((planModel.TrialEnd.HasValue && planModel.TrialEnd.Value > 0))
                    trialEndDate = DateTime.Now.AddDays(planModel.TrialEnd.Value);


                var subscriptionService = new StripeSubscriptionService();
                var subscriptionCreateOption = new StripeSubscriptionUpdateOptions()
                {
                    PlanId = planModel.StripePlanId,
                    Quantity = 1,
                    TrialEnd = trialEndDate,
                    EndTrialNow = !trialEndDate.HasValue
                };

                StripeSubscription stripeSubscription = subscriptionService.Update(planModel.StripeSubscriptionID, subscriptionCreateOption);

                if (stripeSubscription != null)
                    stripeSubscriptionResponse.StripeSubscriptionID = stripeSubscription.Id;

                stripeSubscriptionResponse.StripePlanID = planModel.StripePlanId;
                //stripeSubscriptionResponse.StripeCardId = stripeCustomer.DefaultSourceId;

                response.IsSuccess = true;
                response.Model = stripeSubscriptionResponse;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + Environment.NewLine + (ex.InnerException != null ? ex.InnerException.Message : "");
                response.IsSuccess = false;
                response.Details = ex.StackTrace;
            }

            return response;
        }

        public static SingleModelResponse<StripeSubscriptionResponse> AddNewCardInStripe(CardViewModel cardViewModel, string stripeCustomerID)
        {
            var response = new SingleModelResponse<StripeSubscriptionResponse>();
            try
            {
                StripeSubscriptionResponse stripeSubscriptionResponse = new StripeSubscriptionResponse();

                var updatecustomer = new StripeCustomerUpdateOptions();

                updatecustomer.SourceCard = new SourceCard()
                {
                    Number = cardViewModel.CardNumber,
                    ExpirationYear = cardViewModel.ExpiryYear,
                    ExpirationMonth = cardViewModel.ExpiryMonth,
                    Name = cardViewModel.FirstName + " " + cardViewModel.Surname,
                    Cvc = cardViewModel.Cvc
                };

                var customerService = new StripeCustomerService();

                StripeCustomer ExiststripeCustomer = customerService.Update(stripeCustomerID, updatecustomer);

                stripeSubscriptionResponse.StripeCardId = ExiststripeCustomer.DefaultSourceId;

                response.IsSuccess = true;
                response.Model = stripeSubscriptionResponse;
            }
            catch (Exception ex)
            {
                response.Message = ex.Message + Environment.NewLine + (ex.InnerException != null ? ex.InnerException.Message : "");
                response.IsSuccess = false;
                response.Details = ex.StackTrace;
            }

            return response;
        }

        /*
        public void UpdateCardInfoInStripe(UserBillingInfoModel paymentModel, string stripeCustomerID, string stripeCardID)
        {
            var cardOptions = new StripeCardUpdateOptions()
            {
                ExpirationYear = paymentModel.ExpYear.ToString(),
                ExpirationMonth = paymentModel.ExpMonth.ToString(),
                AddressLine1 = paymentModel.Address1,
                AddressLine2 = paymentModel.Address2,
                AddressCity = paymentModel.City,
                AddressState = paymentModel.StateId.ToString()
            };

            var cardService = new StripeCardService(StripeApiKey);

            cardService.Update(stripeCustomerID, stripeCardID, cardOptions);
        }
          */
        public static IEnumerable<StripePlan> ListAllPlans()
        {
            var planService = new StripePlanService();
            IEnumerable<StripePlan> response = planService.List();
            return response;
        }


    }

    public class StripeSubscriptionResponse
    {
        public string StripeCustomerID { get; set; }
        public string StripeSubscriptionID { get; set; }
        public string StripePlanID { get; set; }
        public long UserID { get; set; }
        public string StripeCardId { get; set; }
        public string TransactionId { get; set; }
    }
    public class SingleModelResponse<TModel> : ISingleModelResponse<TModel>
    {
        public String Message { get; set; }
        public Boolean IsSuccess { get; set; }
        public String Details { get; set; }
        public TModel Model { get; set; }
    }
    public interface ISingleModelResponse<TModel> : IResponse
    {
        TModel Model { get; set; }
    }
    public interface IResponse
    {
        String Message { get; set; }
        Boolean IsSuccess { get; set; }
        String Details { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Teleorthodontics.Web.Helper
{
    public class SessionManager
    {
        public static string GetLanguage(string languageFromQueryString)
        {
            if (Common.CheckLanguage(languageFromQueryString))
            {
                if (!string.IsNullOrEmpty(languageFromQueryString))
                {
                    HttpContext.Current.Session["languagechng"] = languageFromQueryString;
                }
                else if (HttpContext.Current.Session["languagechng"] != null)
                {
                    languageFromQueryString = (string)HttpContext.Current.Session["languagechng"];
                }

                if (languageFromQueryString == null)
                {
                    languageFromQueryString = ConfigurationManager.AppSettings["DefaultLanguage"].ToString();
                    HttpContext.Current.Session["languagechng"] = languageFromQueryString;
                }
            }
            else
            {
                languageFromQueryString = ConfigurationManager.AppSettings["DefaultLanguage"].ToString();
                HttpContext.Current.Session["languagechng"] = languageFromQueryString;
            }

            return (string)HttpContext.Current.Session["languagechng"];
        }
    }
}
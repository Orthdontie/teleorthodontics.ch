﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using Teleorthodontics.Web.DB;

namespace Teleorthodontics.Web.Helper
{
    public class Sendmail
    {
        private static string loginEmail = "Votre Email login : ";
        private static string loginPass = "Votre identifiant Mot de passe : ";
        public static void SendContactUsMail(string User_Id, string name, string email, string phoneNo, string IPAddress, string Comments, string subject)
        {

            string emailsendFrom = string.Empty;
            string emailidofbcc = string.Empty;
            string _subject = string.Empty;
            string mailbody = loginEmail + email + " ";
            string prefLang = "English";
            string query = string.Empty;
            query = "select e.Body,et.emailsentfrom,et.emailidofbcc,e.subject from EmailTemplateType et ";
            query += "inner join EmailTemplates e on e.TypeId=et.ID ";
            query += "where e.Type='TeleContactUs'";

            DatabaseHelper databaseHelper = new DatabaseHelper();
            DataSet ds = new DataSet();
            ds = databaseHelper.ExecuteDataSet(query, CommandType.Text);
            DataTable dtblLetter = ds.Tables[0];
            string emailBody = string.Empty;
            if (prefLang == "English")
            {
                if (!Convert.IsDBNull(dtblLetter.Rows[0][0]))
                    emailBody = (String)dtblLetter.Rows[0][0];
                if (!Convert.IsDBNull(dtblLetter.Rows[0][1]))
                    emailsendFrom = (String)dtblLetter.Rows[0][1];
                if (!Convert.IsDBNull(dtblLetter.Rows[0][2]))
                    emailidofbcc = (String)dtblLetter.Rows[0][2];
                _subject = (String)dtblLetter.Rows[0][3];
            }

            emailBody = emailBody.Replace("@@Name", name);
            emailBody = emailBody.Replace("@@Email", email);

            emailBody = emailBody.Replace("@@PhoneNo", phoneNo);
            emailBody = emailBody.Replace("@@IPAddress", IPAddress);
            emailBody = emailBody.Replace("@@Comments", Comments);

            try
            {
                string strFrom = WebConfigurationManager.AppSettings["Defaultfromemailid"];
                string toEmail = WebConfigurationManager.AppSettings["Defaulttoemailid"];
                Sendmail.SendEmail(User_Id, toEmail, subject, emailBody, strFrom, email);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static void SendmailNewUser(string User_Id, string firstname, string lastname, string email, string password, string comments, string ip, Dictionary<string, string> imageList, Dictionary<string, string> questionList)
        {

            string emailsendFrom = string.Empty;
            string emailidofbcc = string.Empty;
            string _subject = string.Empty;
            string mailbody = loginEmail + email + " ";
            mailbody += "<br/>" + loginPass + password + " ";
            string prefLang = "English";
            string query = string.Empty;
            query = "select e.Body,et.emailsentfrom,et.emailidofbcc,e.subject from EmailTemplateType et ";
            query += "inner join EmailTemplates e on e.TypeId=et.ID ";
            query += "where e.Type='TeleWelcomeLetter'";

            DatabaseHelper databaseHelper = new DatabaseHelper();
            DataSet ds = new DataSet();
            ds = databaseHelper.ExecuteDataSet(query, CommandType.Text);
            DataTable dtblLetter = ds.Tables[0];
            string emailBody = string.Empty;
            if (prefLang == "English")
            {
                if (!Convert.IsDBNull(dtblLetter.Rows[0][0]))
                    emailBody = (String)dtblLetter.Rows[0][0];
                if (!Convert.IsDBNull(dtblLetter.Rows[0][1]))
                    emailsendFrom = (String)dtblLetter.Rows[0][1];
                if (!Convert.IsDBNull(dtblLetter.Rows[0][2]))
                    emailidofbcc = (String)dtblLetter.Rows[0][2];
                _subject = (String)dtblLetter.Rows[0][3];
            }

            emailBody = emailBody.Replace("@@FirstName", firstname);


            var tableData = "";
            if (imageList.Count > 0)
            {
                tableData = "<table><tr><td colspan='2'><h2>Attached Patient Images</h2></td></tr>";
                foreach (var item in imageList)
                {
                    if (!string.IsNullOrEmpty(item.Key))
                    {
                        tableData = tableData + "<tr><td>" + item.Key + " </td><td><img style='height:142px;width:250px'  src='@@PatientUrl" + item.Value + "' alt='" + item.Key + "' /></td></tr>";
                    }
                }
                tableData = tableData + "</table>";
            }

            if (questionList.Count > 0)
            {
                tableData = tableData + "<table><tr><td colspan='2'><h2>Attached Patient Question Answer</h2></td></tr>";
                foreach (var item in questionList)
                {
                    if (!string.IsNullOrEmpty(item.Key))
                    {
                        tableData = tableData + "<tr><td><b>Question:</b> </td><td>" + item.Key + "</td></tr>";
                        tableData = tableData + "<tr><td><b>Answer:</b> </td><td>" + item.Value + "</td></tr>";
                    }
                }
                tableData = tableData + "</table>";
            }

            // Patient Details

            tableData = tableData + "<table><tr><td colspan='2'><h3>Patient Details</h3></td></tr>";
            tableData = tableData + "<tr><td><b>Name:</b> </td><td>" + firstname + " " + lastname + "</td></tr>";
            tableData = tableData + "<tr><td><b>Email:</b> </td><td>" + email + "</td></tr>";
            tableData = tableData + "<tr><td><b>IP Address:</b> </td><td>" + ip + "</td></tr>";
            tableData = tableData + "<tr><td><b>Comments:</b> </td><td>" + comments + "</td></tr>";
            //tableData = tableData + "<tr><td><b>Your Login Id:</b> </td><td>" + email + "</td></tr>";
            //tableData = tableData + "<tr><td><b>Your Password:</b> </td><td>" + password + "</td></tr>";
            tableData = tableData + "</table>";




            string imagePath = WebConfigurationManager.AppSettings["ImageWebsiteName"] + "/OnlineConsultaionImages/UserImages/";


            emailBody = emailBody.Replace("@@userinfo", mailbody);
            emailBody = emailBody.Replace("@@ImageUrl", "http://www.orthodontie1.ch/Admin/letterImage/of1.png");
            emailBody = emailBody.Replace("@@ImageList", tableData);
            emailBody = emailBody.Replace("@@PatientUrl", imagePath);

            try
            {
                //ManageEmail.MailSingleUser(firstname, uEmail, emailBody, _subject,emailsendFrom, null,emailidofbcc);
                Sendmail.SendEmail(User_Id, email, _subject, emailBody, emailsendFrom, emailidofbcc);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static void SendEmail(string User_Id, string _toMail, string _subject, string _sbMailDetails, string emailFrom, string emailidofbcc = null)
        {
            string strFrom = WebConfigurationManager.AppSettings["Defaultfromemailid"];
            string emailbcc = WebConfigurationManager.AppSettings["Defaultbccemailid"];
            if (!string.IsNullOrEmpty(emailidofbcc))
            {
                emailbcc = emailidofbcc;
            }

            if (!string.IsNullOrEmpty(emailidofbcc))
                emailbcc += "," + emailidofbcc;

            _sbMailDetails = Sendmail.AutoLoginFormat(_sbMailDetails, _toMail);

            if (!string.IsNullOrEmpty(emailFrom))
            {
                MailMessage message = new MailMessage() { From = new MailAddress(emailFrom) };
                //message.Headers.Add(emailFrom, emailFrom);
                message.To.Add(new MailAddress(_toMail));

                string emailBCC = string.Empty;
                if (!string.IsNullOrEmpty(emailbcc))
                {
                    emailBCC = emailbcc;
                }
                // If you want to send a copy to your email //
                if (!string.IsNullOrEmpty(emailBCC))
                {
                    string[] bccList = emailBCC.Split(new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string bcc in bccList)
                    {
                        message.Bcc.Add(new MailAddress(bcc));
                    }
                }
                message.Subject = _subject;
                // message.Body = _sbMailDetails.Replace(".orthodontie1.ch", "." + System.Configuration.ConfigurationManager.AppSettings["WebsiteName"].ToString()).Replace("https", System.Configuration.ConfigurationManager.AppSettings["HTTP"].ToString());
                message.Body = _sbMailDetails;
                message.Body = ManageTags(message.Body);
                message.IsBodyHtml = true;
                message.Priority = System.Net.Mail.MailPriority.High;
                SmtpClient client = new SmtpClient();
                try
                {
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                AddEmailHistory(User_Id, _toMail, _subject, message.Body, message.Attachments.Count > 0 ? true : false);
            }
        }
        public static void AddEmailHistory(string User_Id, string patientEmail, string subject, string body, bool isAttachment)
        {
            StreamWriter WriteFile;


            try
            {
                if (!string.IsNullOrEmpty(User_Id))
                {
                    long Id = DateTime.Now.Ticks;

                    //   string PatientData = System.Configuration.ConfigurationManager.AppSettings["PatientData"].ToString();
                    string filename = AppDomain.CurrentDomain.BaseDirectory + "Emailhistory/";
                    if (!Directory.Exists(filename))
                    {
                        Directory.CreateDirectory(filename);
                    }

                    string strpath = Path.GetFullPath(filename);
                    string Newfilename = Id + "_" + User_Id + ".html";
                    string strFile = (strpath + Newfilename);
                    if (!File.Exists(strFile))
                    {
                        WriteFile = new StreamWriter(strFile, true, System.Text.Encoding.UTF8);
                        WriteFile.WriteLine("<html><body>" + body + "</body></html>");
                        //mycmd.Parameters.Add("@Body", SqlDbType.VarChar).Value = Newfilename;
                        WriteFile.Flush();
                        WriteFile.Close();

                        int IsAttachmentDocument = isAttachment ? 1 : 0;

                        DatabaseHelper databaseHelper = new DatabaseHelper();
                        databaseHelper.ExecuteNonQuery("INSERT INTO EmailHistory(PatientId,Subject,Body,IsAttachment) Values ('" + User_Id + "','" + subject + "','" + Newfilename + "'," + IsAttachmentDocument + ")", CommandType.Text);
                        //mycon.Open();
                        //mycmd.ExecuteNonQuery();
                        //mycon.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DateTime ConvertServerTimeToLocalTime(DateTime dateTime)
        {
            string server = ConfigurationManager.AppSettings["ServerTimeZone"].ToString();
            string client = ConfigurationManager.AppSettings["ClientTimeZone"].ToString();
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.SpecifyKind(dateTime, DateTimeKind.Unspecified), server, client);

        }
        public static string AutoLoginFormat(string mailBody, string address)
        {
            #region Autologin
            string Password = "";
            try
            {
                //userID = Infrastructure.GetUserId(address);
                //Password = Infrastructure.GetPassword(address);
            }
            catch { }

            mailBody = mailBody.Replace("@@App", "").Replace("@@Ledger", "").Replace("@@Home", "").Replace("@@email", address).Replace("@@auto", Password);




            #endregion
            // return mailBody.Replace(".orthodontie1.ch", "." + System.Configuration.ConfigurationManager.AppSettings["WebsiteName"].ToString()).Replace("https", System.Configuration.ConfigurationManager.AppSettings["HTTP"].ToString());
            return mailBody;
        }
        public static string ManageTags(string body)
        {
            string ContactNo = System.Configuration.ConfigurationManager.AppSettings["ContactNo"];
            body = body.Replace("@@ContactNo", ContactNo);

            string ContactEmail = System.Configuration.ConfigurationManager.AppSettings["ContactEmail"];
            body = body.Replace("@@ContactEmail", ContactNo);

            string fb = System.Configuration.ConfigurationManager.AppSettings["fb"];
            body = body.Replace("@@fb", ContactNo);

            string tweet = System.Configuration.ConfigurationManager.AppSettings["tweet"];
            body = body.Replace("@@ContactNo", tweet);

            string youtube = System.Configuration.ConfigurationManager.AppSettings["youtube"];
            body = body.Replace("@@youtube", ContactNo);

            string gplus = System.Configuration.ConfigurationManager.AppSettings["gplus"];
            body = body.Replace("@@gplus", ContactNo);

            string WebsiteName = System.Configuration.ConfigurationManager.AppSettings["WebsiteName"];
            body = body.Replace("@@websiteName", WebsiteName);

            return body;

        }


    }
}
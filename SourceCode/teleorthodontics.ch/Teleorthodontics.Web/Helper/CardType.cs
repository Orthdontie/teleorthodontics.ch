﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Teleorthodontics.Web.Helper
{
    public enum CardType
    {
        [Display(Name = "Visa")]
        Visa = 1,
        [Display(Name = "Master Card")]
        Master = 2,
        [Display(Name = "American Express")]
        AmericanExpress = 3,
        [Display(Name = "JCB (US Only)")]
        JCB = 4,
        [Display(Name = "Discover (US only)")]
        Discover = 5,
        [Display(Name = "Diners (US Only)")]
        Diners = 6
    }
    public class SelectListItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Teleorthodontics.Web.Helper
{
    public class Plans
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string StripePlanId { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public decimal Discount { get; set; }
        [Required]
        public string Currency { get; set; }
        public int? TrialPeriodDays { get; set; }
        [Required]
        public string Interval { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
    }

}
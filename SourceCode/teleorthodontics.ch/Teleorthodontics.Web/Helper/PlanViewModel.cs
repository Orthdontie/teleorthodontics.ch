﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;


namespace Teleorthodontics.Web.Helper
{
    public class PlanViewModel : StripeResponse
    {
        public Int64 Id { get; set; }
        public string UserId { get; set; }
        public string StripePlanId { get; set; }
        public string BillingFrequency { get; set; }
        public string Title { get; set; }
        [Required(ErrorMessage = "*")]
        public string Name { get; set; }
        public string Email { get; set; }
        public string CardNumber { get; set; }
        public int CardType { get; set; }
        public string Cvc { get; set; }
        public int ExpiryMonth { get; set; }
        public int ExpiryYear { get; set; }
        public decimal TotalCost { get; set; }
        public bool IsShowSubscribeNowButton { get; set; }
        public decimal Discount { get; set; }
        public decimal TotalRecuringCost { get; set; }
        public string EPM { get; set; }
        public int? TrialEnd { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateCreated { get; set; }
        public List<Plans> StripePlans { get; set; }
        public IEnumerable<SelectListItem> Years { get; set; }
        public IEnumerable<SelectListItem> Months { get; set; }
        public IEnumerable<SelectListItem> BillingCycle { get; set; }
        public IEnumerable<SelectListItem> CardTypes { get; set; }
        public long PlanId { get; set; }
        public CreditCardInfo Subscriptioninfo { get; set; }
        public Plans Plan { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AccountId { get; set; }
        public static IEnumerable<SelectListItem> GetYears()
        {
            var years = Enumerable
          .Range(DateTime.Now.Year, 15)
             .Select(year => new SelectListItem
             {
                 Value = year.ToString(CultureInfo.InvariantCulture),
                 Text = year.ToString(CultureInfo.InvariantCulture)
             });
            return years;
        }
        public static IEnumerable<SelectListItem> GetMonths()
        {
            var months = DateTimeFormatInfo
             .InvariantInfo
             .MonthNames
             .TakeWhile(monthName => monthName != String.Empty)
             .Select((monthName, index) => new SelectListItem
             {
                 Value = (index + 1).ToString(CultureInfo.InvariantCulture),
                 Text = monthName.Substring(0, 3)
             });
            return months;
        }
        public static IEnumerable<SelectListItem> GetCardTypes()
        {
            var selectList = new List<SelectListItem>();

            var enumValues = Enum.GetValues(typeof(CardType)) as CardType[];
            if (enumValues == null)
                return null;
            int index = 0;
            foreach (var enumValue in enumValues)
            {
                index = index + 1;
                selectList.Add(new SelectListItem
                {
                    Value = index.ToString(),
                    Text = GetName(enumValue)
                });
            }

            return selectList;
        }
        private static string GetName(CardType value)
        {

            var memberInfo = value.GetType().GetMember(value.ToString());
            if (memberInfo.Length != 1)
                return null;

            var displayAttribute = memberInfo[0].GetCustomAttributes(typeof(DisplayAttribute), false)
                                   as DisplayAttribute[];
            if (displayAttribute == null || displayAttribute.Length != 1)
                return null;

            return displayAttribute[0].Name;
        }
    }

    public class CardViewModel
    {
        public Int64 UserId { get; set; }
        public string Title { get; set; }
        [Required(ErrorMessage = "*")]
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string CardNumber { get; set; }
        public int CardType { get; set; }
        public string Cvc { get; set; }
        public int ExpiryMonth { get; set; }
        public int ExpiryYear { get; set; }
        public IEnumerable<SelectListItem> Years { get; set; }
        public IEnumerable<SelectListItem> Months { get; set; }
        public IEnumerable<SelectListItem> BillingCycle { get; set; }
        public IEnumerable<SelectListItem> CardTypes { get; set; }
    }

    public class UpgradePlanViewModel : StripeResponse
    {
        public Int64 UserId { get; set; }
        public string StripePlanId { get; set; }
        public string BillingFrequency { get; set; }
        public decimal TotalCost { get; set; }
        public decimal Discount { get; set; }
        public string EPM { get; set; }
        public int? TrialEnd { get; set; }
        public List<Plans> StripePlans { get; set; }
        public IEnumerable<SelectListItem> BillingCycle { get; set; }
        public long PlanId { get; set; }
    }

    public class StripeResponse
    {
        public string StripeCustomerID { get; set; }
        public string StripeSubscriptionID { get; set; }
        public string StripeCardId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Teleorthodontics.Web.Helper
{
    public class CreditCardInfo
    {
        [Required]
        public Int64 UserId { get; set; }
        [Required]
        public string StripeCustomerID { get; set; }
        [Required]
        public string StripeSubscriptionID { get; set; }
        [Required]
        public string StripePlanId { get; set; }
        [Required]
        public string StripeCardId { get; set; }
        [Required]
        public Int64 PlanId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string CardNumber { get; set; }
        [Required]
        public int CardType { get; set; }
        [Required]
        public int ExpiryMonth { get; set; }
        [Required]
        public int ExpiryYear { get; set; }
        [Required]
        public decimal TotalCost { get; set; }
        [Required]
        public decimal Discount { get; set; }
        [Required]
        public string EPM { get; set; }
        [Required]
        public bool IsActive { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
    }
}
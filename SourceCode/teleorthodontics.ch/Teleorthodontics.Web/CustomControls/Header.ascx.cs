using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Threading;
using Teleorthodontics.Web.Helper;
using System.Configuration;

public partial class UserControl_Header : System.Web.UI.UserControl
{
    public string url1;

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public string language;
    public string cMyValue;
    string selectedLanguage;

    protected override void FrameworkInitialize()
    {
        language = Request.QueryString["lang"];


        language = Convert.ToString(SessionManager.GetLanguage(language));

        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(language);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);

        base.FrameworkInitialize();
    }
}

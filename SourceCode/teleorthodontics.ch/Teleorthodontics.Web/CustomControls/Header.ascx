﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Header.ascx.cs" Inherits="UserControl_Header" %>
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span><i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="/"></a>
        </div>
        <div id="language-wrapper">
            <% if (language == "fr-FR")
               { %>
                <a id="current-language" href="javascript:void(0)" class="language lang_a"><img src="/images/french_flag_rotate_selected.png" alt="FR" />Français</a>
            <% }
               else if (language == "de-DE")
               { %>
                <a id="current-language" href="javascript:void(0)" class="language lang_a"><img src="/images/german_flag_rotate_selected.png" alt="DE" />Deutsch</a>
            <% }
               else if (language == "it-IT")
               { %>
                <a id="current-language" href="javascript:void(0)" class="language lang_a"><img src="/images/italian_flag_rotate_selected.png" alt="IT" />Italiano</a>
            <% }
               else if (language == "en-GB")
               { %>
                <a id="current-language" href="javascript:void(0)" class="language lang_a"><img src="/images/USA_flag_rotate_selected.png" alt="EN" />English</a>
            <% }
               else if (language == "es-ES")
               { %>
                <a id="current-language" href="javascript:void(0)" class="language lang_a"><img src="/images/spanish_flag_rotate_selected.png" alt="ES" />Español</a>
            <% }
               else if (language == "ru-RU")
               { %>
                <a id="current-language" href="javascript:void(0)" class="language last_a"><img src="/images/russian_flag_rotate_selected.png" alt="RU" />Русский</a>
            <% }
               else if (language == "ja-JP")
               { %>
                <a id="current-language" href="javascript:void(0)" class="language lang_a"><img src="/images/japanese_flag_rotate_selected.png" alt="JA" />日本人</a>
            <% }
               else if (language == "pt-PT")
               { %>
                <a id="current-language" href="javascript:void(0)" class="language lang_a"><img src="/images/portuguese_flag_rotate_selected.png" alt="PT" />Português</a>
            <% }
               else if (language == "vi-VN")
               { %>
                <a id="current-language" href="javascript:void(0)" class="language lang_a"><img src="/images/vietnamese_flag_rotate_selected.png" alt="VI" />Tiếng Việt</a>
            <% }
               else if (language == "zh-CN")
               { %>
                <a id="current-language" href="javascript:void(0)" class="language lang_a"><img src="/images/chinese_flag_rotate_selected.png" alt="ZH" />中國</a>
            <% }else{ %>
                <a id="current-language" href="javascript:void(0)" class="language lang_a"><img src="/images/french_flag_rotate_selected.png" alt="FR" />Français</a>
            <% }%>
            <ul class="logo-and-mobile-area">
                <% if (language == "fr-FR")
                   { %>
                    <li><a href="<%=url1 %>?lang=de-DE" class="language lang_a"><img src="/images/german_flag_rotate.png" alt="DE" />Deutsch</a></li>
                    <li><a href="<%=url1 %>?lang=it-IT" class="language lang_a"><img src="/images/italian_flag_rotate.png" alt="IT" />Italiano</a></li>
                    <li><a href="<%=url1 %>?lang=en-GB" class="language lang_a"><img src="/images/USA_flag_rotate.png" alt="EN" />English</a></li>
                    <li><a href="<%=url1 %>?lang=es-ES" class="language lang_a"><img src="/images/spanish_flag_rotate.png" alt="ES" />Español</a></li>
                    <li><a href="<%=url1 %>?lang=ru-RU" class="language last_a"><img src="/images/russian_flag_rotate.png" alt="RU" />Русский</a></li>
                    <li><a href="<%=url1 %>?lang=pt-PT" class="language lang_a"><img src="/images/portuguese_flag_rotate.png" alt="PT" />Português</a></li>
                    <li><a href="<%=url1 %>?lang=zh-CN" class="language lang_a"><img src="/images/chinese_flag_rotate.png" alt="ZH" />中國</a></li>
                    <li><a href="<%=url1 %>?lang=vi-VN" class="language lang_a"><img src="/images/vietnamese_flag_rotate.png" alt="VI" />Tiếng Việt</a></li>
                    <li><a href="<%=url1 %>?lang=ja-JP" class="language lang_a"><img src="/images/japanese_flag_rotate.png" alt="JA" />日本人</a></li>
                <% }
                   else if (language == "de-DE")
                   { %>
                    <li><a href="<%=url1 %>?lang=fr-FR" class="language lang_a"><img src="/images/french_flag_rotate.png" alt="FR" />Français</a></li>
                    <li><a href="<%=url1 %>?lang=it-IT" class="language lang_a"><img src="/images/italian_flag_rotate.png" alt="IT" />Italiano</a></li>
                    <li><a href="<%=url1 %>?lang=en-GB" class="language lang_a"><img src="/images/USA_flag_rotate.png" alt="EN" />English</a></li>
                    <li><a href="<%=url1 %>?lang=es-ES" class="language lang_a"><img src="/images/spanish_flag_rotate.png" alt="ES" />Español</a></li>
                    <li><a href="<%=url1 %>?lang=ru-RU" class="language last_a"><img src="/images/russian_flag_rotate.png" alt="RU" />Русский</a></li>
                    <li><a href="<%=url1 %>?lang=pt-PT" class="language lang_a"><img src="/images/portuguese_flag_rotate.png" alt="PT" />Português</a></li>
                    <li><a href="<%=url1 %>?lang=zh-CN" class="language lang_a"><img src="/images/chinese_flag_rotate.png" alt="ZH" />中國</a></li>
                    <li><a href="<%=url1 %>?lang=vi-VN" class="language lang_a"><img src="/images/vietnamese_flag_rotate.png" alt="VI" />Tiếng Việt</a></li>
                    <li><a href="<%=url1 %>?lang=ja-JP" class="language lang_a"><img src="/images/japanese_flag_rotate.png" alt="JA" />日本人</a></li>
                <% }
                   else if (language == "it-IT")
                   { %>
                    <li><a href="<%=url1 %>?lang=fr-FR" class="language lang_a"><img src="/images/french_flag_rotate.png" alt="FR" />Français</a></li>
                    <li><a href="<%=url1 %>?lang=de-DE" class="language lang_a"><img src="/images/german_flag_rotate.png" alt="DE" />Deutsch</a></li>
                    <li><a href="<%=url1 %>?lang=en-GB" class="language lang_a"><img src="/images/USA_flag_rotate.png" alt="EN" />English</a></li>
                    <li><a href="<%=url1 %>?lang=es-ES" class="language lang_a"><img src="/images/spanish_flag_rotate.png" alt="ES" />Español</a></li>
                    <li><a href="<%=url1 %>?lang=ru-RU" class="language last_a"><img src="/images/russian_flag_rotate.png" alt="RU" />Русский</a></li>
                    <li><a href="<%=url1 %>?lang=pt-PT" class="language lang_a"><img src="/images/portuguese_flag_rotate.png" alt="PT" />Português</a></li>
                    <li><a href="<%=url1 %>?lang=zh-CN" class="language lang_a"><img src="/images/chinese_flag_rotate.png" alt="ZH" />中國</a></li>
                    <li><a href="<%=url1 %>?lang=vi-VN" class="language lang_a"><img src="/images/vietnamese_flag_rotate.png" alt="VI" />Tiếng Việt</a></li>
                    <li><a href="<%=url1 %>?lang=ja-JP" class="language lang_a"><img src="/images/japanese_flag_rotate.png" alt="JA" />日本人</a></li>
                <% }
                   else if (language == "en-GB")
                   { %>
                    <li><a href="<%=url1 %>?lang=fr-FR" class="language lang_a"><img src="/images/french_flag_rotate.png" alt="FR" />Français</a></li>
                    <li><a href="<%=url1 %>?lang=de-DE" class="language lang_a"><img src="/images/german_flag_rotate.png" alt="DE" />Deutsch</a></li>
                    <li><a href="<%=url1 %>?lang=it-IT" class="language lang_a"><img src="/images/italian_flag_rotate.png" alt="IT" />Italiano</a></li>
                    <li><a href="<%=url1 %>?lang=es-ES" class="language lang_a"><img src="/images/spanish_flag_rotate.png" alt="ES" />Español</a></li>
                    <li><a href="<%=url1 %>?lang=ru-RU" class="language last_a"><img src="/images/russian_flag_rotate.png" alt="RU" />Русский</a></li>
                    <li><a href="<%=url1 %>?lang=pt-PT" class="language lang_a"><img src="/images/portuguese_flag_rotate.png" alt="PT" />Português</a></li>
                    <li><a href="<%=url1 %>?lang=zh-CN" class="language lang_a"><img src="/images/chinese_flag_rotate.png" alt="ZH" />中國</a></li>
                    <li><a href="<%=url1 %>?lang=vi-VN" class="language lang_a"><img src="/images/vietnamese_flag_rotate.png" alt="VI" />Tiếng Việt</a></li>
                    <li><a href="<%=url1 %>?lang=ja-JP" class="language lang_a"><img src="/images/japanese_flag_rotate.png" alt="JA" />日本人</a></li>
                <% }
                   else if (language == "es-ES")
                   { %>
                    <li><a href="<%=url1 %>?lang=fr-FR" class="language lang_a"><img src="/images/french_flag_rotate.png" alt="FR" />Français</a></li>
                    <li><a href="<%=url1 %>?lang=de-DE" class="language lang_a"><img src="/images/german_flag_rotate.png" alt="DE" />Deutsch</a></li>
                    <li><a href="<%=url1 %>?lang=it-IT" class="language lang_a"><img src="/images/italian_flag_rotate.png" alt="IT" />Italiano</a></li>
                    <li><a href="<%=url1 %>?lang=en-GB" class="language lang_a"><img src="/images/USA_flag_rotate.png" alt="EN" />English</a></li>
                    <li><a href="<%=url1 %>?lang=ru-RU" class="language last_a"><img src="/images/russian_flag_rotate.png" alt="RU" />Русский</a></li>
                    <li><a href="<%=url1 %>?lang=pt-PT" class="language lang_a"><img src="/images/portuguese_flag_rotate.png" alt="PT" />Português</a></li>
                    <li><a href="<%=url1 %>?lang=zh-CN" class="language lang_a"><img src="/images/chinese_flag_rotate.png" alt="ZH" />中國</a></li>
                    <li><a href="<%=url1 %>?lang=vi-VN" class="language lang_a"><img src="/images/vietnamese_flag_rotate.png" alt="VI" />Tiếng Việt</a></li>
                    <li><a href="<%=url1 %>?lang=ja-JP" class="language lang_a"><img src="/images/japanese_flag_rotate.png" alt="JA" />日本人</a></li>
                <% }
                   else if (language == "ru-RU")
                   { %>
                    <li><a href="<%=url1 %>?lang=fr-FR" class="language lang_a"><img src="/images/french_flag_rotate.png" alt="FR" />Français</a></li>
                    <li><a href="<%=url1 %>?lang=de-DE" class="language lang_a"><img src="/images/german_flag_rotate.png" alt="DE" />Deutsch</a></li>
                    <li><a href="<%=url1 %>?lang=it-IT" class="language lang_a"><img src="/images/italian_flag_rotate.png" alt="IT" />Italiano</a></li>
                    <li><a href="<%=url1 %>?lang=en-GB" class="language lang_a"><img src="/images/USA_flag_rotate.png" alt="EN" />English</a></li>
                    <li><a href="<%=url1 %>?lang=es-ES" class="language lang_a"><img src="/images/spanish_flag_rotate.png" alt="ES" />Español</a></li>
                    <li><a href="<%=url1 %>?lang=pt-PT" class="language lang_a"><img src="/images/portuguese_flag_rotate.png" alt="PT" />Português</a></li>
                    <li><a href="<%=url1 %>?lang=zh-CN" class="language lang_a"><img src="/images/chinese_flag_rotate.png" alt="ZH" />中國</a></li>
                    <li><a href="<%=url1 %>?lang=vi-VN" class="language lang_a"><img src="/images/vietnamese_flag_rotate.png" alt="VI" />Tiếng Việt</a></li>
                    <li><a href="<%=url1 %>?lang=ja-JP" class="language lang_a"><img src="/images/japanese_flag_rotate.png" alt="JA" />日本人</a></li>
                <% }
                   else if (language == "ja-JP")
                   { %>
                    <li><a href="<%=url1 %>?lang=fr-FR" class="language lang_a" title=""><img src="/images/french_flag_rotate.png" alt="FR" />Français</a></li>
                    <li><a href="<%=url1 %>?lang=de-DE" class="language lang_a" title=""><img src="/images/german_flag_rotate.png" alt="DE" />Deutsch</a></li>
                    <li><a href="<%=url1 %>?lang=it-IT" class="language lang_a" title=""><img src="/images/italian_flag_rotate.png" alt="IT" />Italiano</a></li>
                    <li><a href="<%=url1 %>?lang=en-GB" class="language lang_a" title=""><img src="/images/USA_flag_rotate.png" alt="EN" />English</a></li>
                    <li><a href="<%=url1 %>?lang=es-ES" class="language lang_a" title=""><img src="/images/spanish_flag_rotate.png" alt="ES" />Español</a></li>
                    <li><a href="<%=url1 %>?lang=ru-RU" class="language last_a" title=""><img src="/images/russian_flag_rotate.png" alt="RU" />Русский</a></li>
                    <li><a href="<%=url1 %>?lang=pt-PT" class="language lang_a" title=""><img src="/images/portuguese_flag_rotate.png" alt="PT" />Português</a></li>
                    <li><a href="<%=url1 %>?lang=zh-CN" class="language lang_a" title=""><img src="/images/chinese_flag_rotate.png" alt="ZH" />中國</a></li>
                    <li><a href="<%=url1 %>?lang=vi-VN" class="language lang_a" title=""><img src="/images/vietnamese_flag_rotate.png" alt="VI" />Tiếng Việt</a></li>
                <% }
                   else if (language == "pt-PT")
                   { %>
                    <li><a href="<%=url1 %>?lang=fr-FR" class="language lang_a"><img src="/images/french_flag_rotate.png" alt="FR" />Français</a></li>
                    <li><a href="<%=url1 %>?lang=de-DE" class="language lang_a"><img src="/images/german_flag_rotate.png" alt="DE" />Deutsch</a></li>
                    <li><a href="<%=url1 %>?lang=it-IT" class="language lang_a"><img src="/images/italian_flag_rotate.png" alt="IT" />Italiano</a></li>
                    <li><a href="<%=url1 %>?lang=en-GB" class="language lang_a"><img src="/images/USA_flag_rotate.png" alt="EN" />English</a></li>
                    <li><a href="<%=url1 %>?lang=es-ES" class="language lang_a"><img src="/images/spanish_flag_rotate.png" alt="ES" />Español</a></li>
                    <li><a href="<%=url1 %>?lang=ru-RU" class="language last_a"><img src="/images/russian_flag_rotate.png" alt="RU" />Русский</a></li>
                    <li><a href="<%=url1 %>?lang=zh-CN" class="language lang_a"><img src="/images/chinese_flag_rotate.png" alt="ZH" />中國</a></li>
                    <li><a href="<%=url1 %>?lang=vi-VN" class="language lang_a"><img src="/images/vietnamese_flag_rotate.png" alt="VI" />Tiếng Việt</a></li>
                    <li><a href="<%=url1 %>?lang=ja-JP" class="language lang_a"><img src="/images/japanese_flag_rotate.png" alt="JA" />日本人</a></li>
                <% }
                   else if (language == "vi-VN")
                   { %>
                    <li><a href="<%=url1 %>?lang=fr-FR" class="language lang_a"><img src="/images/french_flag_rotate.png" alt="FR" />Français</a></li>
                    <li><a href="<%=url1 %>?lang=de-DE" class="language lang_a"><img src="/images/german_flag_rotate.png" alt="DE" />Deutsch</a></li>
                    <li><a href="<%=url1 %>?lang=it-IT" class="language lang_a"><img src="/images/italian_flag_rotate.png" alt="IT" />Italiano</a></li>
                    <li><a href="<%=url1 %>?lang=en-GB" class="language lang_a"><img src="/images/USA_flag_rotate.png" alt="EN" />English</a></li>
                    <li><a href="<%=url1 %>?lang=es-ES" class="language lang_a"><img src="/images/spanish_flag_rotate.png" alt="ES" />Español</a></li>
                    <li><a href="<%=url1 %>?lang=ru-RU" class="language last_a"><img src="/images/russian_flag_rotate.png" alt="RU" />Русский</a></li>
                    <li><a href="<%=url1 %>?lang=pt-PT" class="language lang_a"><img src="/images/portuguese_flag_rotate.png" alt="PT" />Português</a></li>
                    <li><a href="<%=url1 %>?lang=zh-CN" class="language lang_a"><img src="/images/chinese_flag_rotate.png" alt="ZH" />中國</a></li>
                    <li><a href="<%=url1 %>?lang=ja-JP" class="language lang_a"><img src="/images/japanese_flag_rotate.png" alt="JA" />日本人</a></li>
                <% }
                   else if (language == "zh-CN")
                   { %>
                    <li><a href="<%=url1 %>?lang=fr-FR" class="language lang_a"><img src="/images/french_flag_rotate.png" alt="FR" />Français</a></li>
                    <li><a href="<%=url1 %>?lang=de-DE" class="language lang_a"><img src="/images/german_flag_rotate.png" alt="DE" />Deutsch</a></li>
                    <li><a href="<%=url1 %>?lang=it-IT" class="language lang_a"><img src="/images/italian_flag_rotate.png" alt="IT" />Italiano</a></li>
                    <li><a href="<%=url1 %>?lang=en-GB" class="language lang_a"><img src="/images/USA_flag_rotate.png" alt="EN" />English</a></li>
                    <li><a href="<%=url1 %>?lang=es-ES" class="language lang_a"><img src="/images/spanish_flag_rotate.png" alt="ES" />Español</a></li>
                    <li><a href="<%=url1 %>?lang=ru-RU" class="language last_a"><img src="/images/russian_flag_rotate.png" alt="RU" />Русский</a></li>
                    <li><a href="<%=url1 %>?lang=pt-PT" class="language lang_a"><img src="/images/portuguese_flag_rotate.png" alt="PT" />Português</a></li>
                    <li><a href="<%=url1 %>?lang=vi-VN" class="language lang_a"><img src="/images/vietnamese_flag_rotate.png" alt="VI" />Tiếng Việt</a></li>
                    <li><a href="<%=url1 %>?lang=ja-JP" class="language lang_a"><img src="/images/japanese_flag_rotate.png" alt="JA" />日本人</a></li>
                <% }else{ %>
                    <li><a href="<%=url1 %>?lang=de-DE" class="language lang_a"><img src="/images/german_flag_rotate.png" alt="DE" />Deutsch</a></li>
                    <li><a href="<%=url1 %>?lang=it-IT" class="language lang_a"><img src="/images/italian_flag_rotate.png" alt="IT" />Italiano</a></li>
                    <li><a href="<%=url1 %>?lang=en-GB" class="language lang_a"><img src="/images/USA_flag_rotate.png" alt="EN" />English</a></li>
                    <li><a href="<%=url1 %>?lang=es-ES" class="language lang_a"><img src="/images/spanish_flag_rotate.png" alt="ES" />Español</a></li>
                    <li><a href="<%=url1 %>?lang=ru-RU" class="language last_a"><img src="/images/russian_flag_rotate.png" alt="RU" />Русский</a></li>
                    <li><a href="<%=url1 %>?lang=pt-PT" class="language lang_a"><img src="/images/portuguese_flag_rotate.png" alt="PT" />Português</a></li>
                    <li><a href="<%=url1 %>?lang=zh-CN" class="language lang_a"><img src="/images/chinese_flag_rotate.png" alt="ZH" />中國</a></li>
                    <li><a href="<%=url1 %>?lang=vi-VN" class="language lang_a"><img src="/images/vietnamese_flag_rotate.png" alt="VI" />Tiếng Việt</a></li>
                    <li><a href="<%=url1 %>?lang=ja-JP" class="language lang_a"><img src="/images/japanese_flag_rotate.png" alt="JA" />日本人</a></li>
                <% }%>
            </ul>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="page-scroll" href="how-it-works.aspx"><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:header1%>" /></a>
                </li>
                <li>
                    <a class="page-scroll" href="pricing.aspx"><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:header2%>" /></a>
                </li>
                <li>
                    <a class="page-scroll" href="contact.aspx"><asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:header3%>" /></a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Footer.ascx.cs" Inherits="UserControl_Footer" %>
<footer>
    <div class="container">
        <p>&copy; <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:footer1%>" /></p>
        <ul class="list-inline">
            <li>
                <a href="/privacy.aspx"><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:footer2%>" /></a>
            </li>
            <li>
                <a href="/terms.aspx"><asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:footer3%>" /></a>
            </li>
            <li>
                <a href="/faq.aspx"><asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:footer4%>" /></a>
            </li>
        </ul>
    </div>
</footer>
<!-- jQuery -->
<script src="lib/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="lib/bootstrap/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

<!-- Theme JavaScript -->
<script src="js/new-age.min.js"></script>
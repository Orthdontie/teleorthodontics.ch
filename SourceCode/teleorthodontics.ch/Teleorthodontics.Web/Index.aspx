﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/General.Master" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Teleorthodontics.Web.Index" Culture="Auto" UICulture="Auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="bodyattributes" runat="server">
    id="smile-analysis" class="homepage"
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
    <title><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:title%>" /></title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="<asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:metadescription%>" />" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="header-content">
                        <div class="header-content-inner" id="header-chunk">
                            <h1>
                                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:index1%>" />
                            </h1>
                            <a href="Registration.aspx" class="btn btn-outline btn-xl page-scroll">
                                <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:index2%>" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="header-iphonegirl" class="<asp:Literal ID="Literal4a" runat="server" Text="<%$ Resources:index29%>" />"></div>
            </div>
        </div>
    </header>

    <section id="download" class="download bg-primary text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-heading">
                        <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:index3%>" />
                    </h2>
                    <p>
                        <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:index4%>" />
                    </p>
                    <div class="badges">
                        <a class="badge-link" href="#"><img src="img/google-play-badge.png" alt="<asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:index5%>" />"></a>
                        <a class="badge-link" href="#"><img src="img/app-store-badge.png" alt="<asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:index6%>" />"></a>
                        <a class="badge-link" href="#"><img src="img/windows-store-badge.png" alt="<asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:index7%>" />"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="features" class="features">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="section-heading">
                        <h2>
                            <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:index8%>" />
                        </h2>
                        <p class="text-muted">
                            <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:index9%>" />
                        </p>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="device-container">
                        <div class="device-mockup iphone6_plus portrait white">
                            <div class="device">
                                <div class="screen">
                                    <img src="img/app-screenshot-<asp:Literal ID="Literal9a" runat="server" Text="<%$ Resources:index30%>" />.png" class="img-responsive" alt="<asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:index10%>" />">
                                </div>
                                <div class="button">
                                    <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div id="orthodonticsteps" class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="feature-item">
                                    <i class="icon-screen-smartphone text-primary"></i>
                                    <h3>
                                        <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:index11%>" />
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:index12%>" />
                                    </p>
                                    <a id="smileanalysis" href="Registration.aspx" class="btn btn-outline btn-xl page-scroll">
                                        <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:index13%>" />
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="feature-item">
                                    <i class="icon-info text-primary"></i>
                                    <h3>
                                        <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:index14%>" />
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:index15%>" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="feature-item">
                                    <i id="take-impression" class="icon-frame text-primary"></i>
                                    <h3>
                                        <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:index16%>" />
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:index17%>" />
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="feature-item">
                                    <i class="icon-present text-primary"></i>
                                    <h3>
                                        <asp:Literal ID="Literal20" runat="server" Text="<%$ Resources:index18%>" />
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:index19%>" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="feature-item">
                                    <i class="icon-camera text-primary"></i>
                                    <h3>
                                        <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:index20%>" />
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal23" runat="server" Text="<%$ Resources:index21%>" />
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="feature-item">
                                    <i class="icon-check text-primary"></i>
                                    <h3>
                                        <asp:Literal ID="Literal24" runat="server" Text="<%$ Resources:index22%>" />
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal25" runat="server" Text="<%$ Resources:index23%>" />
                                    </p>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:index24%>" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cta">
        <div class="cta-content">
            <div class="container">
                <h2>
                    <asp:Literal ID="Literal27" runat="server" Text="<%$ Resources:index25%>" />
                    <br><br>
                    <asp:Literal ID="Literal27a" runat="server" Text="<%$ Resources:index25a%>" />
                </h2>
                <a href="Registration.aspx" class="btn btn-outline btn-xl page-scroll">
                    <asp:Literal ID="Literal28" runat="server" Text="<%$ Resources:index26%>" />
                </a>
            </div>
        </div>
        <div class="overlay"></div>
    </section>

    <section id="contact" class="contact bg-primary">
        <div class="container">
            <h2>
                <asp:Literal ID="Literal29" runat="server" Text="<%$ Resources:index27%>" /> 
                <i class="fa fa-heart"></i> 
                <asp:Literal ID="Literal30" runat="server" Text="<%$ Resources:index28%>" />
            </h2>
            <ul class="list-inline list-social">
                <li class="social-twitter">
                    <a href="https://twitter.com/TeleOrthodontic" target="_blank"><i class="fa fa-twitter"></i></a>
                </li>
                <li class="social-facebook">
                    <a href="https://www.facebook.com/TeleOrthodontics-334215297012913/" target="_blank"><i class="fa fa-facebook"></i></a>
                </li>
                <li class="social-google-plus">
                    <a href="https://plus.google.com/108183723962557816143" target="_blank"><i class="fa fa-google-plus"></i></a>
                </li>
            </ul>
        </div>
    </section>
</asp:Content>  
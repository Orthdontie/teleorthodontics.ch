﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Imaging;
using System.Linq;

namespace Teleorthodontics.Web
{
    public partial class CImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Create a random code and store it in the Session object.
            if (this.Session["CaptchaImageText"] == null)
            {
                this.Session["CaptchaImageText"] = GenerateRandomCode();
            }
            // Create a CAPTCHA image using the text stored in the Session object.
            RandomImage ci = new RandomImage(this.Session["CaptchaImageText"].ToString(), 300, 75);
            // Change the response headers to output a JPEG image.
            this.Response.Clear();
            this.Response.ContentType = "image/jpeg";
            // Write the image to the response stream in JPEG format.
            ci.Image.Save(this.Response.OutputStream, ImageFormat.Jpeg);
            // Dispose of the CAPTCHA image object.
            ci.Dispose();
        }
        public static string GenerateRandomCode()
        {
            string chars2 = string.Empty;
            var chars = "ABCDWXYZ23456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 5)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            

            if (result.ToString().Trim().Length < 5)
            {
                chars2 = result.ToString().Trim() + "2";
            }
            else
            {
                chars2 = result.ToString().Trim();
            }

            return chars2;
        }
    }
}
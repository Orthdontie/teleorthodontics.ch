﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stripe;
using System.Threading.Tasks;
using Teleorthodontics.Web.Helper;
using System.Data;
using Teleorthodontics.Web.DB;
using Newtonsoft.Json;
using System.Collections;


namespace Teleorthodontics.Web
{
    public partial class Payment : System.Web.UI.Page
    {
        public string UserId = "0";
        public bool isValidGuid = true;
        public string PaymentAmount = string.Empty;
        DatabaseHelper databaseHelper = new DatabaseHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            PaymentAmount = ConfigurationManager.AppSettings["PaymentAmount"];
            string encryptUserId = Request.QueryString["UserId"];
            string guid = Request.QueryString["u"];
            if (!string.IsNullOrEmpty(encryptUserId) && !string.IsNullOrEmpty(guid))
            {
                UserId = Common.Decrypt(encryptUserId);
                isValidGuid = Common.IsValidGuid(guid);
            }
            
            if (!IsPostBack)
            {
                if (!IsValidUser(UserId) || !isValidGuid)
                {
                    var msg = "Invalid Url";
                    DisplayMessage(msg, false);
                }
                else if (IsValidUser(UserId) && isValidGuid)
                {
                    paymentHeading.Visible=true;
                    PopulateDayMonthYear();
                }
                else
                {
                    btnPayment.Attributes.Remove("disabled");
                }

                
            }

        }

        public void GetPlanByGuid()
        {
            DatabaseHelper databaseHelper = new DatabaseHelper();
          //  String query = @"select P.Id,P.Name + ' - ' + CASE WHEN P.Interval = 'month' THEN 'Monthly' ELSE P.Interval END AS Name,P.StripePlanId,P.Interval from Plans P join RecommendedPlans R on p.Id=R.PlanId where R.UserId='" + UserId + "' AND p.IsActive=1 order by P.Name";
            String query = @"select P.Id,P.Name + ' - ' + CASE WHEN P.Interval = 'month' THEN 'Monthly' ELSE P.Interval END AS Name,P.StripePlanId,P.Interval from Plans P join RecommendedPlans R on p.Id=R.PlanId where R.UserId='" + UserId + "' AND p.IsActive=1 and R.RecommendedGuid='" + Request.QueryString["u"].ToString() + "' and IsSubscribe is null order by P.Name";

            DataSet ds = new DataSet();
            ds = databaseHelper.ExecuteDataSet(query, CommandType.Text);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlPlans.DataSource = ds;
                ddlPlans.DataTextField = "Name";
                ddlPlans.DataValueField = "Id";
                ddlPlans.DataBind();
                ddlPlans.Items.Insert(0, new ListItem("--Select--", "0"));
            }
            else
            {
                ddlPlans.Visible = false;
                lblChoosePlan.Text = "Payment Amount";
                txtTotalAmount.Text = PaymentAmount;
                txtTotalAmount.Visible = true;
            }

        }
        
        public void GetCardType()
        {
            Array itemNames = System.Enum.GetNames(typeof(CardType));
            foreach (String name in itemNames)
            {
                Int32 value = (Int32)Enum.Parse(typeof(CardType), name);
                ListItem listItem = new ListItem(name, value.ToString());
                ddlCardType.Items.Add(listItem);
            }
            ddlCardType.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        
        public void PopulateDayMonthYear()
        {
            GetPlanByGuid();
            GetCardType();
            ddlMonth.Items.Add("Month");
            for (int i = 1; i <= 12; i++)
            {
                ddlMonth.Items.Add(Convert.ToString(i));
            }
            ddlYear.Items.Add("Year");
            int currentYear = Convert.ToInt16(DateTime.Now.Year.ToString());
            for (int i = currentYear; i <= currentYear + 30; i++)
            {
                ddlYear.Items.Add(Convert.ToString(i));
            }
        }
        
        public bool IsValidUser(string userId)
        {
            String query = @"select TOP 1 Email FROM OnlineConsultation where UserId='" + userId + "'";
            var userEmail = databaseHelper.ExecuteScalar(query, CommandType.Text);
            if (userEmail != null)
            {
                return true;
            }
            return false;
        }

        public bool IsExistCreditCardInfo(string userId)
        {
            String query = @"select TOP 1 [StripePlanId] FROM CreditCardInfo where [UserId]='" + userId + "' AND IsActive=1";
            var UserInfoId = databaseHelper.ExecuteScalar(query, CommandType.Text);
            if (UserInfoId != null)
            {
                string msg = UserInfoId.ToString() == "" ? "Already activated." : "Subscription already activated.";
                DisplayMessage(msg, false);
                return true;
            }
            String Requery = @"select TOP 1 UserId FROM RecommendedPlans where [UserId]='" + userId + "'";
            var UserId = databaseHelper.ExecuteScalar(Requery, CommandType.Text);
            if (UserId == null)
            {
                DisplayMessage("Invalid Patient", false);
                return true;
            }
            return false;
        }
        
        protected void btnPayment_Click(object sender, EventArgs e)
        {
            try
            {
                string userEmail = string.Empty;
                string firstName = string.Empty;
                string lastName= string.Empty;
                string accountId = string.Empty;
                String query = @"select top 1 OC.Email,U.User_Fname,U.User_Lname,ISNULL(U.AccountingId, '' ) AS AccountingId from OnlineConsultation OC Inner Join user_table U ON OC.UserId=U.User_Id where OC.UserId='" + UserId + "'";
                var dr = databaseHelper.ExecuteReader(query, CommandType.Text);

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        userEmail = SqlValueConverter.GetString(dr,"Email");
                        firstName = SqlValueConverter.GetString(dr, "User_Fname");
                        lastName = SqlValueConverter.GetString(dr, "User_Lname");
                        accountId = SqlValueConverter.GetString(dr, "AccountingId");
                    }

                    dr.Close();
                }

                if (CreditCardAttribute.IsValid(txtCardNumber.Text, (CardType)Convert.ToInt32(ddlCardType.SelectedValue)))
                {
                    PlanViewModel customerinfo = new PlanViewModel();
                    customerinfo.FirstName = firstName;
                    customerinfo.LastName = lastName;
                    customerinfo.AccountId = accountId;
                    customerinfo.Name = txtFullName.Text;
                    customerinfo.CardNumber = txtCardNumber.Text;
                    customerinfo.Email = userEmail;
                    customerinfo.UserId = UserId;
                    customerinfo.ExpiryYear = Convert.ToInt32(ddlYear.SelectedValue);
                    customerinfo.ExpiryMonth = Convert.ToInt32(ddlMonth.SelectedValue);
                    // customerinfo.StripePlanId = "";
                    customerinfo.Cvc = txtCVC.Text;
                    if (ddlPlans.SelectedValue != "")
                    {
                        var planDetail = GetPlan(ddlPlans.SelectedValue);
                        customerinfo.StripePlanId = planDetail.StripePlanId;
                        customerinfo.TotalCost = planDetail.Amount;
                    }
                    else
                    {
                        customerinfo.TotalCost = Convert.ToDecimal(PaymentAmount);
                    }
                    SingleModelResponse<StripeSubscriptionResponse> response;
                    if (customerinfo.StripePlanId == null || customerinfo.StripePlanId == "")
                    {
                        response = StripeApiClient.CreateCustomerAndPaymentOneTime(customerinfo);
                    }
                    else
                    {
                        response = StripeApiClient.CreateCustomerAndSubscription(customerinfo);
                    }

                    if (response.IsSuccess)
                    {
                        string paymentDetails = string.Empty;
                        string paymentStatus = string.Empty;
                        string transactionId = string.Empty;
                        var msg = string.Empty;
                        long? planId = null;
                        decimal totalCost = 0;
                        decimal discount = 0;
                        if (ddlPlans.SelectedValue != "")
                        {
                            DataTable dtPlan = StripeApi.GetPlan(ddlPlans.SelectedValue);
                            if (dtPlan != null && dtPlan.Rows.Count > 0)
                            {
                                if (dtPlan.Rows[0]["Id"] != null)
                                    planId = Convert.ToInt64(dtPlan.Rows[0]["Id"]);
                                if (dtPlan.Rows[0]["Amount"] != null)
                                    totalCost = Convert.ToDecimal(dtPlan.Rows[0]["Amount"]);
                                if (dtPlan.Rows[0]["Discount"] != null)
                                    discount = Convert.ToDecimal(dtPlan.Rows[0]["Discount"]);
                            }
                            databaseHelper.ExecuteNonQuery("Insert Into CreditCardInfo(UserId,StripeCustomerID,StripeSubscriptionID,StripePlanId,StripeCardId,PlanId,Title,Name,CardNumber,CardType,ExpiryMonth,ExpiryYear,TotalCost,Discount,EPM,IsActive,DateCreated) values('" + UserId + "','" + response.Model.StripeCustomerID + "','" + response.Model.StripeSubscriptionID + "','" + response.Model.StripePlanID + "','" + response.Model.StripeCardId + "', " + planId + ",'TeleOrtho','" + txtFullName.Text + "','" + txtCardNumber.Text.Substring(txtCardNumber.Text.Length - 4) + "','" + ddlCardType.SelectedValue + "'," + Convert.ToInt32(ddlMonth.SelectedValue) + "," + Convert.ToInt32(ddlYear.SelectedValue) + "," + totalCost + "," + discount + ",'',1,GetDate())", CommandType.Text);
                            databaseHelper.ExecuteNonQuery("update RecommendedPlans set IsSubscribe=1 where UserId='" + UserId + "' and RecommendedGuid='" + Request.QueryString["u"].ToString() + "'", CommandType.Text);

                            if (customerinfo.StripePlanId == "")
                            {
                                msg = "Thank you! Your payment was processed successfully.";
                                paymentStatus = "Payment successfully";
                            }
                            else
                            {
                                msg = "Thank you! Your Subscription  activated successfully.";
                                paymentStatus = "Subscription  activated successfully";
                            }

                        }
                        else
                        {
                            //totalCost = Convert.ToDecimal(PaymentAmount);
                            totalCost = customerinfo.TotalCost;
                            databaseHelper.ExecuteNonQuery("Insert Into CreditCardInfo(UserId,StripeCustomerID,StripeSubscriptionID,StripePlanId,StripeCardId,Title,Name,CardNumber,CardType,ExpiryMonth,ExpiryYear,TotalCost,Discount,EPM,IsActive,DateCreated) values('" + UserId + "','" + response.Model.StripeCustomerID + "','" + response.Model.StripeSubscriptionID + "','" + response.Model.StripePlanID + "','" + response.Model.StripeCardId + "','TeleOrtho','" + txtFullName.Text + "','" + txtCardNumber.Text.Substring(txtCardNumber.Text.Length - 4) + "','" + ddlCardType.SelectedValue + "'," + Convert.ToInt32(ddlMonth.SelectedValue) + "," + Convert.ToInt32(ddlYear.SelectedValue) + "," + totalCost + "," + discount + ",'',1,GetDate())", CommandType.Text);
                            msg = "Thank you! Your payment was processed successfully.";
                            paymentStatus = "Payment successfully";
                            transactionId = "<tr><td><strong>TransactionId:</strong></td><td>" + response.Model.TransactionId + "</td></tr>";
                        }
                        paymentDetails = "<table><tr><td><strong>Status:</strong></td><td>" + paymentStatus + "</td></tr><tr><td><strong>Amount:  </strong></td><td> " + " " + totalCost + "</td></tr><tr><td><strong>Date:</strong></td><td>" + DateTime.Now.ToShortDateString() + "  " + DateTime.Now.ToShortTimeString() + "</td></tr>" + transactionId + "</table>";
                        SendPaymentConfirmationMail(UserId, customerinfo.Name, customerinfo.Email, paymentDetails);

                        dvPanel.Attributes.Add("class", "alert alert-success fade in");
                        DisplayMessage(msg, false);
                    }
                    else
                    {
                        var msg = "Subscription  not created. " + response.Message + " " + response.Message;
                        DisplayMessage(msg, true);
                    }


                }
            }
            catch (Exception ex)
            {

                var msg = ex.Message;
                DisplayMessage(msg, true);
            }

        }

        private PlanModel GetPlan(string planId)
        {
            var plan = new PlanModel();

            String query = @"select * FROM Plans where [Id]=" + planId + "  AND IsActive=1";
            DataSet ds = new DataSet();
            ds = databaseHelper.ExecuteDataSet(query, CommandType.Text);

            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];

                plan.PlanName = row["StripePlanId"].ToString();
                plan.Amount = Convert.ToDecimal(row["Amount"]);

                if (row["StripePlanId"] != DBNull.Value)
                    plan.StripePlanId = row["StripePlanId"].ToString();
                else
                    plan.StripePlanId = "";

                if (row["Currency"] != DBNull.Value)
                    plan.Currency = row["Currency"].ToString();
                else
                    plan.Currency = "";

                if (row["Interval"] != DBNull.Value)
                    plan.Interval = row["Interval"].ToString();
                else
                    plan.Interval = "";

                if (row["Discount"] != DBNull.Value)
                    plan.Discount = Convert.ToDecimal(row["Discount"]);
                else
                    plan.Discount = 0m;

                if (row["TrialPeriodDays"] != DBNull.Value)
                    plan.TrialPeriodDays = Convert.ToInt32(row["TrialPeriodDays"]);
                else
                    plan.TrialPeriodDays = 0;

                if (row["DurationInMonths"] != DBNull.Value)
                    plan.DurationInMonth = Convert.ToInt32(row["DurationInMonths"]);
                else
                    plan.DurationInMonth = 0;

            }

            return plan;
        }

        public void SendPaymentConfirmationMail(string UserId, string name, string toMail, string paymentDetails)
        {
            string emailsendFrom = string.Empty;
            string emailidofbcc = string.Empty;
            string _subject = string.Empty;
            string query = string.Empty;
            string prefLang = "English";
            query = "select e.Body,et.emailsentfrom,et.emailidofbcc,e.subject from EmailTemplateType et ";
            query += "inner join EmailTemplates e on e.TypeId=et.ID ";
            query += "where e.Type='PaymentConfirmation'";

            DatabaseHelper databaseHelper = new DatabaseHelper();
            DataSet ds = new DataSet();
            ds = databaseHelper.ExecuteDataSet(query, CommandType.Text);
            DataTable dtblLetter = ds.Tables[0];
            string emailBody = string.Empty;
            if (prefLang == "English")
            {
                if (!Convert.IsDBNull(dtblLetter.Rows[0][0]))
                    emailBody = (String)dtblLetter.Rows[0][0];
                if (!Convert.IsDBNull(dtblLetter.Rows[0][1]))
                    emailsendFrom = (String)dtblLetter.Rows[0][1];
                if (!Convert.IsDBNull(dtblLetter.Rows[0][2]))
                    emailidofbcc = (String)dtblLetter.Rows[0][2];
                _subject = (String)dtblLetter.Rows[0][3];
            }

            emailBody = emailBody.Replace("@@Name", name);
            emailBody = emailBody.Replace("@@PaymentDetails", paymentDetails);
            Sendmail.SendEmail(UserId, toMail, _subject, emailBody, emailsendFrom, emailidofbcc);
            //string User_Id, string _toMail, string _subject, string _sbMailDetails, string emailFrom, string emailidofbcc = null
        }

        public void DisplayMessage(string msg, Boolean IsDisplay)
        {
            dvMsg.Visible = true;
            lblMsg.Text = msg;
            dvContent.Visible = IsDisplay;
        }
    }    
}
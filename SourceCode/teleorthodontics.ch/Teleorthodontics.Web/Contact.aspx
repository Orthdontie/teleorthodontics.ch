﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/General2.Master" AutoEventWireup="true" CodeFile="contact.aspx.cs" Inherits="Teleorthodontics.Web.Contact" Culture="Auto" UICulture="Auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="bodyattributes" runat="server">
    id="contact"
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
    <title><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:title%>" /></title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="<asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:metadescription%>" />" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <section id="contact-section" class="contact bg-primary">
        <div class="container">
            <h2>
                <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:contact1%>" />
            </h2>
            <%--<div id="contact-info">
                <p>
                    <asp:Literal ID="Literal4a" runat="server" Text="<%$ Resources:contact2%>" />
                </p>
                <p>
                    <asp:Literal ID="Literal5a" runat="server" Text="<%$ Resources:contact3%>" />
                </p>
                <p>
                    <asp:Literal ID="Literal6a" runat="server" Text="<%$ Resources:contact4%>" />
                </p>
                <hr>
                <p>
                    <asp:Literal ID="Literal7a" runat="server" Text="<%$ Resources:contact5%>" />
                    <br/>
                    <asp:Literal ID="Literal8a" runat="server" Text="<%$ Resources:contact6%>" />
                    <br/>
                    <asp:Literal ID="Literal9a" runat="server" Text="<%$ Resources:contact7%>" />
                </p>
                <p>
                    <asp:Literal ID="Literal10a" runat="server" Text="<%$ Resources:contact8%>" />
                    <br/>
                    <asp:Literal ID="Literal11a" runat="server" Text="<%$ Resources:contact9%>" />
                    <br/>
                    <asp:Literal ID="Literal12a" runat="server" Text="<%$ Resources:contact10%>" />
                </p>
            </div>--%>
            <div id="<asp:Literal ID="Literal13a" runat="server" Text="<%$ Resources:contact27%>" />"></div>
            <div id="contact-form">
                <fieldset>
                    <asp:HiddenField ID="hdnCaptchaCode" runat="server" />
                    <div class="field">
                        <div runat="server" id="dvMsg" visible="false" class="col-md-12">
                            <div class="col-md-6 col-md-offset-3 alert alert-danger fade in">
                                <span id="lblmsg" runat="server" class="namecontent" style="display: inline-block; color: Red; height: 18px; width: 321px;"></span>
                            </div>
                        </div>
                    </div>
                 
                        <div class="field">
                            <label class="required star">*</label>
                            <asp:TextBox ID="txtFirstName" runat="server" placeholder="<%$ Resources:contact11%>"></asp:TextBox>
                        </div>
                        <div class="field">
                            <label class="required star">*</label>
                            <asp:TextBox ID="txtLastName" runat="server" placeholder="<%$ Resources:contact12%>"></asp:TextBox>
                        </div>
                        <div class="field">
                            <label class="required star">*</label>
                            <asp:TextBox ID="txtEmail" runat="server" placeholder="<%$ Resources:contact13%>"></asp:TextBox>
                            <br />
                            <span id="spnEmail" style="color: red"></span>
                        </div>
                        <div class="field">
                            <label class="required star">*</label>
                            <asp:TextBox ID="txtPhone" runat="server" placeholder="<%$ Resources:contact14%>"></asp:TextBox>
                            <br />
                            <span id="spnPhone" style="color: red"></span>
                        </div>

                        <div class="field">
                            <label class="required star">*</label>
                            <asp:DropDownList ID="ddlSubject" runat="server">
                                <asp:ListItem Text="<%$ Resources:contact15%>" Value="0"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:contact16%>" Value="1"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:contact17%>" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="field">
                            <label class="message required star">*</label>
                            <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Rows="2" Columns="20" placeholder="<%$ Resources:contact18%>"></asp:TextBox>
                        </div>
                        <div id="captcha-cnt" class="field">
                            <asp:Image ID="imgCaptcha" AlternateText="imgCaptcha" runat="server" ImageUrl="~/CImage.aspx" />
                            <br>
                            <label class="required star">*</label>
                            <asp:TextBox ID="txtCaptcha" runat="server" placeholder="<%$ Resources:contact19%>"></asp:TextBox>
                          <br />  <span id="spnCaptcha" style="color: red;"></span>
                        </div>
                        <div id="submit">
                            <asp:Button ID="btnSubmit" runat="server" Text="<%$ Resources:contact20%>" OnClientClick="return Validate()" CssClass="btn btn-outline btn-xl page-scroll" OnClick="btnSubmit_Click" />
                        </div>
                    
                </fieldset>
            </div>

        </div>
    </section>
    <script>

        Validate = function () {
            var IsError = false;
            var firstName = $("#ContentPlaceHolderBody_txtFirstName");
            var lastName = $("#ContentPlaceHolderBody_txtLastName");
            var email = $("#ContentPlaceHolderBody_txtEmail");
            var phone = $("#ContentPlaceHolderBody_txtPhone");
            var subject = $("#ContentPlaceHolderBody_ddlSubject")
            var message = $("#ContentPlaceHolderBody_txtMessage");
            var captcha = $("#ContentPlaceHolderBody_txtCaptcha");
            if (firstName.val().trim() == "") {
                firstName.css("border-color", "red");
                IsError = true;
            }
            else {
                firstName.css("border-color", "");
            }
            if (lastName.val().trim() == "") {
                lastName.css("border-color", "red");
                IsError = true;
            }
            else {
                lastName.css("border-color", "");
            }
            if (email.val().trim() == "") {
                email.css("border-color", "red");
                IsError = true;
            }
            else if (!IsEmail(email.val())) {
                email.css("border-color", "red");
                $("#spnEmail").text("<asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:contact24%>" />");
                IsError = true;
            }
            else {
                email.css("border-color", "");
            }
            if (phone.val().trim() == "") {
                phone.css("border-color", "red");
                IsError = true;
            }
            else if (!TelephoneCheck(phone.val())) {
                phone.css("border-color", "red");
                $("#spnPhone").text("<asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:contact25%>" />");
                IsError = true;
            }
            else {
                phone.css("border-color", "");
            }

            if (subject.val() == "0") {
                subject.css("border-color", "red");
                IsError = true;
            }
            else {
                subject.css("border-color", "");
            }
            if (message.val().trim() == "") {
                message.css("border-color", "red");
                IsError = true;
            }
            else {
                message.css("border-color", "");
            }

            if (captcha.val().trim() == "") {
                captcha.css("border-color", "red");
                IsError = true;
            }
            else if (captcha.val().trim() != $("#ContentPlaceHolderBody_hdnCaptchaCode").val()) {
                captcha.css("border-color", "red");
                $("#spnCaptcha").text("<asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:contact26%>" />");
                IsError = true;
            }
            else {
                captcha.css("border-color", "");
                $("#spnCaptcha").text("");
            }
            return !IsError;
        }

        function IsEmail(email) {
            return /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email);

        }
        function TelephoneCheck(str) {
            return /^(1\s|1|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/.test(str);
            // return /^(\([0-9]{3}\)|[0-9]{3}-)[0-9]{3}-[0-9]{4}$/.test(str);
        }
    </script>
</asp:Content>


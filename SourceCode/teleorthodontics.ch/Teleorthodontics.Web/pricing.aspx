﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/General.Master" AutoEventWireup="true" CodeFile="pricing.aspx.cs" Inherits="Teleorthodontics.Web.pricing" Culture="Auto" UICulture="Auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="bodyattributes" runat="server">
    id="pricing"
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
    <title><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:title%>" /></title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="<asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:metadescription%>" />" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <section id="pricing" class="pricing text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="section-heading">
                        <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:pricing1%>" />
                    </h2>
                    <p>
                        <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:pricing2%>" />
                        <br />
                        <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:pricing3%>" />
                    </p>
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <p class="or">
                                    <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:pricing4%>" />
                                </p>
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 id="left-pricing">
                                        <a href="<asp:Literal ID="Literal13a" runat="server" Text="<%$ Resources:pricing4a%>" />" target="_blank">
                                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:pricing5%>" />
                                            <span>
                                                <asp:Literal ID="Literal7a" runat="server" Text="<%$ Resources:pricing5a%>" />
                                            </span>
                                        </a>
                                    </h4>
                                    <%--<h4 class="subheading">
                                            <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:pricing6%>" />
                                    </h4>--%>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">
                                        <a id="learnmoreoninternet1" href="<asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:pricing7%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                            <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:pricing8%>" />
                                        </a>
                                        <span class="learnmoreoninternet1price">
                                            <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:pricing6%>" />
                                        </span>
                                    </p>
                                </div>
                                <!--<div class="timeline-body">
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:pricing9%>" />
                                    </p>
                                </div>-->
                            </div>
                            <div class="timeline-panel inverted">
                                <div class="timeline-heading">
                                    <h4>
                                        <a href="<asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:pricing10%>" />" target="_blank">
                                            <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:pricing11%>" />
                                            <span>
                                                <asp:Literal ID="Literal14a" runat="server" Text="<%$ Resources:pricing11a%>" />
                                            </span>
                                        </a>
                                    </h4>
                                    <%--<h4 class="subheading">
                                        <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:pricing12%>" />
                                    </h4>--%>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">
                                        <a id="learnmoreoninternet2a" href="<asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:pricing13%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                            <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:pricing14%>" />
                                        </a>
                                        <span class="learnmoreoninternet2price">
                                            <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:pricing12%>" />
                                        </span>
                                        <a id="learnmoreoninternet2" href="<asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:pricing15%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                            <asp:Literal ID="Literal20" runat="server" Text="<%$ Resources:pricing16%>" />
                                        </a>
                                    </p>
                                </div>
                                <!--<div class="timeline-body">
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:pricing17%>" />
                                    </p>
                                </div>-->
                            </div>
                        </li>
                    </ul>
                    <a id="learnmore" href="whats-included.aspx" class="btn btn-outline btn-xl page-scroll">
                        <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:pricing18%>" />
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
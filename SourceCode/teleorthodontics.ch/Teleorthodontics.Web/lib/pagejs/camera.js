﻿var hiddenField;

$(document).ready(function () {
    $("#video").show();
    $("#canvas").hide();
    $("#BtnCapture").show();
    $("#btnRetake").hide();
});

function StartCamera(uploadCtrl, btnId, btnHfID, lblError) {
    var isMobile = true; //initiate as false    
    //$("#" + uploadCtrl + "").removeAttr("onchange");
    //$("#" + uploadCtrl + "").attr("onchange", "fileSelected(this,'" + btnHfID + "','" + lblError + "');");
    //$("#" + uploadCtrl + "").click();
    // device detection
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
        isMobil=true;
    }

    if(isMobile){
        $("#" + uploadCtrl + "").removeAttr("onchange");
        $("#" + uploadCtrl + "").attr("onchange", "fileSelected(this,'" + btnHfID + "','" + lblError + "');");
        $("#" + uploadCtrl + "").click();
    }
    else {
        $("#video").show();
        $("#video").attr("src","");
        $("#canvas").hide();
        $("#BtnCapture").show();
        $("#btnRetake").hide();

        $("#modelPhoto").show();
        Capture(uploadCtrl, btnId, btnHfID, lblError);
    }
}

function Capture(uploadCtrl, btnId, btnHfID, lblError) {
    var canvas = $("#canvas"),
context = canvas[0].getContext("2d"),
video = $("#video")[0],
videoObj = { "video": true },
errHandler = function (error) {
    console.log("Video capture error: ", error.code);
};

    if (navigator.getUserMedia) { // Standard
        navigator.getUserMedia(videoObj, function (stream) {
            video.src = stream;
            video.play();
        }, errHandler);
    } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia(videoObj, function (stream) {
            video.src = window.webkitURL.createObjectURL(stream);
            video.play();
        }, errHandler);
    }
    else if (navigator.mozGetUserMedia) { // Firefox-prefixed
        navigator.mozGetUserMedia(videoObj, function (stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
        }, errHandler);
    }

    $("#BtnCapture").click(function (e) {
        $("#video").hide();
        $("#canvas").show();
        $("#btnRetake").show();
        $("#BtnCapture").hide();
        context.drawImage(video, 0, 0, 310, 200);
        var canvas = $("#canvas");
        var imgStr = canvas[0].toDataURL();
        //imgStr = imgStr.replace('data:image/png;base64,', '');
        $("#" + btnHfID + "").val(imgStr);
        //document.getElementById("<%= hfImage.ClientID %>").value = imgStr;
        //var img = document.getElementById('<%=ImgProfile.ClientID%>');
        //img.src = 'data:image/png;base64,' + imgStr;
    });

    $("#btnRetake").click(function (e) {
        $("#video").show();
        $("#canvas").hide();
        $("#BtnCapture").show();
        $("#btnRetake").hide();
    });
}

function CloseModel() {
    $('video').each(function (k, v) { jQuery('video')[k].pause(); });

    $("#modelPhoto").hide();
}

function fileSelected(ctrlUpload, btnHfID, lblError) {
    var file = ctrlUpload.files[0];
    var imgStr = "";//file.readAsDataURL();
    var reader = new FileReader();

    reader.addEventListener("load", function () {
        imgStr = reader.result;
    }, false);


    if (file) {
        reader.readAsDataURL(file);
    }

    setTimeout(function () {
        //$("#" + btnHfID + "").val(imgStr);
        uploadFile(ctrlUpload, btnHfID);
    },500);
    //document.getElementById('details').innerHTML = "";
    //for (var index = 0; index < count; index++) {
    //    var file = document.getElementById('fileToUpload').files[index];
    //    var fileSize = 0;
    //    if (file.size > 1024 * 1024)
    //        fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
    //    else
    //        fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
    //    document.getElementById('details').innerHTML += 'Name: ' + file.name + '<br>Size: ' + fileSize + '<br>Type: ' + file.type;
    //    document.getElementById('details').innerHTML += '<p>';
    //}
}

function uploadFile(fileToUpload, hfID) {
    var fd = new FormData();
    var count = fileToUpload.files.length;
    for (var index = 0; index < count; index++) {
        var file = fileToUpload.files[index];
        fd.append(file.name, file);
    }

    hiddenField = hfID;
    //alert("START");
    var xhr = new XMLHttpRequest();
    xhr.upload.addEventListener("progress", uploadProgress, false);
    xhr.addEventListener("load", uploadComplete, false);
    xhr.addEventListener("error", uploadFailed, false);
    xhr.addEventListener("abort", uploadCanceled, false);
    xhr.open("POST", "Uploader.ashx");
    xhr.send(fd);
    //alert("END");
   
}

function uploadProgress(evt) {
    if (evt.lengthComputable) {
        var percentComplete = Math.round(evt.loaded * 100 / evt.total);
        document.getElementById('progress').innerHTML = "Uploading the image : "+ percentComplete.toString() + '%';
        document.getElementById('progress').style.display = "block";
    }
    else {
        document.getElementById('progress').innerHTML = 'unable to compute';
    }
}


function uploadComplete(evt) {
    /* This event is raised when the server send back a response */
    $("#" + hiddenField + "").val(evt.target.responseText);
    document.getElementById('progress').style.display = "none";
}

function uploadFailed(evt) {
    alert("There was an error attempting to upload the file." + JSON.stringify(evt));
    document.getElementById('progress').style.display = "none";
}

function uploadCanceled(evt) {
    alert("The upload has been canceled by the user or the browser dropped the connection.");
    document.getElementById('progress').style.display = "none";
}

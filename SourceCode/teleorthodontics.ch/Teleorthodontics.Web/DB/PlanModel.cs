﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Teleorthodontics.Web.DB
{
    public class PlanModel
    {
        public string PlanName { get; set; }
        public decimal Amount { get; set; }
        public int DurationInMonth { get; set; }
        public string StripePlanId { get; set; }
        public decimal Discount { get; set; }
        public string Currency { get; set; }
        public string Interval { get; set; }
        public int TrialPeriodDays { get; set; }
    }
}
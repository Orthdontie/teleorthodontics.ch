﻿using System;
using System.Web;
using Microsoft.VisualBasic;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.Data.Common;
using System.IO;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Configuration;

namespace Teleorthodontics.Web.DB
{
    public class DatabaseHelper : IDisposable
    {
        private string strConnectionString;
        private DbConnection objConnection;
        private DbCommand objCommand;
        private DbProviderFactory objFactory = null;
        private bool boolHandleErrors;
        private string strLastError;
        private bool boolLogError;

        private string strLogFile;
        public DatabaseHelper(string connectionstring, Providers provider)
        {
            strConnectionString = connectionstring;
            switch (provider)
            {
                case Providers.SqlServer:
                    objFactory = SqlClientFactory.Instance;
                    break;
                case Providers.OleDb:
                    objFactory = OleDbFactory.Instance;
                    break;
                case Providers.ODBC:
                    objFactory = OdbcFactory.Instance;
                    break;
                case Providers.ConfigDefined:
                    string providername = ConfigurationManager.ConnectionStrings["connectionstring"].ProviderName;
                    switch (providername)
                    {
                        case "System.Data.SqlClient":
                            objFactory = SqlClientFactory.Instance;
                            break;
                        case "System.Data.OleDb":
                            objFactory = OleDbFactory.Instance;
                            break;
                        case "System.Data.Odbc":
                            objFactory = OdbcFactory.Instance;
                            break;
                    }
                    break;
            }
            objConnection = objFactory.CreateConnection();
            objCommand = objFactory.CreateCommand();

            objConnection.ConnectionString = strConnectionString;
            objCommand.Connection = objConnection;
        }

        public DatabaseHelper(Providers provider)
            : this(ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString, provider)
        {
        }

        public DatabaseHelper(string connectionstring)
            : this(connectionstring, Providers.SqlServer)
        {
        }

        public DatabaseHelper()
            : this(ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString, Providers.ConfigDefined)
        {
        }

        public bool HandleErrors
        {
            get { return boolHandleErrors; }
            set { boolHandleErrors = value; }
        }

        public string LastError
        {
            get { return strLastError; }
        }

        public bool LogErrors
        {
            get { return boolLogError; }
            set { boolLogError = value; }
        }

        public string LogFile
        {
            get { return strLogFile; }
            set { strLogFile = value; }
        }

        public int AddParameter(string name, object value)
        {
            DbParameter p = objFactory.CreateParameter();
            p.ParameterName = name;
            p.Value = value;
            return objCommand.Parameters.Add(p);
        }

        public int AddParameter(DbParameter parameter)
        {
            return objCommand.Parameters.Add(parameter);
        }

        public DbCommand Command
        {
            get { return objCommand; }
        }

        public void BeginTransaction()
        {
            if (objConnection.State == System.Data.ConnectionState.Closed)
            {
                objConnection.Open();
            }
            objCommand.Transaction = objConnection.BeginTransaction();
        }

        public void CommitTransaction()
        {
            objCommand.Transaction.Commit();
            objConnection.Close();
        }

        public void RollbackTransaction()
        {
            objCommand.Transaction.Rollback();
            objConnection.Close();
        }

        #region "ExecuteNonQuery"

        public int ExecuteNonQuery(string spName)
        {
            return ExecuteNonQuery(spName, CommandType.StoredProcedure, ConnectionState.CloseOnExit);
        }

        public int ExecuteNonQuery(string spName, params object[] parameterValues)
        {
            return ExecuteNonQuery(spName, ConnectionState.CloseOnExit, parameterValues);
        }

        public int ExecuteNonQuery(string spName, ConnectionState connectionstate__1, params object[] parameterValues)
        {
            objCommand.CommandText = spName;
            objCommand.CommandType = CommandType.StoredProcedure;

            DbParameter[] parameters = ParameterCache.GetSpParameterSet(objConnection, spName);
            AssignParameterValues(parameters, parameterValues);
            objCommand.Parameters.AddRange(parameters);

            int i = -1;
            try
            {
                if (objConnection.State == System.Data.ConnectionState.Closed)
                {
                    objConnection.Open();
                }
                i = objCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
            finally
            {
                objCommand.Parameters.Clear();
                if (connectionstate__1 == ConnectionState.CloseOnExit)
                {
                    objConnection.Close();
                }
            }

            return i;
        }

        public int ExecuteNonQuery(string query, CommandType commandtype)
        {
            return ExecuteNonQuery(query, commandtype, ConnectionState.CloseOnExit);
        }

        public int ExecuteNonQuery(string query, CommandType commandtype, ConnectionState connectionstate__1)
        {
            objCommand.CommandText = query;
            objCommand.CommandType = commandtype;

            int i = -1;
            try
            {
                if (objConnection.State == System.Data.ConnectionState.Closed)
                {
                    objConnection.Open();
                }
                i = objCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
            finally
            {
                objCommand.Parameters.Clear();
                if (connectionstate__1 == ConnectionState.CloseOnExit)
                {
                    objConnection.Close();
                }
            }

            return i;
        }

        #endregion

        #region "ExecuteScalar"
        public object ExecuteScalar(string spName)
        {
            return ExecuteScalar(spName, CommandType.StoredProcedure, ConnectionState.CloseOnExit);
        }

        public object ExecuteScalar(string spName, params object[] parameterValues)
        {
            return ExecuteScalar(spName, ConnectionState.CloseOnExit, parameterValues);
        }

        public object ExecuteScalar(string spName, ConnectionState connectionstate__1, params object[] parameterValues)
        {
            objCommand.CommandText = spName;
            objCommand.CommandType = CommandType.StoredProcedure;

            DbParameter[] parameters = ParameterCache.GetSpParameterSet(objConnection, spName);
            AssignParameterValues(parameters, parameterValues);
            objCommand.Parameters.AddRange(parameters);

            object o = null;
            try
            {
                if (objConnection.State == System.Data.ConnectionState.Closed)
                {
                    objConnection.Open();
                }
                o = objCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
            finally
            {
                objCommand.Parameters.Clear();
                if (connectionstate__1 == ConnectionState.CloseOnExit)
                {
                    objConnection.Close();
                }
            }

            return o;
        }

        public object ExecuteScalar(string query, CommandType commandtype)
        {
            return ExecuteScalar(query, commandtype, ConnectionState.CloseOnExit);
        }

        public object ExecuteScalar(string query, CommandType commandtype, ConnectionState connectionstate__1)
        {
            objCommand.CommandText = query;
            objCommand.CommandType = commandtype;
            object o = null;
            try
            {
                if (objConnection.State == System.Data.ConnectionState.Closed)
                {
                    objConnection.Open();
                }
                o = objCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
            finally
            {
                objCommand.Parameters.Clear();
                if (connectionstate__1 == ConnectionState.CloseOnExit)
                {
                    objConnection.Close();
                }
            }

            return o;
        }
        #endregion

        #region "ExecuteReader"
        public DbDataReader ExecuteReader(string spName)
        {
            return ExecuteReader(spName, CommandType.StoredProcedure, ConnectionState.CloseOnExit);
        }

        public DbDataReader ExecuteReader(string spName, params object[] parameterValues)
        {
            return ExecuteReader(spName, ConnectionState.CloseOnExit, parameterValues);
        }

        public DbDataReader ExecuteReader(string spName, ConnectionState connectionstate__1, params object[] parameterValues)
        {
            objCommand.CommandText = spName;
            objCommand.CommandType = CommandType.StoredProcedure;

            DbParameter[] parameters = ParameterCache.GetSpParameterSet(objConnection, spName);
            AssignParameterValues(parameters, parameterValues);
            objCommand.Parameters.AddRange(parameters);

            DbDataReader reader = null;
            try
            {
                if (objConnection.State == System.Data.ConnectionState.Closed)
                {
                    objConnection.Open();
                }
                if (connectionstate__1 == ConnectionState.CloseOnExit)
                {
                    reader = objCommand.ExecuteReader(CommandBehavior.CloseConnection);
                }
                else
                {
                    reader = objCommand.ExecuteReader();

                }
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
            finally
            {
                objCommand.Parameters.Clear();
            }

            return reader;
        }

        public DbDataReader ExecuteReader(string query, CommandType commandtype)
        {
            return ExecuteReader(query, commandtype, ConnectionState.CloseOnExit);
        }

        public DbDataReader ExecuteReader(string query, CommandType commandtype, ConnectionState connectionstate__1)
        {
            objCommand.CommandText = query;
            objCommand.CommandType = commandtype;
            DbDataReader reader = null;
            try
            {
                if (objConnection.State == System.Data.ConnectionState.Closed)
                {
                    objConnection.Open();
                }
                if (connectionstate__1 == ConnectionState.CloseOnExit)
                {
                    reader = objCommand.ExecuteReader(CommandBehavior.CloseConnection);
                }
                else
                {
                    reader = objCommand.ExecuteReader();

                }
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
            finally
            {
                objCommand.Parameters.Clear();
            }

            return reader;
        }
        #endregion

        #region "ExecuteDataSet"
        public DataSet ExecuteDataSet(string spName)
        {
            return ExecuteDataSet(spName, CommandType.StoredProcedure, ConnectionState.CloseOnExit);
        }

        public DataSet ExecuteDataSet(string spName, params object[] parameterValues)
        {
            return ExecuteDataSet(spName, CommandType.StoredProcedure, ConnectionState.CloseOnExit, parameterValues);
        }

        public DataSet ExecuteDataSet(string spName, CommandType commandtype, ConnectionState connectionstate__1, params object[] parameterValues)
        {
            DbDataAdapter adapter = objFactory.CreateDataAdapter();

            DbParameter[] parameters = ParameterCache.GetSpParameterSet(objConnection, spName);
            AssignParameterValues(parameters, parameterValues);
            objCommand.Parameters.AddRange(parameters);
            objCommand.CommandText = spName;
            objCommand.CommandType = commandtype;
            adapter.SelectCommand = objCommand;
            DataSet ds = new DataSet();
            try
            {
                adapter.Fill(ds);
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
            finally
            {
                objCommand.Parameters.Clear();
                if (connectionstate__1 == ConnectionState.CloseOnExit)
                {
                    if (objConnection.State == System.Data.ConnectionState.Open)
                    {
                        objConnection.Close();
                    }
                }
            }
            return ds;
        }

        public DataSet ExecuteDataSet(string query, CommandType commandtype)
        {
            return ExecuteDataSet(query, commandtype, ConnectionState.CloseOnExit);
        }

        public DataSet ExecuteDataSet(string query, CommandType commandtype, ConnectionState connectionstate__1)
        {
            DbDataAdapter adapter = objFactory.CreateDataAdapter();
            objCommand.CommandText = query;
            objCommand.CommandType = commandtype;
            adapter.SelectCommand = objCommand;
            DataSet ds = new DataSet();
            try
            {
                adapter.Fill(ds);
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
            finally
            {
                objCommand.Parameters.Clear();
                if (connectionstate__1 == ConnectionState.CloseOnExit)
                {
                    if (objConnection.State == System.Data.ConnectionState.Open)
                    {
                        objConnection.Close();
                    }
                }
            }
            return ds;
        }
        #endregion

        private void HandleExceptions(Exception ex)
        {
            if (LogErrors)
            {
                WriteToLog(ex.Message);
            }
            if (HandleErrors)
            {
                strLastError = ex.Message;
            }
            else
            {
                throw ex;
            }
        }

        private void WriteToLog(string msg)
        {
            StreamWriter writer = File.AppendText(LogFile);
            writer.WriteLine(DateTime.Now.ToString() + " - " + msg);
            writer.Close();
        }

        public void Dispose()
        {
            objConnection.Close();
            objConnection.Dispose();
            objCommand.Dispose();
        }

        private static void AssignParameterValues(DbParameter[] commandParameters, object[] parameterValues)
        {
            if ((commandParameters == null) || (parameterValues == null))
            {
                // Do nothing if we get no data
                return;
            }

            // We must have the same number of values as we pave parameters to put them in
            if (commandParameters.Length != parameterValues.Length)
            {
                throw new ArgumentException("Parameter count does not match Parameter Value count.");
            }

            // Iterate through the SqlParameters, assigning the values from the corresponding position in the 
            // value array
            int i = 0;
            int j = commandParameters.Length;
            while (i < j)
            {
                // If the current array value derives from IDbDataParameter, then assign its Value property
                if (parameterValues[i] is IDbDataParameter)
                {
                    IDbDataParameter paramInstance = (IDbDataParameter)parameterValues[i];
                    if (paramInstance.Value == null)
                    {
                        commandParameters[i].Value = DBNull.Value;
                    }
                    else
                    {
                        commandParameters[i].Value = paramInstance.Value;
                    }
                }
                else if (parameterValues[i] == null)
                {
                    commandParameters[i].Value = DBNull.Value;
                }
                else
                {
                    commandParameters[i].Value = parameterValues[i];
                }
                i += 1;
            }
        }
    }

    public enum Providers
    {
        SqlServer,
        OleDb,
        Oracle,
        ODBC,
        ConfigDefined
    }

    public enum ConnectionState
    {
        KeepOpen,
        CloseOnExit
    }

    public sealed class SqlValueConverter
    {
        private SqlValueConverter()
        {
        }

        #region "Get based on ordinals"

        public static String GetString(IDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
            {
                return String.Empty;
            }
            return Convert.ToString(reader.GetValue(ordinal));
        }

        public static Guid GetGuid(DbDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
            {
                return Guid.Empty;
            }
            return reader.GetGuid(ordinal);
        }

        public static int GetInt32(IDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
            {
                return 0;
            }
            int value = 0;
            int.TryParse(Convert.ToString(reader.GetValue(ordinal)), out value);
            return value;
        }

        public static long GetInt64(IDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
            {
                return 0;
            }
            return Convert.ToInt64(reader.GetValue(ordinal));
        }

        public static Double GetDouble(IDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
            {
                return 0;
            }
            return Math.Round(Convert.ToDouble(reader.GetValue(ordinal)), 2);
        }

        public static System.Nullable<DateTime> GetDateTime(IDataReader reader, int ordinal, bool nullable)
        {
            if (reader.IsDBNull(ordinal))
            {
                if (!nullable)
                {
                    return DateTime.MinValue;
                }
                else
                {
                    return null;
                }
            }

            return Convert.ToDateTime(reader.GetValue(ordinal));
        }

        public static DateTime GetDateTime(IDataReader reader, int ordinal)
        {
            return GetDateTime(reader, ordinal, false).Value;
        }

        public static bool GetBoolean(IDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
            {
                return false;
            }
            return Convert.ToBoolean(reader.GetValue(ordinal));
        }

        public static System.Nullable<bool> GetBooleanNullable(IDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
            {
                return null;
            }
            return Convert.ToBoolean(reader.GetValue(ordinal));
        }

        public static Decimal GetDecimal(IDataReader reader, int ordinal)
        {
            if (reader.IsDBNull(ordinal))
            {
                return 0;
            }
            return Convert.ToDecimal(reader.GetValue(ordinal));
        }

        #endregion

        #region "Get based on name"""

        public static String GetString(IDataReader reader, string name)
        {
            return GetString(reader, reader.GetOrdinal(name));
        }

        public static int GetInt32(IDataReader reader, string name)
        {
            return GetInt32(reader, reader.GetOrdinal(name));
        }

        public static long GetInt64(IDataReader reader, string name)
        {
            return GetInt64(reader, reader.GetOrdinal(name));
        }

        public static Double GetDouble(IDataReader reader, string name)
        {
            return GetDouble(reader, reader.GetOrdinal(name));
        }

        public static DateTime GetDateTime(IDataReader reader, string name)
        {
            return GetDateTime(reader, reader.GetOrdinal(name));
        }

        public static bool GetBoolean(IDataReader reader, string name)
        {
            return GetBoolean(reader, reader.GetOrdinal(name));
        }

        #endregion
    }

    internal sealed class ParameterCache
    {
        private ParameterCache()
        {
        }
        #region "private methods, variables, and constructors"


        private static Hashtable paramCache = Hashtable.Synchronized(new Hashtable());
        private static DbParameter[] DiscoverSpParameterSet(DbConnection connection, string spName, bool includeReturnValueParameter)
        {
            if (connection == null)
            {
                throw new ArgumentNullException("connection");
            }
            if (spName == null || spName.Length == 0)
            {
                throw new ArgumentNullException("spName");
            }

            DbCommand cmd = connection.CreateCommand();
            cmd.CommandText = spName;
            cmd.CommandType = CommandType.StoredProcedure;

            connection.Open();
            if (connection is SqlConnection)
            {
                SqlCommandBuilder.DeriveParameters((SqlCommand)cmd);
            }
            else if (connection is OdbcConnection)
            {
                OdbcCommandBuilder.DeriveParameters((OdbcCommand)cmd);
            }
            else if (connection is OleDbConnection)
            {
                OleDbCommandBuilder.DeriveParameters((OleDbCommand)cmd);
            }

            connection.Close();

            if (!includeReturnValueParameter)
            {
                cmd.Parameters.RemoveAt(0);
            }

            DbParameter[] discoveredParameters = new DbParameter[cmd.Parameters.Count];

            cmd.Parameters.CopyTo(discoveredParameters, 0);

            // Init the parameters with a DBNull value
            foreach (DbParameter discoveredParameter in discoveredParameters)
            {
                discoveredParameter.Value = DBNull.Value;
            }
            return discoveredParameters;
        }

        private static DbParameter[] CloneParameters(DbParameter[] originalParameters)
        {
            DbParameter[] clonedParameters = new DbParameter[originalParameters.Length];

            int i = 0;
            int j = originalParameters.Length;
            while (i < j)
            {
                clonedParameters[i] = (DbParameter)((ICloneable)originalParameters[i]).Clone();
                i += 1;
            }

            return clonedParameters;
        }

        #endregion

        #region "caching functions"

        public static void CacheParameterSet(string connectionString, string commandText, params DbParameter[] commandParameters)
        {
            if (connectionString == null || connectionString.Length == 0)
            {
                throw new ArgumentNullException("connectionString");
            }
            if (commandText == null || commandText.Length == 0)
            {
                throw new ArgumentNullException("commandText");
            }

            string hashKey = connectionString + ":" + commandText;

            paramCache[hashKey] = commandParameters;
        }

        public static DbParameter[] GetCachedParameterSet(string connectionString, string commandText)
        {
            if (connectionString == null || connectionString.Length == 0)
            {
                throw new ArgumentNullException("connectionString");
            }
            if (commandText == null || commandText.Length == 0)
            {
                throw new ArgumentNullException("commandText");
            }

            string hashKey = connectionString + ":" + commandText;

            SqlParameter[] cachedParameters = paramCache[hashKey] as SqlParameter[];
            if (cachedParameters == null)
            {
                return null;
            }
            else
            {
                return CloneParameters(cachedParameters);
            }
        }

        #endregion

        #region "Parameter Discovery Functions"

        public static DbParameter[] GetSpParameterSet(DbProviderFactory factory, string connectionString, string spName)
        {
            return GetSpParameterSet(factory, connectionString, spName, false);
        }

        public static DbParameter[] GetSpParameterSet(DbProviderFactory factory, string connectionString, string spName, bool includeReturnValueParameter)
        {
            if (connectionString == null || connectionString.Length == 0)
            {
                throw new ArgumentNullException("connectionString");
            }
            if (spName == null || spName.Length == 0)
            {
                throw new ArgumentNullException("spName");
            }

            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                return GetSpParameterSetInternal(connection, spName, includeReturnValueParameter);
            }
        }

        static internal DbParameter[] GetSpParameterSet(DbConnection connection, string spName)
        {
            return GetSpParameterSet(connection, spName, false);
        }

        static internal DbParameter[] GetSpParameterSet(DbConnection connection, string spName, bool includeReturnValueParameter)
        {
            if (connection == null)
            {
                throw new ArgumentNullException("connection");
            }
            using (DbConnection clonedConnection = (DbConnection)((ICloneable)connection).Clone())
            {
                return GetSpParameterSetInternal(clonedConnection, spName, includeReturnValueParameter);
            }
        }

        private static DbParameter[] GetSpParameterSetInternal(DbConnection connection, string spName, bool includeReturnValueParameter)
        {
            if (connection == null)
            {
                throw new ArgumentNullException("connection");
            }
            if (spName == null || spName.Length == 0)
            {
                throw new ArgumentNullException("spName");
            }

            string hashKey = Convert.ToString(connection.ConnectionString) + ":" + spName + (includeReturnValueParameter ? ":include ReturnValue Parameter" : "");

            DbParameter[] cachedParameters = null;

            cachedParameters = paramCache[hashKey] as DbParameter[];
            if (cachedParameters == null)
            {
                DbParameter[] spParameters = DiscoverSpParameterSet(connection, spName, includeReturnValueParameter);
                paramCache[hashKey] = spParameters;
                cachedParameters = spParameters;
            }

            return CloneParameters(cachedParameters);
        }

        #endregion
    }
}
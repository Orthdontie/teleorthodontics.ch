﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Teleorthodontics.Web.Helper;

namespace Teleorthodontics.Web.DB
{
    public class StripeApi
    {
        public static void SyncPlan()
        {
            try
            {
                DatabaseHelper databaseHelper = new DatabaseHelper();
                var plans = StripeApiClient.ListAllPlans().ToList();
                foreach (var plan in plans)
                {
                    String query = @"select TOP 1 StripePlanId FROM Plans where StripePlanId='" + plan.Id + "'";
                    var StripePlanId = databaseHelper.ExecuteScalar(query, CommandType.Text);
                    if (StripePlanId == null)
                    {
                        databaseHelper.ExecuteNonQuery("INSERT INTO Plans(StripePlanId,Name,Amount,Discount,Currency,Interval,TrialPeriodDays,IsActive,DateCreated) VALUES('" + plan.Id + "','" + plan.Name + "'," + plan.Amount + ",0,'" + plan.Currency + "','" + plan.Interval + "'," + plan.TrialPeriodDays + ",0,GetDate())", CommandType.Text);
                    }
                    else
                    {
                        databaseHelper.ExecuteNonQuery("Update Plans set Name='" + plan.Name + "',Amount=" + plan.Amount + ",Currency='" + plan.Currency + "',Interval='" + plan.Interval + "',TrialPeriodDays=" + plan.TrialPeriodDays + " where StripePlanId='" + plan.Id + "'", CommandType.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable GetPlan(string stripePlanId)
        {
            DatabaseHelper databaseHelper = new DatabaseHelper();
            String query = @"select * from Plans where Id='" + stripePlanId + "'";
            DataSet ds = new DataSet();
            ds = databaseHelper.ExecuteDataSet(query, CommandType.Text);
            return ds.Tables[0];
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for LedgerPosting
/// </summary>
/// 
public class LedgerPosting
{

    #region Ledger Variables

    public string Pat_Id { get; set; }
    public string Comp_Id { get; set; }
    public string Lab_Fee_Total { get; set; }
    public string Patient_Lab_Fee { get; set; }
    public string Patient_Lab_Fee_Start_Date { get; set; }
    public string Patient_Lab_Fee_No_Months { get; set; }
    public string Ins1_Lab_Fee { get; set; }
    public string Ins1_Lab_Fee_Start_Date { get; set; }
    public string Ins2_Lab_Fee { get; set; }
    public string Ins2_Lab_Fee_Start_Date { get; set; }
    public string Treatment_Fee_Total { get; set; }
    public string Patient_Treat_Fee { get; set; }
    public string Patient_Treat_Fee_Start_Date { get; set; }
    public string Ins1_Treat_Fee { get; set; }
    public string Ins2_Treat_Fee { get; set; }
    public string Monthly_Payement_Patient { get; set; }
    public string Monthly_Payement_Patient_No_Month { get; set; }
    public string Insurance1_Quarterly_Payment { get; set; }
    public string Insurance1_Quarterly_Payment_Start_Date { get; set; }
    public string Insurance2_Quarterly_Payment { get; set; }
    public string Insurance2_Quarterly_Payment_Start_Date { get; set; }
    public string Phase { get; set; }
    public string Action { get; set; }
    public string Insurance1_Quarterly_Payment_Months { get; set; }
    public string Insurance2_Quarterly_Payment_Months { get; set; }
    public string T_Type { get; set; }
    public string AccountingId { get; set; }
    public string PatDiscount { get; set; }

    public string PatAmount { get; set; }
    public string Ins1Amount { get; set; }
    public string Ins2Amount { get; set; }
    public string remarks { get; set; }
    public string C_Date { get; set; }
    public string FeeType { get; set; }
    public string Contract_Date { get; set; }
    public string Trans_for { get; set; }
    public string DateTo { get; set; }
    public string DateFrom { get; set; }
    public string SearchMode { get; set; }
    public string DoctorId { get; set; }
    public string PayType { get; set; }
    public string ServiceId { get; set; }
    public string ServiceName { get; set; }
    public string DoctorName { get; set; }
    public string OfficeId { get; set; }
    public string PayType_insurance1 { get; set; }
    public string PayType_insurance2 { get; set; }

    #endregion

    # region Sp calling variables

    public string[] InputName = new string[33];
    public string[] InputValue = new string[33];
    public string[] InputType = new string[33];

    #endregion

    public void ClearObj()
    {

    }

    public void ClearArray()
    {
        Array.Clear(InputName, 0, InputName.Length - 1);
        Array.Clear(InputType, 0, InputType.Length - 1);
        Array.Clear(InputValue, 0, InputValue.Length - 1);
    }
    public DataTable Insert_Ledger_Entry()
    {
        ClearArray();

        InputName[0] = "Action";
        InputType[0] = "V";
        InputValue[0] = "Insert_Ledger_Entry";

        InputName[1] = "patAmt";
        InputType[1] = "V";
        InputValue[1] = PatAmount;


        InputName[2] = "remarks";
        InputType[2] = "V";
        InputValue[2] = remarks;

        InputName[3] = "C_Date";
        InputType[3] = "V";
        InputValue[3] = C_Date;

        InputName[4] = "Pat_Id";
        InputType[4] = "V";
        InputValue[4] = Pat_Id;

        InputName[5] = "FeeType";
        InputType[5] = "V";
        InputValue[5] = FeeType;

        InputName[6] = "PayType";
        InputType[6] = "V";
        InputValue[6] = PayType;

        InputName[7] = "DoctorId";
        InputType[7] = "V";
        InputValue[7] = DoctorId;

        InputName[8] = "ServiceId";
        InputType[8] = "V";
        InputValue[8] = ServiceId;

        InputName[9] = "OfficeId";
        InputType[9] = "V";
        InputValue[9] = OfficeId;

        InputName[10] = "ins1Amt";
        InputType[10] = "V";
        InputValue[10] = Ins1Amount;

        InputName[11] = "ins2Amt";
        InputType[11] = "V";
        InputValue[11] = Ins2Amount;

        InputName[12] = "PayType";
        InputType[12] = "V";
        InputValue[12] = PayType;

        DataTable dt = new DataTable();

        //search parameter
        dt = DAL.SelectProcedureArr("USP_Contract", InputName, "RowCount", InputType, "I", InputValue);

        return dt;
    }

}
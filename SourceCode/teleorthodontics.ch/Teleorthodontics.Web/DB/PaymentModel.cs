﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Teleorthodontics.Web.DB
{
    public class PaymentModel
    {
        static DatabaseHelper databaseHelper = new DatabaseHelper();
        public static void AddPaymentHistory(string userId, long creditCardInfoId, string _stripecustomerId, string _stripesubscriptionId, string stripeCardId, decimal amount, string _transactionnumber, string _invoice, bool status)
        {
            databaseHelper.ExecuteNonQuery("INSERT INTO [PaymentHistory]([UserId] ,[CreditCardInfoId],[StripeCustomerID],[StripeSubscriptionID],[StripeCardId],[Amount]  ,[TransactionCode],[IsSuccess],[DateCreated],[StripeInvoiceId]) VALUES ('" + userId + "' ," + creditCardInfoId + ",'" + _stripecustomerId + "','" + _stripesubscriptionId + "','" + stripeCardId + "'," + (amount/100) + " ,'" + _transactionnumber + "'," + (status ? 1 : 0) + ",GetDate(),'" + _invoice + "')", commandtype: System.Data.CommandType.Text);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Teleorthodontics.Web.Helper;

namespace Teleorthodontics.Web.DB
{
    public class Registration
    {
        DatabaseHelper databaseHelper = new DatabaseHelper();
        public string AddRegistration(string User_Fname, string User_Lname, string Email_Id, string Ipaddress, string phone, string DOB, string gender, string age, string User_Id, string Password, string officeCode, string address, string country, string state, string city, string zip)
        {

            string joindate1 = DateTime.Now.Month.ToString();
            string joinDate2 = DateTime.Now.Day.ToString();
            string joindate3 = DateTime.Now.Year.ToString();
            string joinDate = joindate1 + "/" + joinDate2 + "/" + joindate3;

            //string User_Id = CommonClass.generatepatientcode();
            //string Password = CommonClass.GenerateRandomCode();
            string Roll_Id = "3";

            // user Info //
            databaseHelper.ExecuteNonQuery("insert_UserTable", User_Id, User_Fname, User_Lname, Email_Id, Password, "Active", Roll_Id, Ipaddress, "", "", "", "", "", "", joinDate);
            databaseHelper.ExecuteNonQuery("insert_UserInfo", User_Id, Email_Id, address, country, state, city, zip, phone, phone, DOB, null, gender, null, null, null, null, age, null, null, null, 3); // 3 for service Id
            // Treatment Table && Patient Doctor Table ///
            databaseHelper.ExecuteNonQuery("insert into Patient_Doctor(Patn_Id,Doc_Id,Office_Id) values ('" + User_Id + "','" + "DOC20110" + "','" + officeCode + "')", CommandType.Text);
            ////  Next Procedure for New Patient ////

            databaseHelper.ExecuteNonQuery("insert into Treatement_Chart(User_Id,ProcedureId,Alloted_Days) values ('" + User_Id + "','" + 6 + "','" + "0" + "')", CommandType.Text);

            // This method inserts Dental History Records into User_dentalHistory

            databaseHelper.ExecuteNonQuery("INSERT INTO Dentist_Name(Name,Email,Phone,Address,City,Zip,Country,State,UserId) VALUES('','','','','','','','','" + User_Id + "')", CommandType.Text);
            int denId = (int)databaseHelper.ExecuteScalar("select Max(Id) as Id from Dentist_Name", CommandType.Text);
            databaseHelper.ExecuteNonQuery("InsertDentalHistory", User_Id, "", "", "", "", "", "", "", "", "", "", denId.ToString());
            // Facial Pain  //


            databaseHelper.ExecuteNonQuery("INSERT INTO Facial_Pain_Questions(UserID,QlOthers,QlFreely,QlBigbite,QlEar,QlTemples,QlWantTo,QlHeadache,QldVisit,QlbadBite,QlNight,QlTeeth,QlMorning,QlOneSide,QlBlowJaw,QlJoint,QlUlcers,QlColittis,QlNeckPain,QlWhipLash,QlSkinProblem,QlAllergies,QlDoubleJointed) VALUES('" + User_Id + "','','','','','','','','','','','','','','','','','','','','','','')", CommandType.Text);
            // This method inserts Jaw Problems Questionnaire Records into User_dentalHistory //


              databaseHelper.ExecuteNonQuery("INSERT INTO JawProblems(User_Id,JawProblem,Description,Bitesplint,Medication,PhysicaTherapy,OcclusalAdjustment,Orthodontics,Counseling,Surgery,Other) values('"+User_Id+"','No','','','','','','','','','')", CommandType.Text);


            //// Pre-Doctor Info  //
              databaseHelper.ExecuteNonQuery("insert_Patient_Pre_Doc_Info", User_Id, "No doctor", "No Contact","","","","","","","");

            //// Medical Problems//
              string sqlQuery = "";
              for (int i = 1; i <= 37; i++)
              {
                  if (i == 37)
                      sqlQuery += "('" + User_Id + "','" + i.ToString() + "','No')";
                  else
                      sqlQuery += "('" + User_Id + "','" + i.ToString() + "','No'),";

              }
              sqlQuery = "INSERT INTO Patient_MedicalProblems(User_Id,QId,Answer) VALUES " + sqlQuery;
              databaseHelper.ExecuteNonQuery(sqlQuery, CommandType.Text);

            //// Responsible Person //
              databaseHelper.ExecuteNonQuery("INSERT INTO Pat_ResponsiblePerson(User_Id,Name,Address,HomePhone,MobilePhone,Email,City,Zip,IdType,IDNumber,EmployerName,EmpAdd,EmpState,EmpCity,EmpZipCode,EmpTime,RCountry,RState,Relation,workPhone,Profession,EmpCountry) values('" + User_Id + "','','','','','','','','','','','','','','','','','','','','','')", CommandType.Text);
       
            //// Patient table //
              databaseHelper.ExecuteNonQuery("insert_Patient_Table", User_Id, "","","","","","","","","","","","","");

            //// Spouse Information //
              databaseHelper.ExecuteNonQuery("InsertSpousInfo", User_Id,"","","","","","","","","","","","");

            //// Insurance Informations //
              databaseHelper.ExecuteNonQuery("Insertinsurance", User_Id, "", "", "", "", "", "", "", "", "", "", "", "Primary", "", "", "");
              databaseHelper.ExecuteNonQuery("Insertinsurance", User_Id, "", "", "", "", "", "", "", "", "", "", "", "Secondary", "", "", "");
           //   Sendmail.SendmailNewUser(User_Id, User_Fname, User_Lname, Email_Id, Password, imageList);
              return User_Id;
        }

        public string AddOnlineConsultation(string fullName, string email, string telePhone, string zipCode, string age,
        string frontImage, string rightImage, string leftImage, string slightlyImage, string topImage,
        string bottomImage, string comments, string ip, string Type, string bigSmileImage, string mouthOpenImage, string mouthClosedImage, string profileImage, string panoramicXRay, string lateralCeph, string User_Id, string website)
        {

            string query = @"INSERT INTO OnlineConsultation([Id],[Name],[Email],[Phone],[ZipCode],[Age],[FrontImage], " +
        "[RightImage] ,[LeftImage] ,[SlightImage],[TopImage],[BottomImage],[UserComments], " +
        "[Location],[Type],[BigSmileImage],[MouthOpenImage],[MouthClosedImage],[ProfileImage],[ServiceId],[PanoramicXRay],[LateralCeph],[UserId],[Website]) VALUES ('" + CommonClass.GenerateUserId() + "','" + fullName + "','" + email + "','" + telePhone + "','" + zipCode + "','" + age + "','" + frontImage + "','" + rightImage + "' ,'" + leftImage + "', " +
        " '" + slightlyImage + "','" + topImage + "','" + bottomImage + "','" + comments + "','" + ip + "','" + Type + "','" + bigSmileImage + "','" + mouthOpenImage + "','" + mouthClosedImage + "','" + profileImage + "',2,'" + panoramicXRay + "','" + lateralCeph + "','" + User_Id + "','"+ website + "')";

            databaseHelper.ExecuteNonQuery(query, CommandType.Text);

            return "";
        }

    }
    public class splitdate
    {
        public splitdate()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public string split_date(string date)
        {
            string str1 = "";
            string str2 = "";
            string str3 = "";
            string Splittype = null;
            string[] datesub = date.Split('/');
            for (int i = 0; i < datesub.Length; i++)
            {
                string str = datesub.GetValue(i).ToString();
                if (i == 0)
                {
                    str1 = str;
                    string length = Convert.ToString(str1.Length);
                    if (length == "1")
                    {
                        str1 = "0" + str1;
                    }
                }
                else if (i == 1)
                {
                    str2 = str;
                    string length1 = Convert.ToString(str2.Length);
                    if (length1 == "1")
                    {
                        str2 = "0" + str2;
                    }
                }
                else if (i == 2)
                {
                    str3 = str;
                }
                Splittype = str1 + "/" + str2 + "/" + str3;
            }
            return Splittype;
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for DAL
/// </summary>
public class DAL
{
    static String connectionString = ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString;
    SqlConnection con = new SqlConnection(connectionString);

    public static DataTable SelectProcedureArr(string ProcedureName, string[] InputName, string OutputName, string[] InputType, string OutputType, string[] InputValue)
    {
        SqlConnection MyConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionstring"].ConnectionString);

        SqlDataAdapter MyDataAdapter = new SqlDataAdapter(ProcedureName, MyConnection);
         DataTable DT = new DataTable();

        int LoopCnt;
        try
        {
            //Set the command type as StoredProcedure.
            MyDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;

            //Create and add a parameter to Parameters collection for the stored procedure.

            for (LoopCnt = 0; LoopCnt < InputName.Length; LoopCnt++)
            {
                // if (!DBNull.Value.Equals(InputValue[LoopCnt]) )--Select--
                if (Convert.ToString(InputValue[LoopCnt]) != null && Convert.ToString(InputValue[LoopCnt]) != "" && !Convert.ToString(InputValue[LoopCnt]).Equals("--Select--"))
                {

                    if (InputType[LoopCnt] == "C")
                    {
                        MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@" + InputName[LoopCnt], SqlDbType.Char, 10));
                        //Assign the search value to the parameter.
                        MyDataAdapter.SelectCommand.Parameters["@" + InputName[LoopCnt]].Value = Convert.ToString(InputValue[LoopCnt]);
                    }
                    if (InputType[LoopCnt] == "I")
                    {
                        MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@" + InputName[LoopCnt], SqlDbType.Int, 4));
                        //Assign the search value to the parameter.
                        MyDataAdapter.SelectCommand.Parameters["@" + InputName[LoopCnt]].Value = Convert.ToInt32(InputValue[LoopCnt], 10);
                    }
                    else if (InputType[LoopCnt] == "V")
                    {
                        MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@" + InputName[LoopCnt], SqlDbType.VarChar, 2000000));
                        //Assign the search value to the parameter.
                        MyDataAdapter.SelectCommand.Parameters["@" + InputName[LoopCnt]].Value = InputValue[LoopCnt];
                    }
                    else if (InputType[LoopCnt] == "T")
                    {
                        MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@" + InputName[LoopCnt], SqlDbType.Text, 2000000));
                        MyDataAdapter.SelectCommand.Parameters["@" + InputName[LoopCnt]].Value = InputValue[LoopCnt];
                    }
                       

                    else if (InputType[LoopCnt] == "D")
                    {
                        MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@" + InputName[LoopCnt], SqlDbType.DateTime, 8));
                        MyDataAdapter.SelectCommand.Parameters["@" + InputName[LoopCnt]].Value = Convert.ToDateTime(InputValue[LoopCnt]);
                    }
                    else if (InputType[LoopCnt] == "DE")
                    {
                        MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@" + InputName[LoopCnt], SqlDbType.Decimal, 14));
                        MyDataAdapter.SelectCommand.Parameters["@" + InputName[LoopCnt]].Value = InputValue[LoopCnt];
                    }

                    else if (InputType[LoopCnt] == "B")
                    {
                        MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@" + InputName[LoopCnt], SqlDbType.Bit, 8));
                        MyDataAdapter.SelectCommand.Parameters["@" + InputName[LoopCnt]].Value = Convert.ToBoolean(InputValue[LoopCnt]);
                    }
                }

                //MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@musicname", SqlDbType.Decimal, 23));
            }
            //Create and add an output parameter to the Parameters collection. 
            if (OutputType == "I")
            {
                MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@" + OutputName, SqlDbType.Int, 4));
            }
            else if (OutputType == "V")
            {
                MyDataAdapter.SelectCommand.Parameters.Add(new SqlParameter("@" + OutputName, SqlDbType.VarChar, 50));
            }
            //Set the direction for the parameter. This parameter returns the Rows that are returned.
            MyDataAdapter.SelectCommand.Parameters["@" + OutputName].Direction = ParameterDirection.Output;
            MyDataAdapter.SelectCommand.CommandTimeout = 720;
            MyDataAdapter.Fill(DT);

            MyDataAdapter.Dispose();
            MyConnection.Close();


        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            MyDataAdapter.Dispose();
            MyConnection.Close();
        }

        MyConnection.Close();
        return DT;
    }


}
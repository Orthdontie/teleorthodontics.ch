﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace Teleorthodontics.Web.DB
{
    public class CommonClass
    {
       
        public static string GenerateRandomCode()
        {
            string R = "";

            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());


            R = Convert.ToString(result);

            //string s = "";
            //Random random = new Random();
            //const int length = 8;
            //for (int i = 0; i < length; i++)
            //{
            //    if (random.Next(0, 3) == 0) //if random.Next() == 0 then we generate a random character. 
            //    {
            //        s += ((char)random.Next(65, 91)).ToString();
            //    }
            //    else //if random.Next() == 0 then we generate a random digit
            //    {
            //        s += random.Next(0, 9);
            //    }
            //}

            return R;
        }

        public static string GenerateUserId()
        {
            DatabaseHelper databaseHelper = new DatabaseHelper();
            String query = @"select TOP 1 [Id] FROM OnlineConsultation order by ID desc";
           
            string userID =((int) databaseHelper.ExecuteScalar(query, CommandType.Text)).ToString();
            StringBuilder sbNewId = new StringBuilder();
            string tmpId = null;
            int i = 0;
            if (!string.IsNullOrEmpty(userID))
            {
                tmpId = userID.TrimStart('0');
                i = Convert.ToInt32(tmpId) + 1;
                tmpId = i.ToString();
            }
            else
                tmpId = "00000001";

            for (int j = tmpId.Length; j <= 8; j++)
            {
                sbNewId.Append(0);
            }

            sbNewId.Append(tmpId);

            return sbNewId.ToString();

        }

        public static string generatepatientcode()
        {
            DatabaseHelper databaseHelper = new DatabaseHelper();
            String query = @"select User_id from User_Table where Roll_Id='3'";
            DataSet ds = new DataSet();
            ds = databaseHelper.ExecuteDataSet(query, CommandType.Text);
            string registrationcode = null;
            DataTable dt = ds.Tables[0];
            int count = dt.Rows.Count + 1;
            DateTime dtyear = DateTime.Now;
            string year = (dtyear.Year.ToString());
            string hlfyear = year.Substring(2, 2);
            string date = System.DateTime.Today.ToShortDateString();
            splitdate spday = new splitdate();
            string regdate = spday.split_date(date);
            string rdate = regdate.Substring(0, 2);

            if (count < 10)
            {
                registrationcode = hlfyear + rdate + "000" + count;
            }
            else if (count > 10 && count < 100)
            {
                registrationcode = hlfyear + rdate + "00" + count;
            }
            else if (count > 100 && count < 1000)
            {
                registrationcode = hlfyear + rdate + "0" + count;
            }
            else
            {
                registrationcode = hlfyear + rdate + count;
            }

            return registrationcode;

        }
    }
}
﻿using Newtonsoft.Json;
using Stripe;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using Teleorthodontics.Web.DB;
using Teleorthodontics.Web.Helper;

namespace Teleorthodontics.Web
{
    /// <summary>
    /// Summary description for StripeHelper
    /// </summary>
    public class StripeHelper : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            StreamReader reader = new StreamReader(context.Request.InputStream);
            context.Request.InputStream.Position = 0;
            String Body = reader.ReadToEnd().ToString();
            string userId = string.Empty;

            try
            {
                if (Body != null && Body != "")
                {
                    // var json = Newtonsoft.Json.JsonConvert.SerializeObject(requestModel);

                    var requestData = StripeEventUtility.ParseEvent(Body);

                    SubscriptionModel jsonresult = JsonConvert.DeserializeObject<SubscriptionModel>(Body);
                    var customerid = jsonresult.data.@object.customer;


                    DatabaseHelper databaseHelper = new DatabaseHelper();
                    String query = @"select * from CreditCardInfo where StripeCustomerID='" + customerid + "'";
                    DataSet ds = new DataSet();
                    ds = databaseHelper.ExecuteDataSet(query, CommandType.Text);
                    if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        Boolean status = false;
                        string fileurl = context.Server.MapPath("Logo") + "/abc.txt";
                        File.WriteAllText(fileurl, Body);
                        switch (requestData.Type)
                        {
                               
                            case StripeEvents.InvoicePaymentFailed:
                                {
                                    var stripeCharge = Stripe.Mapper<StripeCharge>.MapFromJson(requestData.Data.Object.ToString());
                                    // PlanHelper.AddPaymentHistory(jsonresult, creditCardInfo, false);
                                }
                                break;
                            case StripeEvents.InvoicePaymentSucceeded:
                                {
                                    var stripeCharge12 = Stripe.Mapper<StripeCharge>.MapFromJson(requestData.Data.Object.ToString());
                                    //Need to post to leader once the correct data coming
                                    LedgerPosting objlp = new LedgerPosting();
                                    objlp.PatAmount = dt.Rows[0]["TotalCost"].ToString();
                                    objlp.remarks = "Stripe Subscription";
                                    objlp.C_Date = DateTime.Now.ToString("dd-MM-yyyy");
                                    objlp.Pat_Id = dt.Rows[0]["UserId"].ToString(); 
                                    objlp.FeeType = "Tre Fee";
                                    objlp.PayType = "";
                                    objlp.DoctorId="1";
                                    objlp.ServiceId="2";
                                    objlp.OfficeId = "2";
                                    objlp.Insert_Ledger_Entry();
                                    status = true;
                                }
                                break;
                            case StripeEvents.ChargeSucceeded:
                                { 
                                    var stripeCharge12 = Stripe.Mapper<StripeCharge>.MapFromJson(requestData.Data.Object.ToString());
                                    //PlanHelper.AddPaymentHistory(jsonresult, creditCardInfo, true);
                                    status = true;
                                }
                                break;
                            case StripeEvents.ChargeFailed:
                                {
                                    var stripeCharge12 = Stripe.Mapper<StripeCharge>.MapFromJson(requestData.Data.Object.ToString());
                                    // PlanHelper.AddPaymentHistory(jsonresult, creditCardInfo, false);
                                }
                                break;
                        }

                        int IsSuccess = status ? 1 : 0;
                        userId = dt.Rows[0]["UserId"].ToString();
                        PaymentModel.AddPaymentHistory(dt.Rows[0]["UserId"].ToString(), Convert.ToInt64(dt.Rows[0]["Id"].ToString()), jsonresult.data.@object.customer, jsonresult.data.@object.subscription, jsonresult.data.@object.source.id, jsonresult.data.@object.amount, jsonresult.data.@object.balance_transaction, jsonresult.data.@object.invoice, status);

                    }
                }

            }
            catch (Exception ex)
            {
                File.WriteAllText(context.Server.MapPath("/img/") + userId + "log2.txt", ex.Message.ToString() + " StackTrace=" + ex.StackTrace + "JSON=" + Body);

                // throw;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
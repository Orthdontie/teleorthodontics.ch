﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Threading;
using Teleorthodontics.Web.Helper;
using System.Configuration;

namespace Teleorthodontics.Web
{
    public partial class whats_included : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string language;
	    public string cMyValue;
	    string selectedLanguage;

        protected override void FrameworkInitialize()
        {
            language = Request.QueryString["lang"];


            if (Common.CheckLanguage(language))
            {
                if (Session["languagechng"] != null)
                {
                    language = (string)Session["languagechng"];
                }
                else
                {
                    Session["languagechng"] = language;
                }

                if (language == null)
                {
                    language = ConfigurationManager.AppSettings["DefaultLanguage"].ToString();
                    Session["languagechng"] = language;
                }
            }
            else
            {
                language = ConfigurationManager.AppSettings["DefaultLanguage"].ToString();
                Session["languagechng"] = language;
            }

            selectedLanguage = Convert.ToString(language);

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);

            base.FrameworkInitialize();
        }
    }
}
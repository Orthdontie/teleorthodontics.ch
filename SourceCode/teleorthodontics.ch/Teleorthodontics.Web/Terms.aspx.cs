﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Threading;
using Teleorthodontics.Web.Helper;
using System.Configuration;

namespace Teleorthodontics.Web
{
    public partial class terms : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string language;
	    public string cMyValue;
	    string selectedLanguage;

        protected override void FrameworkInitialize()
        {
            language = Request.QueryString["lang"];


            selectedLanguage = Convert.ToString(SessionManager.GetLanguage(language));

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);

            base.FrameworkInitialize();
        }
    }
}
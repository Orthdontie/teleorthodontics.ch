﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using Teleorthodontics.Web.DB;
using Teleorthodontics.Web.Helper;
using System.Globalization;
using System.Threading;
using System.Configuration;

namespace Teleorthodontics.Web
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.Session["CaptchaImageText"] = CImage.GenerateRandomCode();
                hdnCaptchaCode.Value = this.Session["CaptchaImageText"].ToString();
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lblmsg.Style.Add("color", "red");
            dvMsg.Visible = true;
            if (this.txtCaptcha.Text != "")
            {
                string ipAddress = Request.UserHostAddress;

                try
                {
                    if (ddlSubject.SelectedItem.Text != "Choose Subject" || ddlSubject.SelectedItem.Value != "0")
                    {
                        DatabaseHelper databaseHelper = new DatabaseHelper();
                        databaseHelper.ExecuteNonQuery("insert_ContactUs_Query", txtFirstName.Text, txtLastName.Text, txtEmail.Text, Convert.ToInt32(ddlSubject.SelectedValue), txtMessage.Text, ipAddress, txtPhone.Text);


                        string query = "select User_Id from User_table where Email_Id='" + txtEmail.Text + "'";
                        var User_Id = databaseHelper.ExecuteScalar(query, CommandType.Text);
                        if (User_Id == null)
                        {
                            User_Id = "";
                        }
                        string name = txtFirstName.Text.ToString() + " " + txtLastName.Text.ToString();

                        Sendmail.SendContactUsMail(User_Id.ToString(), name, txtEmail.Text, txtPhone.Text.ToString(), ipAddress, txtMessage.Text, ddlSubject.SelectedItem.Text);

                        lblmsg.InnerText = GetLocalResourceObject("contact21").ToString();
                        //lblmsg.InnerText = "Thanks for writing us, We will contact you shortly";
                        lblmsg.Style.Add("color", "green");
                        ClearForm();

                    }
                    else
                    {
                        lblmsg.InnerText = GetLocalResourceObject("contact22").ToString();
                        //lblmsg.InnerText = "Please select the subject name";
                    }
                }
                catch (Exception ex)
                {
                    lblmsg.InnerText = ex.Message.ToString();
                }

                // lblmsgcap.Text = "";
                // GenerateNumber1();
            }
            else
            {
                lblmsg.InnerText = GetLocalResourceObject("contact23").ToString();
                //lblmsg.InnerText = "Image code is not valid.";
                // Label1.Text = Convert.ToString(CusText[34]);
            }
        }

        private void ClearForm()
        {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtEmail.Text = "";
            txtPhone.Text = "";
            txtMessage.Text = "";
            txtCaptcha.Text = "";
            ddlSubject.SelectedValue = "0";
        }

        public string language;
        public string cMyValue;
        string selectedLanguage;

        protected override void FrameworkInitialize()
        {
            language = Request.QueryString["lang"];


            selectedLanguage = Convert.ToString(SessionManager.GetLanguage(language));

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(selectedLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(selectedLanguage);

            base.FrameworkInitialize();
        }
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/General.Master" AutoEventWireup="true" CodeFile="whats-included.aspx.cs" Inherits="Teleorthodontics.Web.whats_included" Culture="Auto" UICulture="Auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="bodyattributes" runat="server">
    id="whats-included" class="homepage"
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="title" runat="server">
    <title><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:title%>" /></title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <meta name="description" content="<asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:metadescription%>" />" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderBody" runat="server">
    <section id="features" class="features">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="section-heading">
                        <h2>
                            <asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:whats_included2%>" /></h2>
                        <p class="text-muted">
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:whats_included3%>" />
                        </p>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div id="service-list" class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="feature-item">
                                    <i  id="take-impression" class="icon-screen-smartphone text-primary"></i>
                                    <h3>
                                        <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:whats_included4%>" /><br />
                                        <asp:Literal ID="Literal5b" runat="server" Text="<%$ Resources:whats_included4b%>" />
                                        <span>
                                            <asp:Literal ID="Literal5a" runat="server" Text="<%$ Resources:whats_included4a%>" />
                                        </span>
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:whats_included5%>" />
                                    </p>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:whats_included6%>" />
                                    </p>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:whats_included7%>" />
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="feature-item">
                                    <i class="icon-present text-primary"></i>
                                    <i class="icon-screen-smartphone text-primary"></i>
                                    <h3>
                                        <asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:whats_included8%>" />
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:whats_included9%>" />
                                    </p>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:whats_included10%>" />
                                    </p>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:whats_included11%>" />
                                    </p>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:whats_included12%>" />
                                    </p>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:whats_included13%>" />
                                    </p>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:whats_included14%>" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
        <section id="treatment-options" class="contact bg-primary">
        <div class="container">
            <h2>
                <asp:Literal ID="Literal16" runat="server" Text="<%$ Resources:whats_included15%>" />
            </h2>
            <div class="row">
                <div class="col-lg-12">
                    <div id="service-list" class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="feature-item">
                                    <i class="icon-check text-primary"></i>
                                    <h3 id="mb0">
                                        <asp:Literal ID="Literal17" runat="server" Text="<%$ Resources:whats_included16%>" />
                                        <span>
                                            <asp:Literal ID="Literal18" runat="server" Text="<%$ Resources:whats_included17%>" />
                                        </span>
                                        <a id="learnmoreoninternet10" href="<asp:Literal ID="Literal19" runat="server" Text="<%$ Resources:whats_included18%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                            <asp:Literal ID="Literal20" runat="server" Text="<%$ Resources:whats_included19%>" />
                                        </a>
                                    </h3>
                                    <p>
                                        <asp:Literal ID="Literal21" runat="server" Text="<%$ Resources:whats_included20%>" />
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="feature-item">
                                    <i class="icon-check text-primary"></i>
                                    <h3 id="mb">
                                        <asp:Literal ID="Literal22" runat="server" Text="<%$ Resources:whats_included21%>" />
                                        <span>
                                            <asp:Literal ID="Literal23" runat="server" Text="<%$ Resources:whats_included22%>" />
                                        </span>
                                        <a id="learnmoreoninternet10" href="<asp:Literal ID="Literal24" runat="server" Text="<%$ Resources:whats_included23%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                            <asp:Literal ID="Literal25" runat="server" Text="<%$ Resources:whats_included24%>" />
                                        </a>
                                    </h3>
                                    <p>
                                        <asp:Literal ID="Literal26" runat="server" Text="<%$ Resources:whats_included25%>" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="feature-item">
                                    <i class="icon-check text-primary"></i>
                                    <h3>
                                        <asp:Literal ID="Literal27" runat="server" Text="<%$ Resources:whats_included26%>" />
                                        <span>
                                            <asp:Literal ID="Literal28" runat="server" Text="<%$ Resources:whats_included27%>" />
                                        </span>
                                        <a id="learnmoreoninternet10" href="<asp:Literal ID="Literal29" runat="server" Text="<%$ Resources:whats_included28%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                            <asp:Literal ID="Literal30" runat="server" Text="<%$ Resources:whats_included29%>" />
                                        </a>
                                    </h3>
                                    <p>
                                        <asp:Literal ID="Literal31" runat="server" Text="<%$ Resources:whats_included30%>" />
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="feature-item">
                                    <i class="icon-check text-primary"></i>
                                    <h3>
                                        <asp:Literal ID="Literal32" runat="server" Text="<%$ Resources:whats_included31%>" />
                                        <span>
                                            <asp:Literal ID="Literal33" runat="server" Text="<%$ Resources:whats_included32%>" />
                                        </span>
                                        <a id="learnmoreoninternet10" href="<asp:Literal ID="Literal34" runat="server" Text="<%$ Resources:whats_included33%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                            <asp:Literal ID="Literal35" runat="server" Text="<%$ Resources:whats_included34%>" />
                                        </a>
                                    </h3>
                                    <p>
                                        <asp:Literal ID="Literal36" runat="server" Text="<%$ Resources:whats_included35%>" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="feature-item">
                                    <i class="icon-check text-primary"></i>
                                    <h3>
                                        <asp:Literal ID="Literal37" runat="server" Text="<%$ Resources:whats_included36%>" />
                                        <span>
                                            <asp:Literal ID="Literal38" runat="server" Text="<%$ Resources:whats_included37%>" />
                                        </span>
                                        <a id="learnmoreoninternet10" href="<asp:Literal ID="Literal39" runat="server" Text="<%$ Resources:whats_included38%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                            <asp:Literal ID="Literal40" runat="server" Text="<%$ Resources:whats_included39%>" />
                                        </a>
                                    </h3>
                                    <p>
                                        <asp:Literal ID="Literal41" runat="server" Text="<%$ Resources:whats_included40%>" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="features" class="features">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div id="service-list" class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="feature-item">
                                    <i class="icon-like text-primary"></i>
                                    <h3>
                                        <asp:Literal ID="Literal42" runat="server" Text="<%$ Resources:whats_included41%>" />
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal43" runat="server" Text="<%$ Resources:whats_included42%>" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id="retainers" class="col-md-6">
                                <div class="feature-item">
                                    <i class="icon-like text-primary"></i>
                                    <h3>
                                        <asp:Literal ID="Literal44" runat="server" Text="<%$ Resources:whats_included43%>" />
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal45" runat="server" Text="<%$ Resources:whats_included44%>" />
                                    </p>
                                    <h3>
                                        <asp:Literal ID="Literal46" runat="server" Text="<%$ Resources:whats_included45%>" />
                                        <span>
                                            <asp:Literal ID="Literal47" runat="server" Text="<%$ Resources:whats_included46%>" />
                                        </span>
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal48" runat="server" Text="<%$ Resources:whats_included47%>" />
                                    </p>
                                    <h3>
                                        <asp:Literal ID="Literal49" runat="server" Text="<%$ Resources:whats_included48%>" />
                                        <span>
                                            <asp:Literal ID="Literal50" runat="server" Text="<%$ Resources:whats_included49%>" />
                                        </span>
                                        <a id="learnmoreoninternet15" href="<asp:Literal ID="Literal51" runat="server" Text="<%$ Resources:whats_included50%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                            <asp:Literal ID="Literal52" runat="server" Text="<%$ Resources:whats_included51%>" />
                                        </a>
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal53" runat="server" Text="<%$ Resources:whats_included52%>" />
                                    </p>
                                    <h3>
                                        <asp:Literal ID="Literal54" runat="server" Text="<%$ Resources:whats_included53%>" />
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal55" runat="server" Text="<%$ Resources:whats_included54%>" />
                                    </p>
                                </div>
                            </div>
                            <div id="orthodontic-accelerator" class="col-md-6">
                                <div class="feature-item">
                                    <i class="icon-like text-primary"></i>
                                    <h3>
                                        <asp:Literal ID="Literal56" runat="server" Text="<%$ Resources:whats_included55%>" />
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal57" runat="server" Text="<%$ Resources:whats_included56%>" />
                                    </p>
                                    <h3>
                                        <asp:Literal ID="Literal58b" runat="server" Text="<%$ Resources:whats_included57b%>" />
                                        <span>
                                            <asp:Literal ID="Literal58c" runat="server" Text="<%$ Resources:whats_included57c%>" />
                                        </span>
                                    </h3>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal58d" runat="server" Text="<%$ Resources:whats_included57d%>" />
                                    </p>
                                    <h3>
                                        <asp:Literal ID="Literal58" runat="server" Text="<%$ Resources:whats_included57%>" />
                                        <span>
                                            <asp:Literal ID="Literal58a" runat="server" Text="<%$ Resources:whats_included57a%>" />
                                        </span>
                                    </h3>
                                    <a id="learnmoreoninternet16" href="<asp:Literal ID="Literal59" runat="server" Text="<%$ Resources:whats_included58%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                        <asp:Literal ID="Literal60" runat="server" Text="<%$ Resources:whats_included59%>" />
                                    </a>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal61" runat="server" Text="<%$ Resources:whats_included60%>" />
                                    </p>
                                    <h3>
                                        <asp:Literal ID="Literal62" runat="server" Text="<%$ Resources:whats_included61%>" />
                                        <span>
                                            <asp:Literal ID="Literal63" runat="server" Text="<%$ Resources:whats_included62%>" />
                                        </span>
                                    </h3>
                                    <a id="learnmoreoninternet17" href="<asp:Literal ID="Literal64" runat="server" Text="<%$ Resources:whats_included63%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                        <asp:Literal ID="Literal65" runat="server" Text="<%$ Resources:whats_included64%>" />
                                    </a>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal66" runat="server" Text="<%$ Resources:whats_included65%>" />
                                    </p>
                                    <h3>
                                        <asp:Literal ID="Literal67" runat="server" Text="<%$ Resources:whats_included66%>" />
                                        <span>
                                            <asp:Literal ID="Literal68" runat="server" Text="<%$ Resources:whats_included67%>" />
                                        </span>
                                    </h3>
                                    <a id="learnmoreoninternet18" href="<asp:Literal ID="Literal69" runat="server" Text="<%$ Resources:whats_included68%>" />" class="btn btn-outline btn-xl page-scroll learnmoreoninternet">
                                        <asp:Literal ID="Literal70" runat="server" Text="<%$ Resources:whats_included69%>" />
                                    </a>
                                    <p class="text-muted">
                                        <asp:Literal ID="Literal71" runat="server" Text="<%$ Resources:whats_included70%>" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
